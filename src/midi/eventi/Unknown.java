package midi.eventi;

import javax.sound.midi.ShortMessage;

/**
 *
 * @author mgiacomo
 */
public class Unknown extends Evento {
	public Unknown(long timeStamp, ShortMessage mm) {
		super(timeStamp, mm);
	}

	@Override
	public String toString() {
		return "["+getTimeFromStamp()+"]"+" "+name()+" status = " +getStatus() + " byte1: " + getData1()+" byte2: "+ getData2();
	}


	@Override
	public String[] objectsFromEvento() {
		final String[] st ={getTimeFromStamp(),name(),""+getStatus()
		,""+getData1(),""+getData2()};
		return st;
	}

	@Override
	public String name() {
		return "Unknown Message";
	}
}
