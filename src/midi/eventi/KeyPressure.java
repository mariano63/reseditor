package midi.eventi;

import midi.DumpReceiver;
import javax.sound.midi.ShortMessage;

/**
 *
 * @author mgiacomo
 */
public class KeyPressure extends Evento{

	public KeyPressure(long timeStamp, ShortMessage _mm) {
		super(timeStamp,_mm);
	}

	@Override
	public String toString() {
		return "["+getTimeFromStamp()+"]"+" channel:("+getChannel()+") "+name()+" " + DumpReceiver.getKeyName(getData1()) + " pressure: " + getData2();
	}

	@Override
	public String[] objectsFromEvento() {
		final String[] st ={getTimeFromStamp(),""+getChannel(),name(),
			""+DumpReceiver.getKeyName(getData1()), ""+getData2()};
		return st;
	}

	@Override
	public String name() {
		return "Key Pressure";
	}

}
