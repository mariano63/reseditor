package midi.eventi;

import javax.sound.midi.ShortMessage;

/**
 *
 * @author mgiacomo
 * This is a ShortMessage
 */
public class ControlChange extends Evento {


	public ControlChange(long timeStamp, ShortMessage _mm) {
		super(timeStamp, _mm);
	}
	@Override
	public String toString() {
		return "[" + getTimeFromStamp() + "]" + " channel:(" + getChannel() + ") " + name() + " " + getData1()+" "+getDescController(getData1()) + " value: " + getData2();
	}

	@Override
	public String[] objectsFromEvento() {
		final String[] st = {getTimeFromStamp(), "" + getChannel(), name(), "" + getData1()+" "+getDescController(getData1()), "" + getData2()};
		return st;
	}

	@Override
	public String name() {
		return "Control Change";
	}
	
	public static String getDescController(int ndx){
		return descController[ndx];
	}
	static String[] descController = {
		"Bank Select",
		"Modulation wheel",
		"Breath controller",
		"Undefined",
		"Foot controller",
		"Portamento time",
		"Data entry MSB",
		"Channel volume",
		"Balance",
		"Undefined",
		"Pan",
		"Expression",
		"Effect 1",
		"Effect 2",
		"Undefined",
		"Undefined",
		"General Purpose 1",
		"General Purpose 2",
		"General Purpose 3",
		"General Purpose 4",
		"Undefined",
		"???",
		"???",
		"???",
		"???",
		"???",
		"???",
		"???",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Bank Select (LSB)",
		"Mod wheel (LSB)",
		"Breath controller (LSB)",
		"Undefined",
		"Foot controller (LSB)",
		"Portamento time (LSB)",
		"Data entry (LSB)",
		"Channel volume (LSB)",
		"Balance (LSB)",
		"Undefined",
		"Pan (LSB)",
		"Expression (LSB)",
		"Effect 1 (LSB)",
		"Effect 2 (LSB)",
		"Undefined",
		"Undefined",
		"Gen Purpose 1 (LSB)",
		"Gen Purpose 2 (LSB)",
		"Gen Purpose 3 (LSB)",
		"Gen Purpose 4 (LSB)",
		"??????",
		"??????",
		"??????",
		"??????",
		"??????",
		"??????",
		"??????",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Damper Pedal",
		"Portamento On/Off",
		"Sustenuto",
		"Soft Pedal",
		"Legato Footswitch",
		"Hold 2",
		"Sound Controller 1 (Sound Variation)",
		"Sound Controller 2 (Harmonic Intensity)",
		"Sound Controller 3 (Release Time)",
		"Sound Controller 4 (Attack Time)",
		"Sound Controller 5 (Brightness)",
		"Sound Controller 6",
		"Sound Controller 7",
		"Sound Controller 8",
		"Sound Controller 9",
		"Sound Controller 10",
		"General Purpose 5",
		"General Purpose 6",
		"General Purpose 7",
		"General Purpose 8",
		"Portamento Control",
		"????",
		"????",
		"????",
		"????",
		"????",
		"????",
		"Effects 1 Depth",
		"Effects 2 Depth",
		"Effects 3 Depth",
		"Effects 4 Depth",
		"Effects 5 Depth",
		"Data Increment",
		"Data Decrement",
		"NRPN LSB",
		"NRPN MSB",
		"RPN LSB",
		"RPN MSB",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"Undefined",
		"All Sound Off",
		"Reset All Controllers",
		"Local Control On/Off",
		"All Notes Off",
		"Omni Mode On",
		"Omni Mode Off",
		"Mono Mode On (Poly Off)",
		"Poly Mode On (Mono Off)"
	};

}
