package midi.eventi;

import javax.sound.midi.ShortMessage;

/**
 *
 * @author mgiacomo
 */
public class ProgramChange extends Evento {
	public ProgramChange(long timeStamp, ShortMessage mm) {
		super(timeStamp, mm);
	}

	@Override
	public String toString() {
		return "["+getTimeFromStamp()+"]"+" channel:("+getChannel()+") "+name()+" " + getData1();
	}

	@Override
	public String[] objectsFromEvento() {
		final String[] st ={getTimeFromStamp(),""+getChannel(),name(),""+getData1()};
		return st;
	}

	@Override
	public String name() {
		return "Program Change";
	}
}
