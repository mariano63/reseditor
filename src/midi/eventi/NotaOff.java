package midi.eventi;

import midi.DumpReceiver;
import javax.sound.midi.ShortMessage;

/**
 *
 * @author mgiacomo
 */
public class NotaOff extends Evento{
	ShortMessage msg;
	
	public NotaOff(long timeStamp, ShortMessage _mm) {
		super(timeStamp, _mm);
		msg = _mm;
	}

	@Override
	public String toString() {
		return "["+getTimeFromStamp()+"]"+" channel:("+getChannel()+") "+name()+" " + DumpReceiver.getKeyName(getData1()) + " velocity: " + getData2();
	}

	@Override
	public String[] objectsFromEvento() {
		final String[] st ={getTimeFromStamp(),""+getChannel(),name(),
			""+DumpReceiver.getKeyName(getData1()), ""+getData2()};
		return st;
	}

	@Override
	public String name() {
		return "Note Off";
	}
}
