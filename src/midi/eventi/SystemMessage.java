package midi.eventi;

import midi.DumpReceiver;
import javax.sound.midi.ShortMessage;

/**
 *
 * @author mgiacomo
 */
public final class SystemMessage extends Evento {

	String strMessage = null;
	public SystemMessage(long timeStamp, ShortMessage _mm) {
		super(timeStamp, _mm);
		getMsg1(_mm);
	}

	public String getMsg1(ShortMessage msg) {
		int channel = getChannel();
		int dt1 = getData1();
		int dt2 = getData2();
		strMessage = SYSTEM_MESSAGE_TEXT[channel-1];
		switch (channel-1) {
			case 0x1:
				int nQType = (dt1 & 0x70) >> 4;
				int nQData = dt1 & 0x0F;
				if (nQType == 7) {
					nQData = nQData & 0x1;
				}
				strMessage += QUARTER_FRAME_MESSAGE_TEXT[nQType] + nQData;
				if (nQType == 7) {
					int nFrameType = (dt1 & 0x06) >> 1;
					strMessage += ", frame type: " + FRAME_TYPE_TEXT[nFrameType];
				}
				break;
			case 0x2:
				strMessage += DumpReceiver.get14bitValue(dt1, dt2);
				break;
			case 0x3:
				strMessage += dt1;
				break;
			case 0x8:
				Evento ev = new RealTimeF8(getTimeStamp(), msg, strMessage);
				if (RealTimeF8.isActive()) {
					ev.propagaEvento();
				}
				//F8 ha un suo oggetto evento, perciò o propaga il suo oggetto, o non si visualizza
				//affatto il real time clock.
				strMessage = null;
				//Chiamata al timer per set jlabel di presenza F8
//				TimerF8FE.isF8=true;
				break;
			case 0xE:
//				TimerF8FE.isFE=true;
				break;
		}
		return strMessage;
	}

	@Override
	public String toString() {
		if (strMessage != null)
			return "[" + getTimeFromStamp() + "]" + name();
		else
			return null;
	}

	@Override
	public String[] objectsFromEvento() {
		final String[] st ={getTimeFromStamp(),"", ""+name()};
		return st;
	}
private static final String[]		SYSTEM_MESSAGE_TEXT =
	{
		"System Exclusive (should not be in ShortMessage!)",
		"MTC Quarter Frame: ",
		"Song Position: ",
		"Song Select: ",
		"Undefined",
		"Undefined",
		"Tune Request",
		"End of SysEx (should not be in ShortMessage!)",
		"Timing clock",
		"Undefined",
		"Start",
		"Continue",
		"Stop",
		"Undefined",
		"Active Sensing",
		"System Reset"
	};


	private static final String[]		QUARTER_FRAME_MESSAGE_TEXT =
	{
		"frame count LS: ",
		"frame count MS: ",
		"seconds count LS: ",
		"seconds count MS: ",
		"minutes count LS: ",
		"minutes count MS: ",
		"hours count LS: ",
		"hours count MS: "
	};


	private static final String[]		FRAME_TYPE_TEXT =
	{
		"24 frames/second",
		"25 frames/second",
		"30 frames/second (drop)",
		"30 frames/second (non-drop)",
	};

	@Override
	public String name() {
		return strMessage;
	}

}
