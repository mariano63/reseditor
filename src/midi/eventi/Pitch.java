package midi.eventi;

import midi.DumpReceiver;
import javax.sound.midi.MidiMessage;

/**
 *
 * @author mgiacomo
 */
public class Pitch extends Evento {
	int value;

	public Pitch(long timeStamp,MidiMessage mm) {
		super(timeStamp,mm );
		this.value=DumpReceiver.get14bitValue(getData1(),getData2());
	}

	@Override
	public String toString() {
		return "["+getTimeFromStamp()+"]"+" channel:("+getChannel()+") "+name()+" value: " + value;
	}

	@Override
	public String[] objectsFromEvento() {
		final String[] st ={getTimeFromStamp(),""+getChannel(),name(),""+value};
		return st;
	}

	@Override
	public String name() {
		return "Pitch Bender";
	}

}
