package midi.eventi;

import config.ParameterManager;
import midi.DumpReceiver;
import javax.sound.midi.SysexMessage;

/**
 *
 * @author mgiacomo
 */
public class SystemExclusive extends Evento {
	private static boolean active=true;
	SysexMessage msg;
	String strMessage=null;
	
	public SystemExclusive(long timeStamp, SysexMessage _message) {
		super(timeStamp, _message);
		msg = _message;
		
		byte[]	abData = msg.getData();
		// System.out.println("sysex status: " + message.getStatus());
		if (msg.getStatus() == SysexMessage.SYSTEM_EXCLUSIVE)
		{
			strMessage = "Sysex message: F0" + DumpReceiver.getHexString(abData);
		}
		else if (msg.getStatus() == SysexMessage.SPECIAL_SYSTEM_EXCLUSIVE)
		{
			strMessage = "Continued Sysex message F7" + DumpReceiver.getHexString(abData);
		}		
		
        //**************
        ParameterManager.gTestExcC.data = msg.getMessage();
        ParameterManager.gVVPrmExcRx.ParseExcC(ParameterManager.gTestExcC.data);
        
		
	}
	//Properties...
	public String getMsg() { return strMessage;	}
	public void setMsg(String msg) { this.strMessage = msg;	}
	public static void setActive(boolean flag){ active=flag; }
	public static boolean isActive() { return active; }

	@Override
	public void propagaEvento() {
		if (isActive())
			super.propagaEvento();
	}
	
	@Override
	public String toString() {
		return "["+getTimeFromStamp()+"] "+name()+" " + strMessage;
	}

	@Override
	public String[] objectsFromEvento() {
		final String[] st ={getTimeFromStamp(),"",name(),""+strMessage};
		return st;
	}

	@Override
	public String name() {
		return "System Exclusive";
	}
}
