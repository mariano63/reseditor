package midi.eventi;

import javax.sound.midi.ShortMessage;

/**
 *
 * @author mgiacomo
 */
public class RealTimeF8 extends Evento{
	private static boolean active=false;
	private final String msg;
	
	public RealTimeF8(long timeStamp, ShortMessage _mm, String _msg) {
		super(timeStamp, _mm);
		this.msg = _msg;
	}

	@Override
	public void propagaEvento() {
		if (isActive())
			super.propagaEvento();
	}

	public static void setActive(boolean flag){
		active=flag;
	}

	public static boolean isActive() {
		return active;
	}

	@Override
	public String toString() {
		return "["+getTimeFromStamp()+"]"+" "+name();
	}

	@Override
	public String[] objectsFromEvento() {
		final String[] st ={getTimeFromStamp(),""," "+name()};
		return st;
	}

	@Override
	public String name() {
		return msg;
	}
}
