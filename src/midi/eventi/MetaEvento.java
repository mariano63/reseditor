
package midi.eventi;

import javax.sound.midi.MetaMessage;

/**
 *
 * @author mgiacomo
 */
public class MetaEvento extends Evento{

	String str;
	MetaMessage msg;
	public MetaEvento(long timeStamp, MetaMessage mm, String st) {
		super(timeStamp, mm);
		msg = mm;
	}

	
	@Override
	public String[] objectsFromEvento() {
//		final String[] st ={getTimeFromStamp(),""+channel,name(),""};
		final String[] st ={getTimeFromStamp(),"°",name(),""+msg.getMessage()};
		return st;
	}

	@Override
	public String name() {
		throw new UnsupportedOperationException("Meta");
	}
	
}
