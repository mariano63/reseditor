/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans01_prove;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class BasicDraw {
  public static void main(String[] args) {
    new BasicDraw();
  }

  BasicDraw() {
      
      int[] i = new int[100];
      List<Integer> lst = new ArrayList<>();
      
      for(int s : i){
          if (s>100)
                lst.add(s);
      }
        Integer[] prmTbl = new Integer[0];
        prmTbl = lst.toArray(prmTbl);
      
    JFrame frame = new JFrame();

    frame.add(new MyComponent());

    frame.setSize(300, 300);
    frame.setVisible(true);
  }

}

class MyComponent extends JComponent {

  public void paint(Graphics g) {
    Graphics2D g2d = (Graphics2D) g;
    g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
  }

}