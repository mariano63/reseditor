/**
 * Padre di tutti i beans!
 * E' un pannello che contiene utilità varie e condivise a tutti i beans. come l'emilId, ad esempio
 * o i metodi per leggere/scrivere streams
 */
package beans;

import config.Parameter;
import config.ParameterManager;
import emil.COEmilStm;
import emil.istm.CIEmilStmPC;
import emil.istm.CIEmilStmPF;
import emil.istm.CIEmilStmPH;
import emil.istm.CIEmilStmPT;
import emil.istm.CIEmilStmPV;
import emil.istm.CIEmilStmPX;
import emil.istm.CIEmilStmRC;
import emil.istm.CIEmilStmRF;
import emil.istm.CIEmilStmRH;
import emil.istm.CIEmilStmRT;
import emil.istm.CIEmilStmRV;
import emil.ostm.COEmilStmPC;
import emil.ostm.COEmilStmPF;
import emil.ostm.COEmilStmPH;
import emil.ostm.COEmilStmPT;
import emil.ostm.COEmilStmPV;
import emil.ostm.COEmilStmPX;
import emil.ostm.COEmilStmRC;
import emil.ostm.COEmilStmRF;
import emil.ostm.COEmilStmRH;
import emil.ostm.COEmilStmRT;
import emil.ostm.COEmilStmRV;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Timer;

/**
 *
 * @author mgiacomo
 */
public abstract class JBeansFather extends javax.swing.JPanel implements Serializable {
    protected String emilId = "";       //EMSC_LCD_CONTRAST, EMSQ_EQ6_HI_FREQ...
    protected Parameter prm = null;
    protected TimerUpdate tim;
    protected DaPrmtoChildXswing daPrmtoChildXswing=new DaPrmtoChildXswing();
    
    ObservObj observObj;
    
    private int tonePatch_0_3 = 0;  //"EMPT"
    private int rythmPatch_0_3 = 0;  //"EMRT"
    //Se il pannello non e' inizializzato, dataIsChanged() non deve richiamare swingPaint() o send sysexcl.
    private boolean panelInitialized = false;
    /**
     * Creates new form JBeansFather
     */
    public JBeansFather() {
        initComponents();
        tim = new TimerUpdate();    //timer controllo data, partirà, dopo initializePanel()
        observObj = new ObservObj(this);
    }

    /**
     * Properties
     */
    
    public ObservObj getObservObj() {
        return observObj;
    }

    
    public void setPrm(Parameter prm) {
        this.prm = prm;
    }

    public Parameter getPrm() {
        return prm;
    }
    public String getEmilId() {
        return emilId;
    }
    public void setEmilId(String emId) {
        panelInitialized = false;   //dataIsChanged() non avrà effetto
        ParameterManager.getInstance().initialize();
        this.emilId = emId;
        prm = ParameterManager.getInstance().getMappa().get(emilId);
        daPrmtoChildXswing.readMinAndMax();
        initializePanel();  
        panelInitialized=true;      //dataIsChanged() verra' servita
        tim.start();
    }
    public void setTonePatch_0_3(int tonePatch_0_3) {
        this.tonePatch_0_3 = tonePatch_0_3;
    }

    public int getTonePatch_0_3() {
        return tonePatch_0_3;
    }
    public void setRythmPatch_0_3(int rythmPatch_0_3) {
        this.rythmPatch_0_3 = rythmPatch_0_3;
    }

    public int getRythmPatch_0_3() {
        return rythmPatch_0_3;
    }
    
    /**
     * Properties End
     */
    
    
    /**
     * fondamentale! E viene eseguito solo quando si effettua il setEmilId()!!!
     */
    abstract public void initializePanel();

    /**
     *
     * @return Es. Se il figlio è uno slider, questo metodo deve ritornare
     * jslider.getValue();
     * E' il valore che viaggera' poi col sistema esclusivo, quindi sempre numerico.
     * L' eventuale offset verrà aggiunto in seguito dal sistema Emil.
     */
    abstract public long getDataSwingObject();

    /**
     *
     *  Es. Se il figlio è uno slider, questo metodo deve essere
     *  public void setDataSwingObject(long data){
     *      jslider.settext(""+data);
     * }
     * jslider.getValue();
     * @param data
     */
    abstract public void setDataSwingObject(long data);
    abstract public void swingPaint();

    /**
     * Chiamarla quando si modifica il dato sul widget
     *
     */
    public void dataIsChanged(boolean writeInternalBuffer) {
        if (!panelInitialized)  return; //Pannello non ancora inizializzato per intero. esci.
        long dato = getDataSwingObject();
        if (writeInternalBuffer) {
            sysExcToSend(dato);
        }
        swingPaint();
        
        observObj.setData(dato);    //
        observObj.propagate();      //for eventual observer
    }

    public void sysExcToSend(long value) {
        COEmilStm aPC;

        System.out.format("Write %d on %s \n", value, emilId);
        String s = emilId.substring(0,4);
        switch(s){
            case "EMPC":{
                aPC = new COEmilStmPC();
                aPC.Open(0);
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            case "EMPF":{
                aPC = new COEmilStmPF();
                aPC.Open(0);
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            case "EMPH":{
                aPC = new COEmilStmPH();
                aPC.Open(0);
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            case "EMPT":{
                aPC = new COEmilStmPT();
                aPC.Open(0, tonePatch_0_3); //TODO
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            case "EMPV":{
                aPC = new COEmilStmPV();
                aPC.Open(0);
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            case "EMPX":{
                aPC = new COEmilStmPX();
                aPC.Open(0);
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            case "EMRC":{
                aPC = new COEmilStmRC();
                aPC.Open(0);
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            case "EMRF":{
                aPC = new COEmilStmRF();
                aPC.Open(0);
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            case "EMRH":{
                aPC = new COEmilStmRH();
                aPC.Open(0);
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            case "EMRT":{
                aPC = new COEmilStmRT();
                aPC.Open(0, rythmPatch_0_3); //TODO
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            case "EMRV":{
                aPC = new COEmilStmRV();
                aPC.Open(0);
                aPC.Write(value, (short) ParameterManager.getPrmId(emilId));
                aPC.Close(1);
                break;
            }
            default:        
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        if(evt.getClickCount()==2){
            setDataSwingObject(prm.getSwInit());
            dataIsChanged(true);
        }
    }//GEN-LAST:event_formMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    /**
     * timer class per controllo dato slider. se cambiato, viene riletto da user
     * area, e inserito nell slider.
     */
    public class TimerUpdate {

        Timer timer;
        int low, high;		//il fire viene lanciato a partire da low, fino ad high.
        int delay;

        public TimerUpdate() {
            delay = 500;	//300msec. is the default time.
            ActionListener taskPerformer = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    timer.stop();
                    //se il widget non e' visibile per vari motivi, 
                    //non viene gestito il monitor
                    Rectangle rec = JBeansFather.this.getVisibleRect();
                    if (rec.width==0){
                        timer.start();
                        return;
                    }
                    String s = emilId.substring(0, 4);
                    long data;
                    switch (s) {
                        case "EMPC":{
                            CIEmilStmPC aPC = new CIEmilStmPC();
                            aPC.Open(0);
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        case "EMPF":{
                            CIEmilStmPF aPC = new CIEmilStmPF();
                            aPC.Open(0);
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        case "EMPH":{
                            CIEmilStmPH aPC = new CIEmilStmPH();
                            aPC.Open(0);
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        case "EMPT":{
                            CIEmilStmPT aPC = new CIEmilStmPT();
                            aPC.Open(0,tonePatch_0_3);  //!!!!! TODO
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        case "EMPV":{
                            CIEmilStmPV aPC = new CIEmilStmPV();
                            aPC.Open(0);
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        case "EMPX":{
                            CIEmilStmPX aPC = new CIEmilStmPX();
                            aPC.Open(0);
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        case "EMRC":{
                            CIEmilStmRC aPC = new CIEmilStmRC();
                            aPC.Open(0);
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        case "EMRF":{
                            CIEmilStmRF aPC = new CIEmilStmRF();
                            aPC.Open(0);
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        case "EMRH":{
                            CIEmilStmRH aPC = new CIEmilStmRH();
                            aPC.Open(0);
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        case "EMRT":{
                            CIEmilStmRT aPC = new CIEmilStmRT();
                            aPC.Open(0, rythmPatch_0_3 );    //TODO !!!
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        case "EMRV":{
                            CIEmilStmRV aPC = new CIEmilStmRV();
                            aPC.Open(0);
                            data = aPC.ReadValue((short) ParameterManager.getPrmId(emilId));
                            aPC.Close();
                        }
                            break;
                        default:
                            return; //No correct argument, exit
                    }
                    
                    //DEBUG
//                    System.out.println("EmilId="+emilId+"--getPrmId="+(short) ParameterManager.getPrmId(emilId)+" -- Value="+data);
//                    if (emilId.equalsIgnoreCase("EMPC_LEVEL")){
//                        System.out.println("Read EMPC_LEVEL by parameters:\n");
//                         CIEmilStmPC aReadPC = new CIEmilStmPC();
//                        aReadPC.Open(0);
//                         int aValue = (int) aReadPC.ReadValue((short) ParameterManager.getPrmId("EMPC_LEVEL"));
//                        aReadPC.Close();
//                        System.out.format("%d\n", aValue);         
//                    }
                    //END OF DEBUG
                    if ( (getDataSwingObject()) != data) {
                        setDataSwingObject(data);
                        dataIsChanged(false);
                    }
                    timer.start();
                }
            };
            timer = new Timer(delay, taskPerformer);
        }

        public void stop() {
            timer.stop();
        }

        public void start() {
            timer.start();
        }

        /**
         * set milliseconds delay for fireEvent.
         *
         * @param delay
         */
        public void setDelay(int delay) {
            this.delay = delay;
        }
    }
    
    /**
     *legge alcuni dati da prm, e li rende accessibili ai beans figli
     *in em_prmtbl, ci sono i dati min e max relativi al midi send-receive
     *Ma non e' detto che la visualizzazione sia biunivoca
     *Ad esempio si potrebbe avere 1..4 da spedire e 0..3 da visualizzare
     *oppure 10-100, a step di 10, da visualizzare, mentre i valori da spedire/ricevere sono 1..10
     *e cosi via.
     */
    public class DaPrmtoChildXswing{
        private int minValue;   //valore minimo letto da em_prmrng.h
        private int maxValue;   //valore massimo letto da em_prmrng.h
        private int multiplierForStringValue=1;
        boolean Ispanpot = false;

        public boolean isIspanpot() {
            return Ispanpot;
        }
        
        
        public int getminValue(){
            return minValue;
        }

        public int getMaxValue() {
            return maxValue;
        }
                
        private void readMinAndMax(){
            minValue = prm.getSwMin();
            maxValue = prm.getSwMax();
            String[] strWithRange = prm.getEmilRange().split(",");
            Pattern pattern = Pattern.compile("[-|+]*(\\d+)");
            Matcher matcher = pattern.matcher(strWithRange[1]);
            if (matcher.find()){ 
                minValue = Integer.parseInt( matcher.group().replace('+', ' ').trim() ) ;
            }
            if (matcher.find()){   
                maxValue = Integer.parseInt( matcher.group().replace('+', ' ').trim() ) ;
            }
                
            multiplierForStringValue = (prm.getSwMax() != 0) ? maxValue/prm.getSwMax() : 1;
            
            if (prm.getEmilRange().indexOf(maxValue+"R") != -1){
                minValue = -minValue;
                Ispanpot = true;
            }
        }
             
        public String readDataToDisplay(long data){       
            long dataOut;
            
            //Il range qui e' 0 to 127 OR MUSICAL-NOTE.
            if (prm.getEmilRange().indexOf("MUSICAL-NOTES") != -1){
                //Se valori superiori a 127, inserire dati tabella...
                if (data>127)
                    return stblMusicalNote[(int)(data-128)];
                return data+""; 
            }
            
            if(prm.getEmilRange().indexOf("FIXED\"or")!=-1 && data==0){
                return "FIXED";
            }
            
            if(prm.getEmilRange().indexOf("OFF\"or")!=-1 && data==0){
                return "OFF";
            }

            if( (prm.getEmilRange().indexOf("C-1\"to")!=-1) || (prm.getEmilRange().indexOf("to\"G9")!=-1)){
                return notes[(int)data%12]+((data/12)-1);
            }

            //Se numero di step slider effettivi, sono uguali al numero di step effettivi
            if ( (prm.getSwMax() - prm.getSwMin() == (maxValue-minValue) ) || Ispanpot){
                if (prm.getSwMin() != minValue)
                    dataOut = data + minValue; //+ prm.getOfst(); 
                else 
                    dataOut = data; //+ prm.getOfst(); 
            } else {
                // Moltiplicatore.
                dataOut = data * multiplierForStringValue;  //+prm.getOfst();
            }
            
            String toAppend ="";
            if (Ispanpot){
                if (dataOut<0) return "L"+Math.abs(dataOut);
                else if (dataOut>0) return dataOut+"R";
            }
            return dataOut+"";
        }
    }
    
    
        private static final String[] notes= {
            "C","C#","D","Eb","E","F","F#","G","G#","A","Bb","B"
        };
                
        protected static final String[] stblMusicalNote= {
            "1/64 T",	/*  Sixty-four triplet */
            "1/64",	/*  Sixty-four note */
            "1/32 T", 	/*  Thirty-second triplet */     
            "1/32",	/*  Thirty-second note */ 
            "1/16 T",	/*  Sixteenth-note triplet */     
            "1/32.", 	/*  Dotted thirty-second note */
            "1/16", 	/*  Sixteenth note */           
            "1/8 T", 	/*  Eighth-note triplet */     
            "1/16.",	/*  Dotted sixteenth note */      
            "1/8",  	/*  Eighth note */         
            "1/4 T", 	/*  Quarter-note triplet */   
            "1/8.", 	/*  Dotted eighth note */      
            "1/4", 	/*  Quarter note */              
            "1/2 T", 	/*  Half-note triplet */        
            "1/4.", 	/*  Dotted quarter note */     
            "1/2",	/*  Half note */                 
            "1/1 T", 	/*  Whole-note triplet */       
            "1/2.",	/*  Dotted half note */         
            "1/1",	/*  Whole note */                
            "2/1 T", 	/*  Double-note triplet */      
            "1/1.", 	/*  Dotted whole note */        
            "2/1",	/*  Double note */ 
        };                      
}
