package beans.panels;

import beans.JBeansFather;
import beans.panels.xml.PanelLists;
import beans.panels.xml.XmlData;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.io.File;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * un ' immagine secca
 * @author mgiacomo
 */
public class PanGeneric extends JPanel implements Serializable {
    int matteTop = 14;
    int matteLeft = 1;
    int matteRight=1;
    int matteBottom=1;
    Color matteTitleColor = java.awt.Color.white;
    Font matteTitleFont = new java.awt.Font("Dialog", 1, 14);
//    final String slashOs = System.getProperty("file.separator");
//    String sTmp=String.format("%sbeans%spanels%sbmp%sPatternJeans.png", slashOs,slashOs, slashOs, slashOs);
//    Icon matteIcon =  new javax.swing.ImageIcon(getClass().getResource(sTmp));
    Icon matteIcon =  new javax.swing.ImageIcon(getClass().getResource("/beans/panels/bmp/PatternJeans.png"));
    String matteTitle="???";
    Color matteColor = new Color(199, 90, 31);
    boolean useMatteColor = false;
    boolean useMatteIcon = false;
    
    public PanGeneric() {
        initComponents();
    }

    public boolean isUseMatteIcon() {
        return useMatteIcon;
    }

    public void setUseMatteIcon(boolean useMatteIcon) {
        this.useMatteIcon = useMatteIcon;
        if (useMatteIcon)
            setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(getMatteTop(), getMatteLeft(), getMatteBottom(), getMatteRight(), getMatteIcon()), getMatteTitle(), javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, getMatteTitleFont(), getMatteTitleColor())); // NOI18N
    }

    
    public boolean isUseMatteColor() {
        return useMatteColor;
    }

    public void setUseMatteColor(boolean useMatteColor) {
        this.useMatteColor = useMatteColor;
        if (useMatteColor)
            setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(getMatteTop(), getMatteLeft(), getMatteBottom(), getMatteRight(), getMatteColor()), getMatteTitle(), javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, getMatteTitleFont(), getMatteTitleColor())); // NOI18N
    }

    public int getMatteBottom() {
        return matteBottom;
    }

    public int getMatteLeft() {
        return matteLeft;
    }

    public int getMatteRight() {
        return matteRight;
    }

    public int getMatteTop() {
        return matteTop;
    }

    public void setMatteBottom(int matteBottom) {
        this.matteBottom = matteBottom;
    }

    public void setMatteLeft(int matteLeft) {
        this.matteLeft = matteLeft;
    }

    public void setMatteRight(int matteRight) {
        this.matteRight = matteRight;
    }

    public void setMatteTop(int matteTop) {
        this.matteTop = matteTop;
    }

    
    public Color getMatteTitleColor() {
        return matteTitleColor;
    }

    public void setMatteTitleColor(Color matteTitleColor) {
        this.matteTitleColor = matteTitleColor;
    }

    
    public void setMatteTitleFont(Font matteTitleFont) {
        this.matteTitleFont = matteTitleFont;
    }

    public Font getMatteTitleFont() {
        return matteTitleFont;
    }
    
    
    public void setMatteColor(Color colorMatte) {
        this.matteColor = colorMatte;
    }

    public Color getMatteColor() {
        return matteColor;
    }
    
    public void setMatteTitle(String title) {
        this.matteTitle = title;
    }

    public String getMatteTitle() {
        return matteTitle;
    }
    
    
    public void setMatteIcon(Icon matteIcon) {
        this.matteIcon = matteIcon;
    }

    public Icon getMatteIcon() {
        return matteIcon;
    }
    
    /**
     *  XML Section 
     */
    
    private void getAllBeansInfo(Container c, PanelLists<XmlData> pl) throws JAXBException {
        Component[] components = c.getComponents();
        for (Component com : components) {
            if (com instanceof JBeansFather) {
                pl.getValues().add(new XmlData( ((JBeansFather)com).getEmilId(), ((JBeansFather)com).getDataSwingObject()));
            } else if (com instanceof Container) {
                getAllBeansInfo((Container) com, pl);
            }
        }
    }

    public void writeSerializedPanel() {
        try {
            String fileName = String.format("%s%s%s%s_ser.xml", System.getProperty("user.dir"), System.getProperty("file.separator"), System.getProperty("file.separator"), getName());
            File f = new File(fileName);
            
            JAXBContext context;
            Marshaller m;
            context = JAXBContext.newInstance(PanelLists.class, XmlData.class);
            PanelLists<XmlData> panelLists = new PanelLists<>(getName());
            
            m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            getAllBeansInfo(this, panelLists);
            m.marshal(panelLists, System.out);
            // Write to File
            //                m.marshal(com, new File(BOOKSTORE_XML));       //!!!
        } catch (JAXBException ex) {
            Logger.getLogger(PanelImgScaled.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void readSerializedPanel() {
        String sTempo = String.format("%s%sxml%s%s_ser.xml", System.getProperty("user.dir"), System.getProperty("file.separator"), System.getProperty("file.separator"), getName());
        String fileName = getClass().getResource(sTempo).getFile();
        File f = new File(fileName);

        //TODO
    }
    
    
    /**
     *  XML Section END
     */
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setOpaque(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 117, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 109, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
