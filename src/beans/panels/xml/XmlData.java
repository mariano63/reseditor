/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.panels.xml;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mgiacomo
 */
@XmlRootElement(name="beans")
public class XmlData {

    String emilId;
    long value;

    public String getEmilId() {
        return emilId;
    }

    public void setEmilId(String emilId) {
        this.emilId = emilId;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    
    public XmlData(String emilId, long value) {
        this.emilId = emilId;
        this.value = value;
    }

    public XmlData() {
    }

}
