package beans.panels.xml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *  JAXB pannello.
 * @author mgiacomo
 * @param <XmlData>
 */

@XmlRootElement(name="PANEL")
public class PanelLists<XmlData> {
    private String panelName;
    private List<XmlData> values = new ArrayList<>();

    public String getPanelName() {
        return panelName;
    }

    public void setPanelName(String panelName) {
        this.panelName = panelName;
    }
    
    @XmlAnyElement(lax=true)
    public List<XmlData> getValues() {
        return values;
    }

    public PanelLists(String panelName) {
        this.panelName = panelName;
    }

    public PanelLists() {
    }
    
    
    
    
    
}
