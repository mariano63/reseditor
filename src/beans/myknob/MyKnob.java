package beans.myknob;

// Imports for the GUI classes.
import beans.panels.PanelImg;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class MyKnob extends JComponent implements MouseListener, MouseMotionListener, MouseWheelListener {

    private int widgetWheelSteps = 1;
    Image img = null;    //Conterra' immagine rotella, ridimensionata ad hoc, a seconda width,height

    public int getWidgetWheelSteps() {
        return widgetWheelSteps;
    }

    public void setWidgetWheelSteps(int widgetWheelSteps) {
        this.widgetWheelSteps = widgetWheelSteps;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent evt) {
        if (evt.getWheelRotation() < 0)//mouse wheel was rotated up/away from the user
        {
//            int iNewValue = jSlider.getValue() - jSlider.getMinorTickSpacing()  ;
            int iNewValue = (int) (getValue() - widgetWheelSteps);
            if (iNewValue >= getMinValue()) {
                setValue(iNewValue);
            } else {
                setValue(getMinValue());
            }
        } else {
//            int iNewValue = jSlider.getValue() + jSlider.getMinorTickSpacing()  ;
            int iNewValue = (int) (getValue() + widgetWheelSteps);
            if (iNewValue <= getMaxValue()) {
                setValue(iNewValue);
            } else {
                setValue(getMaxValue());
            }
        }
    }

    //Con addObservers puoi aggiungere chiunque, e poi visualizzare il dato
    public class forObserver extends Observable {

        java.util.List<Observer> list = new ArrayList<>();

        public void callObservers() {
            setChanged();
            notifyObservers(getValue());
        }
    }

    forObserver forObserver = new forObserver();

    private final double minAtan = -2.25;
    private final double maxAtan = 2.25;
    final String DEFAULTPNG = "klr_";
    final String DEFAULTPNG_H = "klr_h_";
    String iconNameWithoutNumber = DEFAULTPNG;
//    Icon backgroundIcon = new javax.swing.ImageIcon(getClass().getResource("bmp/Knob01.png")); // NOI18N
    Icon backgroundIcon;
    private double theta;
    private boolean pressedOnSpot;
    private int radius = 24;       //Per un immagine 48x48
    private int spotRadius = radius / 5;
    private long value = 0;
    private long minValue = 0;
    private long maxValue = 127;

    /**
     * properties
     */
    public String getDEFAULTPNG_H() {
        return DEFAULTPNG_H;
    }

    public String getDEFAULTPNG() {
        return DEFAULTPNG;
    }

    public void setIconNameWithoutNumber(String iconNameWithoutNumber) {
        this.iconNameWithoutNumber = iconNameWithoutNumber;
    }

    public String getIconNameWithoutNumber() {
        return iconNameWithoutNumber;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
        theta = (((value - getMinValue()) * (maxAtan * 2)) / (getMaxValue() - getMinValue())) - maxAtan;
        forObserver.callObservers();
    }
    private long valore = 0l;

    /**
     * Get the value of valore
     *
     * @return the value of valore
     */
    public long getValore() {
        return valore;
    }

    public long getMinValue() {
        return minValue;
    }

    public void setMinValue(long minValue) {
        this.minValue = minValue;
    }

    public long getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(long maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * PROPERTIES END
     */
    /**
     * No-Arg constructor that initializes the position of the knob to 0 radians
     * (Up).
     */
    public MyKnob() {
        this(0);
    }

    /**
     * Constructor that initializes the position of the knob to the specified
     * position and also allows the colors of the knob and spot to be specified.
     *
     * @param initAngle the initial angle of the knob.
     */
    public MyKnob(double initTheta) {

        super();

        theta = initTheta;
        pressedOnSpot = false;

        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        addMouseWheelListener(this);

        this.setOpaque(false);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (getWidth() != getHeight()) {
            setSize(new Dimension(getWidth(), getWidth()));
            invalidate();
        }
        radius = getWidth() / 2;
        spotRadius = radius / 2;

        int valueToModulize = (int) (((getValue() + Math.abs(getMinValue())) * 63) / (getMaxValue() - getMinValue()));
        long val = (valueToModulize % 64) + 1;   //1..64, Knob01..Knob64
//        String s = ((ImageIcon)backgroundIcon).getDescription();
//        s = s.substring(s.lastIndexOf("/")+1);      //s = fileName
//        s = s.substring(0,s.lastIndexOf(".")-2);      //s = filename without extension and last 2 digits
//        String fName = String.format("bmp%s%s%02d.png", System.getProperty("file.separator") , s, val);   // Nome del file per paint
        String fName = String.format("/beans/myknob/bmp/%s%02d.png", iconNameWithoutNumber, val);   // Nome del file per paint

        URL urlObj = getClass().getResource(fName);
        if (urlObj != null) {
            backgroundIcon = new javax.swing.ImageIcon(urlObj);
        } else {
            Logger.getLogger(MyKnob.class.getName()).log(Level.SEVERE, "backgroundIcon parameter missing!");
            return;
        }

        setOpaque(false);
//        BufferedImage img = scaleImage(getWidth(), getHeight());
        BufferedImage bimg = null;
        if (img == null) {
            bimg = scaleImage(getWidth(), getHeight());
        } else if (img.getWidth(this) != getWidth() || img.getHeight(this) != getHeight()) {
            bimg = scaleImage(getWidth(), getHeight());
        } else {
            img = PanelImg.iconToImage(backgroundIcon);
        }

//        img = PanelImg.iconToImage(backgroundIcon);
        g.drawImage((bimg==null)?img:bimg, 0, 0, null);

    }

    private BufferedImage scaleImage(int WIDTH, int HEIGHT) {
        BufferedImage bi = null;
        try {
            Image ii = PanelImg.iconToImage(backgroundIcon);
            bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
//        bi = new BufferedImage(backgroundIcon.getIconWidth(), backgroundIcon.getIconHeight(),BufferedImage.TYPE_INT_RGB);
            // Create the graphics context
            Graphics g = bi.createGraphics();
            // Now paint the icon
//        backgroundIcon.paintIcon(null, g, 0, 0);
            g.dispose();

            Graphics2D g2d = (Graphics2D) bi.createGraphics();
//        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
            g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));

//        g2d.drawImage(ii, 0, 0, WIDTH, HEIGHT, Color.GREEN, null);
            g2d.drawImage(ii, 0, 0, WIDTH, HEIGHT, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return bi;
    }

    /**
     * Return the ideal size that the knob would like to be.
     *
     * @return the preferred size of the JKnob.
     */
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(2 * radius, 2 * radius);
    }

    /**
     * Return the minimum size that the knob would like to be. This is the same
     * size as the preferred size so the knob will be of a fixed size.
     *
     * @return the minimum size of the JKnob.
     */
    @Override
    public Dimension getMinimumSize() {
        return new Dimension(2 * radius, 2 * radius);
    }

    /**
     * Get the current angular position of the knob.
     *
     * @return the current anglular position of the knob.
     */
    public double getAngle() {
        return theta;
    }

    /**
     * Calculate the x, y coordinates of the center of the spot.
     *
     * @return a Point containing the x,y position of the center of the spot.
     */
    private Point getSpotCenter() {

        // Calculate the center point of the spot RELATIVE to the
        // center of the of the circle.
        int r = radius - spotRadius;

        int xcp = (int) (r * Math.sin(theta));
        int ycp = (int) (r * Math.cos(theta));

        // Adjust the center point of the spot so that it is offset
        // from the center of the circle.  This is necessary becasue
        // 0,0 is not actually the center of the circle, it is  the 
        // upper left corner of the component!
        int xc = radius + xcp;
        int yc = radius - ycp;

        // Create a new Point to return since we can't  
        // return 2 values!
        return new Point(xc, yc);
    }

    /**
     * Determine if the mouse click was on the spot or not. If it was return
     * true, otherwise return false.
     *
     * @return true if x,y is on the spot and false if not.
     */
    private boolean isOnSpot(Point pt) {
        return (pt.distance(getSpotCenter()) < spotRadius);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {

        Point mouseLoc = e.getPoint();
        pressedOnSpot = isOnSpot(mouseLoc);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        pressedOnSpot = false;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    /**
     * Calcolo il nuovo angolo per lo spot e rivisualizzo rotella. Il nuovo
     * angolo e' calcolato basandosi sulla nuova posizione del mouse.
     *
     * @param e
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        if (pressedOnSpot) {

            int mx = e.getX();
            int my = e.getY();

            // Calcolo posizione x, del mouse relativo al centro della rotella.
            int mxp = mx - radius;
            int myp = radius - my;

            // Calcolo nuovo angolo rotella
            // nuova x e y del mouse.  
            // Math.atan2(...) calcola angolo nel quale
            // x,y giacciono from the positive y axis with cw rotations
            // being positive and ccw being negative.
            double tmpTheta = Math.atan2(mxp, myp);

            if (Math.abs(tmpTheta - theta) > 1) {
                return;
            }

            int n = (int) ((theta + maxAtan) * (getMaxValue() - getMinValue()) / (maxAtan * 2));
            n += getMinValue();

            if (tmpTheta > minAtan && tmpTheta < maxAtan) {
                theta = tmpTheta;
                if (getValue() == n) {
                    return;
                }
                setValue(n);
//                    System.out.println(getValue()+"theta="+theta+",tmpTheta="+tmpTheta);
                repaint();
            } else {
                if (tmpTheta < minAtan) {
                    theta = minAtan;
                    if (getValue() == n) {
                        return;
                    }
                    setValue(n);
//                    System.out.println(getValue()+"theta="+theta+",tmpTheta="+tmpTheta);
                    repaint();
                } else if (tmpTheta > maxAtan) {
                    theta = maxAtan;
                    if (getValue() == n) {
                        return;
                    }
                    setValue(n);
//                    System.out.println(getValue()+"theta="+theta+",tmpTheta="+tmpTheta);
                    repaint();
                }
            }
        }
    }
}
