package beans.sliders;

import java.util.Hashtable;
import javax.swing.JLabel;

/**
 *  Slider per Range composto da tutti "or"
 * @author mgiacomo
 */
public class SliderEmilPanelAllOr extends SliderEmilPanel{
    
    Hashtable labelTable = new Hashtable();

    @Override
        public void initializePanel() {
        super.initializePanel();
//         setOrientation(SwingConstants.HORIZONTAL);
//        super.setEmilId(emilId);    //La variabile prm, viene inizializzata sulla super.
        /**
         * Leggiamo le labels attraverso lo split 
         */
        String[] orSplit = prm.getEmilRange().toLowerCase().split("\"or\"");
        JLabel jl;
        for (int i = 0; i< orSplit.length; i++){
            if (i==0){
                jl = new JLabel(orSplit[i].substring(orSplit[i].indexOf("\"")+1));
                jl.setForeground(getWidgetJSlider().getForeground());
                jl.setFont(getWidgetJSlider().getFont());
                labelTable.put( new Integer( 0 ), jl );
                continue;
            }
            if (i==orSplit.length-1){
                jl = new JLabel(orSplit[i].substring(0,orSplit[i].lastIndexOf("\"")));
            } else {
                jl = new JLabel(orSplit[i]);
            }
            jl.setForeground(getWidgetJSlider().getForeground());
            jl.setFont(getWidgetJSlider().getFont());
            labelTable.put( new Integer( i ),  jl );
        }
        getWidgetJSlider().setLabelTable( labelTable );
        getWidgetJSlider().setMajorTickSpacing(1);
        getWidgetJSlider().setMinorTickSpacing(1);
//        valueToDisplay(getWidgetJSlider().getValue(), false);
//        getWidgetJSlider().setForeground(Color.red);    //Questo colorerebbe i thick
        
        swingPaint();
    }
    
    @Override
    public String convertedDataSwingObject(){
        int val = getWidgetJSlider().getValue();
        JLabel jl = (JLabel)(getWidgetJSlider().getLabelTable().get(val));
        if (jl==null)   return "";  //Il primo swingPaint() viene chiamato dal padre, e genererebbe un errore
        return jl.getText();
    }

    
}
