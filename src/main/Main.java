package main;

import config.ParameterManager;
import static emil.MainDebug.hexStringToByteArray;
import emil.istm.CIEmilStmPC;
import emil.ostm.COEmilStmPC;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.Receiver;
import javax.sound.midi.SysexMessage;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import myMidi.MyMidi;

/**
 *
 * @author mgiacomo
 */
public class Main {
    //Properties final
    static private Properties properties=null;
    static final String PROPERTIES_PATH = 
            System.getProperty("user.home")+System.getProperty("file.separator")+
            ".ResEditor"+System.getProperty("file.separator"); 
    static final public String PROPERTIES_FILE = "ResEditor.properties";
    static public final String MIDIDEVICE_OUT_KEY="mididevice.output";
    static public final String MIDIDEVICE_INP_KEY="mididevice.input";
    
    
    static SysExclQueue sendSysExcl;
    MyMidi myMidi = new MyMidi();   //Open and create receive and transmit, if exist on profile
    //Tono attualmente selezionato.
    // Tutti i pannelli fanno riferimento a questa statica.
    //Main.getActualToneSelected()
//    static public int getActualToneSelected(){ return  beans.noemily.JRadioButtonTone.getActualToneSelected(); }
    
    static final List<javax.swing.UIManager.LookAndFeelInfo> listLF = new ArrayList<>();
    static int lookAndFeelNdx = 0;

    static {
//        listLF.addAll(Arrays.asList(javax.swing.UIManager.getInstalledLookAndFeels()));
        
    }

    static public Wb wb;

    static public Properties getProperties(){
        if (properties == null){
            properties = new Properties();
            loadProperties();
        }
        return properties;
    }
    static public void storeProperties(){
        if (properties==null)
            return;
        try {
            properties.store(new FileOutputStream(PROPERTIES_PATH+PROPERTIES_FILE),null);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    static private void loadProperties(){
        //Create properties directory if doesn't exists
        File f = new File(PROPERTIES_PATH);
        if (!f.exists()){
            if (f.mkdir())
                System.out.print("Created directory:");
            else {
                System.out.print("Error creating directory:");
            }
            System.out.print(PROPERTIES_PATH);
        }
        
        //read properties file if exists
        try {
            properties.load(new FileInputStream(PROPERTIES_PATH+PROPERTIES_FILE));
        } catch (FileNotFoundException  ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //create Receive, and Transmitter, if exist on properties        
    }
    static public void loopLookAndFeel(JFrame frame) {
        lookAndFeelNdx++;
        if (lookAndFeelNdx >= listLF.size()) {
            lookAndFeelNdx = 0;
        }
        javax.swing.UIManager.LookAndFeelInfo lf = listLF.get(lookAndFeelNdx);
        try {
            UIManager.setLookAndFeel(lf.getClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        SwingUtilities.updateComponentTreeUI(frame);
        sliderDesign();
        frame.pack();
    }

    /**
     * @param args the command line arguments
     */
    static private void sliderDesign() {
        URL url = ClassLoader.getSystemClassLoader().getResource("bmp/CursoreSlider30x15Red.png");
        Icon icon = new ImageIcon(url);
        UIDefaults defaults = UIManager.getDefaults();
        defaults.put("Slider.verticalThumbIcon", icon);
        url = ClassLoader.getSystemClassLoader().getResource("bmp/CursoreSlider11x16Red.png");
        icon = new ImageIcon(url);
        defaults.put("Slider.horizontalThumbIcon", icon);
        defaults.put("Slider.altTrackColor", Color.DARK_GRAY);
        defaults.put("Slider.tickColor", Color.WHITE);
    }

    public static void main(String[] args) {
//        pman = new ParameterManager();
        //Load New Cursor Slider Icon

//        URL url = ClassLoader.getSystemClassLoader().getResource("bmp/sh_slider3_knob_r.png");
        sliderDesign();

        //use the font
//        yourSwingComponent.setFont(customFont);
        ParameterManager.getInstance().initialize();
//        startDebug();     //Emily Debug

        
        wb = new Wb();
        wb.setVisible(true);

//        mydialogMidi = new MyDialogMidi(wb, false);
//        mydialogMidi.getTransmitter();
        
        ParameterManager.getInstance().idRequest.aggiungiObserver(wb);
        
        sendSysExcl = new SysExclQueue();
        sendSysExcl.start();
    }

    static public void startDebug() {

        System.out.println("Write EMPC_LEVEL to 0 by parameters:\n");
        COEmilStmPC aPC = new COEmilStmPC();
        aPC.Open(0);
        aPC.Write(0, (short) ParameterManager.getPrmId("EMPC_LEVEL"));
        aPC.Close(1);

        System.out.println("Write EMPC_LEVEL to 127 by Exc.");
        ParameterManager.gTestExcC.data = hexStringToByteArray("f0411000003c121f00000e7f54f7");
        ParameterManager.gVVPrmExcRx.ParseExcC(ParameterManager.gTestExcC.data);
        SendMidiExc(ParameterManager.gTestExcC.data, ParameterManager.gTestExcC.data.length);

        System.out.println("Read EMPC_LEVEL by parameters:\n");
        CIEmilStmPC aReadPC = new CIEmilStmPC();
        aReadPC.Open(0);
        int aValue = (int) aReadPC.ReadValue((short) ParameterManager.getPrmId("EMPC_LEVEL"));
        aReadPC.Close();
        System.out.format("%d\n", aValue);

        System.out.println("Identity Request:");
        ParameterManager.gVVPrmExcTx.SendIdentityRequest();
        //Simulazione risposta...
//        ParameterManager.gTestExcC.data = hexStringToByteArray("f07e7f0601f7");
//        ParameterManager.gVVPrmExcRx.ParseExcC(ParameterManager.gTestExcC.data);

        System.out.println("Send temp pat:");
        ParameterManager.gVVPrmExcTx.SendTempPat(0);

        System.out.println("Request patch common name:");
        ParameterManager.gVVPrmExcTx.SendRequestTempPC((short) ParameterManager.getPrmId("EMPC_NAME_FIRST"), (short) ParameterManager.getPrmId("EMPC_NAME_LAST"), 0);

        for (int i = 1; i < 257; i++) {
            System.out.println("Request patch user name 1..256:");
            ParameterManager.gVVPrmExcTx.SendRequestUserPC((short) ParameterManager.getPrmId("EMPC_NAME_FIRST"), (short) ParameterManager.getPrmId("EMPC_NAME_LAST"), i);
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Nomi patch. letti con stream user
        }

        System.out.println("Request all current patch");
        ParameterManager.gVVPrmExcTx.SendRequestTempPat(0);

// printf("Size of Emil Patch: %d\n", sizeof(SEmilPat));
        System.out.println("\nEmil test program end!");

    }

    public static void SendMidiExc(final byte[] theExc, final int theSize) {
//        System.out.format("Size: %d \n", theSize);
//
//        for (int i = 0; i < theSize; i++) {
//            System.out.format("0x%02X ", theExc[i]);
//        }
//        
//        System.out.format("\n");
//        sendToMidiDirect(theExc, theSize);
//        sendSysExcl.add(theExc, theSize);
        wb.getMyMidi().SendMidiExc(theExc, theSize);
    }

    public static void sendthisMessageToMidi(String msg, int length) {
        sendToMidiDirect(hexStringToByteArray(msg), length);
    }
    
    protected static void sendToMidiDirect(byte[] theExc, int theSize) {
        SysexMessage myMsg = new SysexMessage();
        try {
            //			Pitching on C(Do)
//                    byte[] dataArabicScale = new byte[]{(byte)0xF0, 0x41, 0x10 ,0x42 ,0x12 ,0x50 ,0x14 ,0x40 ,0x00 ,0x40 ,0x40 ,0x40 ,0x40 ,0x40 ,0x40 ,0x40 ,0x40 ,0x40 ,0x40 ,0x40 ,0x1c ,(byte)0xF7 };
            myMsg.setMessage(theExc, theSize);
        } catch (InvalidMidiDataException ex) {
            Logger.getLogger(Wb.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        for (int i = 0; i < theSize; i++) {
            System.out.format("0x%02X ", theExc[i]);
        }
        System.out.println();
        Receiver rcvr=null;
//        rcvr = Main.mydialogMidi.getReceiver();
        if (rcvr == null){
            Logger.getLogger(Wb.class.getName()).log(Level.INFO, "No receiver set! Is MIDI port connected?");
            return;
        }
        System.out.print("Send On Receiver:"+rcvr.toString()+" ]");
        rcvr.send(myMsg, -1);
        rcvr.close();
        System.out.println("System Exclusive sended!");
    }
    
}
