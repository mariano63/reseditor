package inframe.z_obsolete_patchsummary1;

import beans.JBeansFather;
import java.awt.Component;
import java.awt.Container;
import javax.swing.JInternalFrame;

/**
 *
 * @author mgiacomo
 */
public class JFPatchSummary1 extends JInternalFrame {

    /**
     * Creates new form JFFather
     */
    public JFPatchSummary1() {
        initComponents();
    }
    
    //***   Use this code to harvest, and make set do you need!
//    public void harvestJBeansFather(Container c) {
//        Component[] components = c.getComponents();
//        for(Component com : components) {
//            if(com instanceof JBeansFather) {
//                ;   //Do something...
//            } else if(com instanceof Container) {
//                harvestJBeansFather((Container) com);
//            }
//        }
//    }
    
    //Scan every component. And if One JBeansFather, change tonePatch variable.
    static public void setTonePatch(Container c, int tone){
        Component[] components = c.getComponents();
        for(Component com : components) {
            if(com instanceof JBeansFather) {
                ((JBeansFather) com).setTonePatch_0_3(tone);   //!!!
            } else if(com instanceof Container) {
                setTonePatch((Container) com, tone);
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panPatchName1 = new inframe.z_obsolete_patchsummary1.PanPatchName();
        panPatchCommon1 = new inframe.z_obsolete_patchsummary1.PanPatchCommon();
        panWG2 = new inframe.z_obsolete_patchsummary1.PanWG();
        panTVF2 = new inframe.z_obsolete_patchsummary1.PanTVF();
        panLF011 = new inframe.z_obsolete_patchsummary1.PanLF01();
        panSwitchesAndTones1 = new inframe.z_obsolete_patchsummary1.PanSwitchesAndTones();

        setTitle("PATCH SUMMARY");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panPatchName1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panSwitchesAndTones1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panTVF2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panLF011, javax.swing.GroupLayout.PREFERRED_SIZE, 499, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panPatchCommon1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panWG2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panPatchName1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(panSwitchesAndTones1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panPatchCommon1, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panWG2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panTVF2, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(panLF011, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)))
        );

        setBounds(0, 0, 1029, 679);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private inframe.z_obsolete_patchsummary1.PanLF01 panLF011;
    private inframe.z_obsolete_patchsummary1.PanPatchCommon panPatchCommon1;
    private inframe.z_obsolete_patchsummary1.PanPatchName panPatchName1;
    private inframe.z_obsolete_patchsummary1.PanSwitchesAndTones panSwitchesAndTones1;
    private inframe.z_obsolete_patchsummary1.PanTVF panTVF2;
    private inframe.z_obsolete_patchsummary1.PanWG panWG2;
    // End of variables declaration//GEN-END:variables
}
