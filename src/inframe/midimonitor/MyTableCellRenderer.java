
package inframe.midimonitor;

import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import midi.eventi.ControlChange;
import midi.eventi.Evento;
import midi.eventi.KeyPressure;
import midi.eventi.NotaOff;
import midi.eventi.NotaOn;
import midi.eventi.Pitch;
import midi.eventi.PolyTouch;
import midi.eventi.ProgramChange;
import midi.eventi.RealTimeF8;
import midi.eventi.SystemMessage;
import midi.eventi.Unknown;

/**
 *
 *
 * Install the custom renderer on the first visible column
	int vColIndex = 0;
	TableColumn col = table.getColumnModel().getColumn(vColIndex);
	col.setCellRenderer(new MyTableCellRenderer());
 * @author mgiacomo
 */// This renderer extends a component. It is used each time a
// cell must be displayed.
public class MyTableCellRenderer extends JLabel implements TableCellRenderer {
    // This method is called each time a cell in a column
    // using this renderer needs to be rendered.
	
	private final Map<Class, String> mp = new HashMap();

	public MyTableCellRenderer() {
		mp.put(NotaOn.class, "/inframe/midimonitor/png/Nota.png");
		mp.put(NotaOff.class, "/inframe/midimonitor/png/NotaOff.png");
		mp.put(ControlChange.class, "/inframe/midimonitor/png/CC.png");
		mp.put(ProgramChange.class, "/inframe/midimonitor/png/PC.png");
		mp.put(Pitch.class, "/inframe/midimonitor/png/PB.png");
		mp.put(KeyPressure.class, "/inframe/midimonitor/png/KP.png");
		mp.put(PolyTouch.class, "/inframe/midimonitor/png/PT.png");
		mp.put(RealTimeF8.class, "/inframe/midimonitor/png/F8.png");
		mp.put(SystemMessage.class, "/inframe/midimonitor/png/SYS.png");
		mp.put(Unknown.class, "/inframe/midimonitor/png/UNK.png");
	}
	
	@Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int rowIndex, int vColIndex) {
        // 'value' is value contained in the cell located at
        // (rowIndex, vColIndex)
		
		Evento e = ((MyAbstractTableModel)(table.getModel())).getEvento(rowIndex);
		if (vColIndex==1 && value != null){	//Midi channel	1..15
			java.net.URL url = getClass().getResource("/inframe/midimonitor/png/CH"+value+".png");
			setIcon( url!=null ? new javax.swing.ImageIcon(url) : null);
		}
		else if (vColIndex==2){	//Evento
			String icona = ((e.getClass()==NotaOn.class && e.getData2()==0)) ? mp.get(NotaOff.class) : mp.get(e.getClass()) ;
			setIcon( (icona==null) ? null : new javax.swing.ImageIcon(getClass().getResource(icona)));
		}
		
		setOpaque(isSelected);
		setBackground(isSelected ? Color.MAGENTA : table.getBackground());
		setForeground(isSelected ? Color.CYAN : table.getForeground());

		// this cell is the anchor and the table has the focus
        if (hasFocus) {}

        // Configure the component with the specified value
        setText(value.toString());

        // Set tool tip if desired
        setToolTipText((String)value);

        // Since the renderer is a component, return itself
        return this;
    }

    // The following methods override the defaults for performance reasons
	@Override
    public void validate() {}
	@Override
    public void revalidate() {}
	@Override
    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {}
	@Override
    public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {}
}

