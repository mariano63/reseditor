package inframe.midimonitor;

import midi.eventi.Evento;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author mgiacomo
 */
public class MyAbstractTableModel extends AbstractTableModel{
	static public final String[] header = {"Time","Channel","Event","Data 1","Data 2"};

	ArrayList<Evento> al = new ArrayList<Evento>();
	TimerFire tf;
	public MyAbstractTableModel() { tf = new TimerFire(); }
	
	@Override
	public int getRowCount() {	return al.size();}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		String[] str = al.get(rowIndex).objectsFromEvento();
		return((columnIndex>=str.length) ? "" : str[columnIndex]);
	}
	
	@Override
	public String getColumnName(int column) {	return header[column];	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {	return true;}
	
	@Override
	public int getColumnCount() {	return header.length; }

	public void removAll(){
		getAl().clear();
		fireTableDataChanged();
	}
	public ArrayList<Evento> getAl() {	return al;}
	
	public Evento getEvento(int index) { return al.get(index);}
	public Evento getPrimoEvento() { return al.get(al.size()-1);}
	
	public void setEvento(int index, Evento ev) {
		al.set(index, ev);
		fireTableRowsUpdated(index,index);		//1)
	}

	public void setAl(ArrayList<Evento> al) {
		this.al = al;
		tf.set(0, getRowCount()-1);
		tf.start();
	}

	public void addElement(Evento o){

		//Riportiamo Time Stamp evento precedente in quello attuale per eventuali elaborazioni e visualizzazioni
		if (al.size()>=1){
			long oo = al.get(0).getTimeStamp();
			o.setOldTimeStamp( oo );
		}
		al.add(0,o);
		//scommentare 1) e commentare 2) se si vuole una risposta immediata
		//ai cambiamenti

		fireTableRowsInserted(0, 0);									//1)

		//Potevo visualizzare l'evento subito, ma sono un perfezionista, uso il timer.
		//scommentare 2) e commentare 1) se si intende usare il timer
		// per visualizzare i dati in maniera piu' rilassata.
		
//		tf.set(0, 0);													//2)
//		tf.start();														//2)
	}


	/**
	 * timer class per lancio fire
	 */
	class TimerFire {
		Timer timer;
		int low, high;		//il fire viene lanciato a partire da low, fino ad high.
		int delay;

		public TimerFire() {
			low=high=0;
			delay = 250;	//250msec. is the default time.
			ActionListener taskPerformer = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent evt) {
					fireTableDataChanged();
					low=high=0;
					timer.stop();
				}
			};
			timer = new Timer(delay, taskPerformer);
		}
		void start(){
			if (high==0)	return;
			timer.start();
		}
		void set(int low, int high){
			if (low<this.low)	this.low = low;
			if (high>this.high)	this.high = high;
		}

		/**
		 * set milliseconds delay for fireEvent.
		 * @param delay
		 */
		public void setDelay(int delay) {	this.delay = delay;	}
	}

}
