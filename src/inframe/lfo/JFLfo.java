package inframe.lfo;

import inframe.tvf.*;

/**
 *
 * @author mgiacomo
 */
public class JFLfo extends javax.swing.JInternalFrame {

    /**
     * Creates new form JFPatchSummary
     */
    public JFLfo() {
        initComponents();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        panelImgScaled1 = new beans.panels.PanelImgScaled();
        panGeneric2 = new beans.panels.PanGeneric();
        panLFO11 = new inframe.lfo.PanLFO1();
        panGeneric1 = new beans.panels.PanGeneric();
        panLFO21 = new inframe.lfo.PanLFO2();
        panGeneric3 = new beans.panels.PanGeneric();
        panStep2 = new inframe.lfo.PanStep();
        panSwitchesAndTones2 = new inframe.patchsummary2.PanSwitchesAndTones();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("PATCH TVF");

        panelImgScaled1.setBackgroundIcon(new javax.swing.ImageIcon(getClass().getResource("/beans/panels/bmp/sfondo_app_01.png"))); // NOI18N

        panGeneric2.setMatteTitle(" LFO1 ");
        panGeneric2.setMatteTitleFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        panGeneric2.setUseMatteColor(true);

        javax.swing.GroupLayout panGeneric2Layout = new javax.swing.GroupLayout(panGeneric2);
        panGeneric2.setLayout(panGeneric2Layout);
        panGeneric2Layout.setHorizontalGroup(
            panGeneric2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panGeneric2Layout.createSequentialGroup()
                .addComponent(panLFO11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panGeneric2Layout.setVerticalGroup(
            panGeneric2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panGeneric2Layout.createSequentialGroup()
                .addComponent(panLFO11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        panGeneric1.setMatteTitle(" LFO2 ");
        panGeneric1.setUseMatteColor(true);

        javax.swing.GroupLayout panGeneric1Layout = new javax.swing.GroupLayout(panGeneric1);
        panGeneric1.setLayout(panGeneric1Layout);
        panGeneric1Layout.setHorizontalGroup(
            panGeneric1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panGeneric1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(panLFO21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panGeneric1Layout.setVerticalGroup(
            panGeneric1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panGeneric1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(panLFO21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panGeneric3.setMatteTitle("");
        panGeneric3.setUseMatteColor(true);

        javax.swing.GroupLayout panGeneric3Layout = new javax.swing.GroupLayout(panGeneric3);
        panGeneric3.setLayout(panGeneric3Layout);
        panGeneric3Layout.setHorizontalGroup(
            panGeneric3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panGeneric3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panStep2, javax.swing.GroupLayout.PREFERRED_SIZE, 896, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panGeneric3Layout.setVerticalGroup(
            panGeneric3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panGeneric3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panStep2, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );

        panSwitchesAndTones2.setMatteTitle("");
        panSwitchesAndTones2.setMatteTitleFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        panSwitchesAndTones2.setMatteTop(12);
        panSwitchesAndTones2.setWidgetPanSwitchVisible(false);

        javax.swing.GroupLayout panelImgScaled1Layout = new javax.swing.GroupLayout(panelImgScaled1);
        panelImgScaled1.setLayout(panelImgScaled1Layout);
        panelImgScaled1Layout.setHorizontalGroup(
            panelImgScaled1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImgScaled1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelImgScaled1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImgScaled1Layout.createSequentialGroup()
                        .addGroup(panelImgScaled1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelImgScaled1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(panGeneric1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(panGeneric3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(panSwitchesAndTones2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(panGeneric2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelImgScaled1Layout.setVerticalGroup(
            panelImgScaled1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImgScaled1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panSwitchesAndTones2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(panGeneric2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panGeneric1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panGeneric3, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(130, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(panelImgScaled1);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JScrollPane jScrollPane1;
    private beans.panels.PanGeneric panGeneric1;
    private beans.panels.PanGeneric panGeneric2;
    private beans.panels.PanGeneric panGeneric3;
    private inframe.lfo.PanLFO1 panLFO11;
    private inframe.lfo.PanLFO2 panLFO21;
    private inframe.lfo.PanStep panStep2;
    private inframe.patchsummary2.PanSwitchesAndTones panSwitchesAndTones2;
    private beans.panels.PanelImgScaled panelImgScaled1;
    // End of variables declaration//GEN-END:variables
}
