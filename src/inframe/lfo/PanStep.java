/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inframe.lfo;

/**
 *
 * @author mgiacomo
 */
public class PanStep extends javax.swing.JPanel {

    /**
     * Creates new form PanStep
     */
    public PanStep() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        switchOnOffMetal2 = new beans.switches.SwitchOnOffMetal();
        panGeneric1 = new beans.panels.PanGeneric();
        sliderEmilPanelAToB1 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB2 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB3 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB5 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB6 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB7 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB8 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB9 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB10 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB11 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB12 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB13 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB14 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB15 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB16 = new beans.sliders.SliderEmilPanelAToB();
        sliderEmilPanelAToB17 = new beans.sliders.SliderEmilPanelAToB();

        setOpaque(false);

        switchOnOffMetal2.setEmilId("EMPT_LFO_STEP_TYPE");
        switchOnOffMetal2.setWidgetTitle("STEP TYPE");
        switchOnOffMetal2.setWidgetTitleOff("TYPE 2");
        switchOnOffMetal2.setWidgetTitleOn("TYPE 1");
        switchOnOffMetal2.setWidgetVisibleValue(false);

        panGeneric1.setMatteTitle(" STEP ");
        panGeneric1.setMatteTitleFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        panGeneric1.setUseMatteColor(true);

        sliderEmilPanelAToB1.setEmilId("EMPT_LFO_STEP1");
        sliderEmilPanelAToB1.setWidgetTitle("1");

        sliderEmilPanelAToB2.setEmilId("EMPT_LFO_STEP2");
        sliderEmilPanelAToB2.setWidgetTitle("2");

        sliderEmilPanelAToB3.setEmilId("EMPT_LFO_STEP3");
        sliderEmilPanelAToB3.setWidgetTitle("3");

        sliderEmilPanelAToB5.setEmilId("EMPT_LFO_STEP4");
        sliderEmilPanelAToB5.setWidgetTitle("4");

        sliderEmilPanelAToB6.setEmilId("EMPT_LFO_STEP5");
        sliderEmilPanelAToB6.setWidgetTitle("5");

        sliderEmilPanelAToB7.setEmilId("EMPT_LFO_STEP6");
        sliderEmilPanelAToB7.setWidgetTitle("6");

        sliderEmilPanelAToB8.setEmilId("EMPT_LFO_STEP7");
        sliderEmilPanelAToB8.setWidgetTitle("7");

        sliderEmilPanelAToB9.setEmilId("EMPT_LFO_STEP8");
        sliderEmilPanelAToB9.setWidgetTitle("8");

        sliderEmilPanelAToB10.setEmilId("EMPT_LFO_STEP9");
        sliderEmilPanelAToB10.setWidgetTitle("9");

        sliderEmilPanelAToB11.setEmilId("EMPT_LFO_STEP10");
        sliderEmilPanelAToB11.setWidgetTitle("10");

        sliderEmilPanelAToB12.setEmilId("EMPT_LFO_STEP11");
        sliderEmilPanelAToB12.setWidgetTitle("11");

        sliderEmilPanelAToB13.setEmilId("EMPT_LFO_STEP12");
        sliderEmilPanelAToB13.setWidgetTitle("12");

        sliderEmilPanelAToB14.setEmilId("EMPT_LFO_STEP13");
        sliderEmilPanelAToB14.setWidgetTitle("13");

        sliderEmilPanelAToB15.setEmilId("EMPT_LFO_STEP14");
        sliderEmilPanelAToB15.setWidgetTitle("14");

        sliderEmilPanelAToB16.setEmilId("EMPT_LFO_STEP15");
        sliderEmilPanelAToB16.setWidgetTitle("15");

        sliderEmilPanelAToB17.setEmilId("EMPT_LFO_STEP16");
        sliderEmilPanelAToB17.setWidgetTitle("16");

        javax.swing.GroupLayout panGeneric1Layout = new javax.swing.GroupLayout(panGeneric1);
        panGeneric1.setLayout(panGeneric1Layout);
        panGeneric1Layout.setHorizontalGroup(
            panGeneric1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panGeneric1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sliderEmilPanelAToB1, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB2, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB3, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB5, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB6, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB7, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB8, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB9, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB10, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB11, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB12, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB13, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB14, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB15, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB16, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliderEmilPanelAToB17, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addContainerGap())
        );
        panGeneric1Layout.setVerticalGroup(
            panGeneric1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sliderEmilPanelAToB2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sliderEmilPanelAToB17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(switchOnOffMetal2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panGeneric1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(switchOnOffMetal2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panGeneric1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private beans.panels.PanGeneric panGeneric1;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB1;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB10;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB11;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB12;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB13;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB14;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB15;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB16;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB17;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB2;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB3;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB5;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB6;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB7;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB8;
    private beans.sliders.SliderEmilPanelAToB sliderEmilPanelAToB9;
    private beans.switches.SwitchOnOffMetal switchOnOffMetal2;
    // End of variables declaration//GEN-END:variables
}
