
package emil;

/**
 *
 * @author mgiacomo
 */
public class STypeIV {			// data base of type IV
		int    modelID;		//  model ID
		byte   adrsSize;		//  address size (1 - 4)

        public STypeIV(int modelID, byte adrsSize) {
            this.modelID = modelID;
            this.adrsSize = adrsSize;
        }

        public int getModelID() {
            return modelID;
        }

        public void setModelID(int modelID) {
            this.modelID = modelID;
        }

        public byte getAdrsSize() {
            return adrsSize;
        }

        public void setAdrsSize(byte adrsSize) {
            this.adrsSize = adrsSize;
        }
        
}
