package emil;

import java.util.Arrays;

/**
 *
 * @author mgiacomo
 */
public class CEXCH {

    final public static int ErrRxData = 0;					//  receive data error
    final public static int ErrTxSysEx = 1;					//  transmitting exclusive (not error)
    final public static int ErrUserProtect = 2;					//  user memory write protected
    final public static int ErrCheckSum = 3;					//  exclusive check sum
    static public final int ManuRoland = 0x41;
    static public final int UnivNonRealTime = 0x7E;			//  universal non-real time message
    static public final int UnivRealTime = 0x7F;			//  universal real time message
    static public final int DeviceBroadcast = 0x7F;			//  broadcast
    static public final int CommandRQ1 = 0x0011;		//  RQ1
    static public final int CommandDT1 = 0x0012;		//  DT1
    static public final int ModelAX = 0x023C;		//  AX Synth    
    public CExcData mExcC = new CExcData();
    public SInfoRoland mInfoRoland = new SInfoRoland();
    public static STypeIV[] mTypeIVTbl = {
        new STypeIV(ModelAX, (byte) 4),
        new STypeIV(0, (byte) 0)
    };

    public void VEXCHRolandRQ1DataError(byte deviceID, int modelID) {
        switch (modelID) {
            case ModelAX:
                SendErrMesg(ErrRxData);
                break;
            default:
                break;
        }
    }

    public void VEXCHRolandRQ1SumError(byte deviceID, int modelID, SInfoRQ1 rq1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void VEXCHRolandRQ1(byte deviceID, int modelID, SInfoRQ1 rq1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void VEXCHRolandDT1DataError(byte deviceID, int modelID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void VEXCHRolandDT1SumError(byte deviceID, int modelID, SInfoDT1 dt1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void VEXCHRolandDT1(byte deviceID, int modelID, SInfoDT1 dt1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    static public void SendErrMesg(int ErrRxData) {
    }

    public class SInfoDT1 {
        //  DT1 (roland type IV)

        long adrs;			//   address
        byte[] data;			//   pointer of the data
        int length;			//   data length        
    }

    public class SInfoRoland {					// roland exclusive format

        byte deviceID;		//  device ID
        int modelID;    //  model ID (MSB is the size of zero data)
        int commandID;  //  command ID (MSB is the size of zero data)
        byte adrsSize;		//  address size
//		union {         ??
        SInfoDT1 dt1 = new SInfoDT1();			//  DT1
        SInfoRQ1 rq1 = new SInfoRQ1();			//  RQ1
//		};
    };
//--------------------------------------------------------------
// ParseExcC
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Parse exclusive.
//	entry:	theExcC		SDataC format data
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
    public void ParseExcC(byte[] theExcC) {
//	mExcC = theExcC;
        mExcC.data = theExcC;
//	int length = theExcC->length;
        int length = mExcC.data.length;

//	const unsigned char* datap = theExcC->data;

        if (length > CExcData.ExcSizeMax) {		// too big size?
            VEXCHFormatError(mExcC);
            return;
        }
        byte[] datap = new byte[length];
        System.arraycopy(mExcC.data, 0, datap, 0, length);

        if (length-- <= 0) {				// no data?
            VEXCHFormatError(mExcC);
            return;
        }
//	if (*(datap + length) != 0xF7) {	// no EOX?
        if (MainDebug.toCmp(datap[length]) != 0xF7) {	// no EOX?
            VEXCHFormatError(mExcC);
            return;
        }
//	if (*datap++ != 0xF0) {		// no status?
        if (MainDebug.toCmp(datap[0]) != 0xF0) {		// no status?
            VEXCHFormatError(mExcC);
            return;
        }

        if (length-- <= 0) {				// no next data?
            VEXCHFormatError(mExcC);
            return;
        }

//	switch (*datap++) {				// manufacturer ID
        switch (datap[1]) {				// manufacturer ID
            case ManuRoland:
                ParseRoland(length, datap);
                break;
            case UnivRealTime:
                ParseRealTime(length, datap);
                break;
            case UnivNonRealTime:
                ParseNonRealTime(length, datap);
                break;
            default:
                VEXCHUnknown(mExcC);
                break;
        }
    }

//--------------------------------------------------------------
// ParseRoland
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Parse roland exclusive.
//	entry:	theLength	length
//		theData		data
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
    public void ParseRoland(int theLength, byte[] theData) {
        int ndx = 2;
        if ((theLength-- <= 0) || IsStatusByte(theData[ndx])) {
            VEXCHRolandUnknown(mExcC);
            return;
        }
//	mInfoRoland.deviceID = *theData++;
        mInfoRoland.deviceID = theData[ndx++];

        int modelID = 0;

        for (;;) {
            if ((theLength-- <= 0) || IsStatusByte(theData[ndx])) {
                VEXCHRolandUnknown(mExcC);
                return;
            }
            byte data = theData[ndx++];
            if (data != 0x00) {
                modelID |= data;
                break;
            }
            modelID += 0x100;
        }
        mInfoRoland.modelID = modelID;

        int ij = 0;
        while (mTypeIVTbl[ij].getModelID() != modelID) {
            if ((mTypeIVTbl[ij++]).getModelID() == 0) {
                VEXCHRolandUnknown(mExcC);
                return;
            }
        }
        mInfoRoland.adrsSize = mTypeIVTbl[ij].getAdrsSize();

        int commandID = 0;
        for (;;) {
            if ((theLength-- <= 0) || IsStatusByte(theData[ndx])) {
                VEXCHRolandUnknown(mExcC);
                return;
            }
            int data = theData[ndx++];
            if (data != 0x00) {
                commandID = commandID | data;
                break;
            }
            commandID = commandID + 0x100;
        }
        mInfoRoland.commandID = commandID;

        switch (commandID) {
            case CommandRQ1:
                ParseRolandRQ1(theLength, theData, ndx);
                return;
            case CommandDT1:
                ParseRolandDT1(theLength, theData, ndx);
                return;
            default:
                VEXCHRolandUnknown(mExcC);
        }
    }

//==============================================================
// Parse exclusive.
//==============================================================
//--------------------------------------------------------------
// ParseRolandDT1
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Parse roland exclusive DT1.
//	entry:	theLength	length
//		theData		data
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
    public void ParseRolandDT1(int theLength, byte[] theData, int ndx) {
        long adrs;
        int i;
        for (adrs = 0, i = mInfoRoland.adrsSize; i > 0; i--) {
            if ((theLength-- <= 0) || IsStatusByte(theData[ndx])) {	// address too short?
                VEXCHRolandDT1DataError(mInfoRoland.deviceID, mInfoRoland.modelID);
                return;
            }
            adrs = (adrs << 7) | theData[ndx++];
        }
        mInfoRoland.dt1.adrs = adrs;

        if (theLength <= 1 + 1) {					// no data byte? (check sum + EOX)
            VEXCHRolandDT1DataError(mInfoRoland.deviceID, mInfoRoland.modelID);
            return;
        }
        byte[] nuovoData = Arrays.copyOfRange(theData, ndx, theData.length);
        mInfoRoland.dt1.data = nuovoData;
        mInfoRoland.dt1.length = theLength - 2;
        if (CheckRolandSum(mExcC) != 0) {				// check sum error?
            VEXCHRolandDT1SumError(mInfoRoland.deviceID, mInfoRoland.modelID, mInfoRoland.dt1);
            return;
        }
        VEXCHRolandDT1(mInfoRoland.deviceID, mInfoRoland.modelID, mInfoRoland.dt1);
    }

//--------------------------------------------------------------
// ParseRolandRQ1
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Parse roland exclusive RQ1.
//	entry:	theLength	length
//		theData		data
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
    public void ParseRolandRQ1(int theLength, byte[] theData, int ndx) {
        long adrs;
        int i;
        for (adrs = 0, i = mInfoRoland.adrsSize; i > 0; i--) {
            if ((theLength-- <= 0) || IsStatusByte(theData[ndx])) {	// address too short?
                VEXCHRolandRQ1DataError(mInfoRoland.deviceID, mInfoRoland.modelID);
                return;
            }
            adrs = (adrs << 7) | theData[ndx++];
        }
        mInfoRoland.rq1.adrs = adrs;

        for (adrs = 0, i = mInfoRoland.adrsSize; i > 0; i--) {
            if ((theLength-- <= 0) || IsStatusByte(theData[ndx])) {	// size too short?
                VEXCHRolandRQ1DataError(mInfoRoland.deviceID, mInfoRoland.modelID);
                return;
            }
            adrs = (adrs << 7) | theData[ndx++];
        }
        mInfoRoland.rq1.size = adrs;

        if ((theLength-- <= 0) || IsStatusByte(theData[ndx++])) {	// no check sum?
            VEXCHRolandRQ1DataError(mInfoRoland.deviceID, mInfoRoland.modelID);
            return;
        }
        if ((theLength-- <= 0) || (theData[ndx] != 0xF7)) {	// no EOX?
            VEXCHRolandRQ1DataError(mInfoRoland.deviceID, mInfoRoland.modelID);
            return;
        }
        if (CheckRolandSum(mExcC) != 0) {				// check sum error?
            VEXCHRolandRQ1SumError(mInfoRoland.deviceID, mInfoRoland.modelID, mInfoRoland.rq1);
            return;
        }
        VEXCHRolandRQ1(mInfoRoland.deviceID, mInfoRoland.modelID, mInfoRoland.rq1);
    }

    public void VEXCHRolandUnknown(CExcData theExcC) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void VEXCHRealTimeUnknown(CExcData theExcC) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

    public void VEXCHNonRealTimeUnknown(CExcData theExcC) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void VEXCHFormatError(CExcData mExcC) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void VEXCHUnknown(CExcData mExcC) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void ParseRealTime(int length, byte[] datap) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void VEXCHIdentityRequest(int theDevID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public void VEXCHIdentityReply(int theDevID, int theManuID, byte[] theData) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
//--------------------------------------------------------------
// ParseNonRealTime
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Parse universal non-real time message.
//	entry:	theLength	length
//		theData		data
//	retrn:	exclusive type
//	remks:	---
//
//--------------------------------------------------------------
    public void ParseNonRealTime(int theLength, byte[] theData) {

        int ndx = 2;
        if ((theLength-- <= 0) || IsStatusByte(theData[ndx])) {
            VEXCHNonRealTimeUnknown(mExcC);
            return;
        }
        byte devID =  theData[ndx++];

        if ((theLength-- <= 0) || IsStatusByte( theData[ndx])) {
            VEXCHNonRealTimeUnknown(mExcC);
            return;
        }
        byte subID1 =  theData[ndx++];

        if ((theLength-- <= 0) || IsStatusByte(theData[ndx])) {
            VEXCHNonRealTimeUnknown(mExcC);
            return;
        }
        byte subID2 =  theData[ndx++];

        int subID = (subID1 << 8) | subID2;
        byte manuID;
        switch (subID) {
            case 0x0601:						// identity request (06 01)
                if ((theLength-- <= 0) || ( theData[ndx] != (byte)0xF7)) {
                    break;
                }
                switch (subID) {
                    case 0x0601:
                        VEXCHIdentityRequest(devID);
                        return;
                    default:
                        break;
                }
                break;
            case 0x0602:						// identity reply (06 02 ...)		
                if ((theLength-- <= 0) || IsStatusByte( theData[ndx])) {
                    break;
                }
                manuID =  theData[ndx++];				// manufacturer ID
                if (theLength != 8 + 1) {			// invalid data size (code 8 byte + EOX)
                    break;
                }
                VEXCHIdentityReply(devID, manuID, theData);
                return;
            default:
                break;
        }
        VEXCHNonRealTimeUnknown(mExcC);
    }

    static public boolean IsStatusByte(byte theData) {		// test if status byte
        return (MainDebug.toCmp(theData) >= 0x80);
    }

    static public boolean IsDataByte(byte theData) {		// test if data byte
        return (MainDebug.toCmp(theData) < 0x80);
    }


//==============================================================
// Utilities.
//==============================================================
//--------------------------------------------------------------
// CheckRolandSum
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Check roland sum.
//	entry:	theExcC		SDataC format data
//	retrn:	check sum
//	remks:	---
//
//--------------------------------------------------------------
    public static byte CheckRolandSum(CExcData theExcC) {
        int ndx = 3;
        byte[] datap = theExcC.data;
        while (datap[ndx++] == 0x00) {			// model ID
            // nop
        }
        while (datap[ndx++] == 0x00) {			// command ID
            // nop
        }
        byte sum = 0;
        while (IsDataByte(datap[ndx])) {
            sum += datap[ndx++];
        }
        return (byte) (sum & 0x7F);
    }

//--------------------------------------------------------------
// SetDT1Header
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Set the header of the DT1.
//	entry:	theExcC		SDataC format data
//		theModelID	model ID
//		theDevID	device ID
//		theAdrs		address
//	retrn:	pointer of the data
//	remks:	---
//
//--------------------------------------------------------------
    public static byte[] SetDT1Header(CExcData theExcC, short theModelID, byte theDevID, long theAdrs) {
        int ndxDatap = 0;
        byte[] datap = theExcC.data;
        datap[ndxDatap++] = (byte) 0xF0;			// status
        datap[ndxDatap++] = ManuRoland;				// manufacturer ID
        datap[ndxDatap++] = theDevID;				// device ID

        int i;
        for (i = 0; theModelID != mTypeIVTbl[i].modelID; i++) {
            if (mTypeIVTbl[i].modelID == 0) {
                break;				// for safety
            }
        }
        int adrsSize = mTypeIVTbl[i].adrsSize;

        for (; theModelID > 0x100; theModelID -= 0x100) {
            datap[ndxDatap++] = 0x00;
        }
        datap[ndxDatap++] = (byte) theModelID;				// model ID
        datap[ndxDatap++] = CommandDT1;				// command ID

        while (adrsSize-- > 0) {			// address
            datap[ndxDatap++] = (byte) ((theAdrs >> (adrsSize * 7)) & 0x7F);
        }
        theExcC.length = ndxDatap;
        return datap;
    }

//--------------------------------------------------------------
// SetDT1Footer
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Set check sum & data length of the DT1.
//	entry:	theExcC		SDataC format data
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
    public static void SetDT1Footer(CExcData theExcC) {
        int ndx = 3;
        byte[] datap = theExcC.data;
        int length = 3;					// status & manufacturer ID & device ID

        while (datap[ndx++] == 0x00) {			// model ID
            length++;
        }
        while (datap[ndx++] == 0x00) {			// command ID
            length++;
        }
        length += 2;					// model ID & command ID

        char sum = 0;
        while (IsDataByte(datap[ndx])) {
            length++;
            sum += datap[ndx++];
        }
        length += 1;					// EOX
        ndx--;					// back to check sum
        datap[ndx] = (byte) ((0x100 - (sum - datap[ndx])) & 0x7F);

        theExcC.length = length;
    }

//--------------------------------------------------------------
// CheckDumpSum
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Check sample dump sum.
//	entry:	theExcC		SDataC format data
//	retrn:	check sum
//	remks:	---
//
//--------------------------------------------------------------
    public static byte CheckDumpSum(CExcData theExcC) {
        int ndx = 1;
        byte[] datap = theExcC.data;
        byte sum = 0;
        while (IsDataByte(datap[ndx])) {
            sum ^= datap[ndx++];
        }
        return (byte) (sum & 0x7F);
    }
//--------------------------------------------------------------
// SetDumpSum
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Set sample dump sum.
//	entry:	theExcC		SDataC format data
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
    public static void SetDumpSum(CExcData theExcC) {
        byte[] datap = theExcC.data;
        int ndx = 1;                //skyp status
        byte sum = 0;
        while (IsDataByte(datap[ndx])) {
            sum ^= datap[ndx++];
        }
        ndx--;					// back to check sum
        datap[ndx] = (byte) ((sum ^ datap[ndx]) & 0x7F);
    }

    public static byte[] SetRQ1Header(CExcData theExcC, short theModelID, byte theDevID, long theAdrs, long theSize) {
        byte[] datap = theExcC.data;
        int ndx = 0;
        datap[ndx++] = (byte) 0xF0;			// status
        datap[ndx++] = ManuRoland;				// manufacturer ID
        datap[ndx++] = theDevID;				// device ID

        int i;
        for (i = 0; theModelID != mTypeIVTbl[i].modelID; i++) {
            if (mTypeIVTbl[i].modelID == 0) {
                break;				// for safety
            }
        }
        int adrsSize = mTypeIVTbl[i].adrsSize;

        for (; theModelID > 0x100; theModelID -= 0x100) {
            datap[ndx++] = 0x00;
        }
        datap[ndx++] = (byte) theModelID;				// model ID
        datap[ndx++] = CommandRQ1;				// command ID

        while (adrsSize-- > 0) {			// address
            datap[ndx++] = (byte) ((theAdrs >> (adrsSize * 7)) & 0x7F);
        }

        int sizeSize = 4;
        while (sizeSize-- > 0) {			// address
            datap[ndx++] = (byte) ((theSize >> (sizeSize * 7)) & 0x7F);
        }
        
        theExcC.length = ndx;
        return datap;
    }

    public static void SetRQ1Footer(CExcData theExcC) {
        byte[] datap = theExcC.data;
        // status, 	// manufacturer ID, 	// device ID
        int ndx = 3;
        int length = 3;					// status & manufacturer ID & device ID

        while (datap[ndx++] == 0x00) {			// model ID
            length++;
        }
        while (datap[ndx++] == 0x00) {			// command ID
            length++;
        }
        length += 2;					// model ID & command ID

        byte sum = 0;
        while (IsDataByte(datap[ndx])) {
            length++;
            sum += datap[ndx++];
        }
        length += 1;					// EOX
        ndx--;					// back to check sum
        datap[ndx] = (byte) ((0x100 - (sum - datap[ndx])) & 0x7F);

        theExcC.length = length;
    }
}
