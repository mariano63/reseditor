package emil;

import config.ParameterManager;
import static emil.CEmilStm.efxBufTbl;

/**
 *
 * @author mgiacomo
 */
public class COEmilStm extends CEmilStm {

    byte[] mBuf;
    byte mProtect;
    short mMinID;
    short mMaxID;

    public void Open(int i){
        throw new UnsupportedOperationException("no poly?"); }   // Polymorph //Mariano
    public void Open(int i, int i0) {
        throw new UnsupportedOperationException("no poly?"); 
    }
    public void Close(int i){
        throw new UnsupportedOperationException("no poly?"); }   // Polymorph //Mariano
    
    private void BlockRead(long[] theValue, short theID, int theTimes) {
        int ndx = 0;
        while (theTimes-- > 0) {
            theValue[ndx++] = ReadValue(theID++);
        }
    }

    private void BlockRead(long[] theValue, short theID, short theOfstID, int theTimes) {
        int ndx = 0;
        while (theTimes-- > 0) {
            theValue[ndx++] = ReadSubValue(theID++, theOfstID);
        }
    }

    private void BlockWrite(long[] theValue, short theID, int theTimes) {
        int ndx = 0;
        while (theTimes-- > 0) {
            Write(theValue[ndx]++, theID++);
        }
    }

    private void BlockWrite(long[] theValue, short theID, short theOfstID, int theTimes) {
        int ndx = 0;
        while (theTimes-- > 0) {
            Write(theValue[ndx++], theID++, theOfstID);
        }
    }

    public long ReadValue(short theID) {
        return CEmilStm.ReadValue(mBuf, theID);
    }

    public long ReadSubValue(short theID, short theOfstID) {
        return CEmilStm.ReadSubValue(mBuf, theID, theOfstID);
    }

    private void StoreID(short theID) {
        if (mMinID > theID) {
            mMinID = theID;
        }
        if (mMaxID < theID) {
            mMaxID = theID;
        }
    }

    private void StoreID(short theID, int theSize) {
        if (theSize <= 0) {
            return;
        }
        if (mMinID > theID) {
            mMinID = theID;
        }
        if (mMaxID < theID + theSize - 1) {
            mMaxID = (short) (theID + theSize - 1);
        }
    }
	protected void SetBuf(byte[] theBuf) {
		mBuf = theBuf;
		mMinID = Short.MAX_VALUE;
		mMaxID = 0;
		mProtect = 0;
	}
	protected void SetProtect(int theSw) { mProtect = (byte) theSw; }
    public short GetMinID() { return mMinID; }
	public short GetMaxID() { return mMaxID; }
	public int GetSizeofID() { return mMaxID - mMinID + 1; }

//--------------------------------------------------------------
// Clear
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Initialize parameter.
//	entry:	theID		parameter ID
//		theTimes	number of times
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
protected void Clear(short theID, int theTimes)
{
	if (mBuf == null) {
		return;
	}
	short excID;
	{
		excID = ParameterManager.gEmilPrm.EmilClearValue(mBuf, theID, theTimes);
	}
	StoreID(excID, theTimes);
}
protected void Clear(short theID, short theOfstID, int theTimes)
{
	if (mBuf == null) {
		return;
	}
	short excID;
	{
		if (theID >= ParameterManager.getPrmId("EM_SUBPRMID_MIN") ) {
			excID = ParameterManager.gEmilPrm.EmilClearSubValue(mBuf, theOfstID, theID, theTimes);
		}
		else {
			excID = ParameterManager.gEmilPrm.EmilClearValue(mBuf, theID, theTimes);
		}
	}

	int size = theTimes;
    if (
        (ParameterManager.getPrmId("EMPF_EFX_SUB") == excID) ||
        (ParameterManager.getPrmId("EMRF_EFX_SUB") == excID)
        )
    {
		size = (theTimes - 4) * 4 + 4;
    }
    else if (
        (ParameterManager.getPrmId("EMPH_CHO_PRM1") == excID) ||
        (ParameterManager.getPrmId("EMPV_REV_PRM1") == excID) ||
        (ParameterManager.getPrmId("EMRH_CHO_PRM1") == excID) ||
        (ParameterManager.getPrmId("EMRV_REV_PRM1") == excID)
            )
    {
		size = theTimes * 4;
    }
    
	size = (size > theTimes) ? size : theTimes;
	StoreID(excID, size);
}
    
//--------------------------------------------------------------
// ClearEfxBuf
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Clear saved flag.
//	entry:	void
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
protected void ClearEfxBuf()
{
	for (int i = 0; i < ParameterManager.getPrmId("EMEFX_TYPE_SIZE"); i++) {
		efxSaved[i] = false;
	}
}
protected void ClearChoBuf()
{
	for (int i = 0; i < ParameterManager.getPrmId("EMCHO_TYPE_SIZE"); i++) {
		choSaved[i] = false;
	}
}
protected void ClearRevBuf()
{
	for (int i = 0; i < ParameterManager.getPrmId("EMREV_TYPE_SIZE"); i++) {
		revSaved[i] = false;
	}
}

//--------------------------------------------------------------
// SaveEfxBuf
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Save sub parameters.
//	entry:	PrmValue	type
//		theOfstID	offset parameter ID
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
public void SaveEfxBuf(int theType, short theOfstID)
{
	if (theType >= ParameterManager.getPrmId("EMEFX_TYPE_SIZE") ) {		// for safety
		return;
	}
//	BlockRead(efxBufTbl[theType], efxPrmTbl[theType].prmID, theOfstID, efxPrmTbl[theType].times);
	BlockRead(efxBufTbl[theType],(short) (CEmilStm.readTypeMinEfx(theType)), theOfstID, (int) (CEmilStm.readTypeMaxEfx(theType)-CEmilStm.readTypeMinEfx(theType)));
	efxSaved[theType] = true;
}
public void SaveChoBuf(int theType, short theOfstID)
{
	if (theType >= ParameterManager.getPrmId("EMCHO_TYPE_SIZE")) {		// for safety
		return;
	}
	BlockRead(choBufTbl[theType], (short) (CEmilStm.readTypeMinCho(theType)),theOfstID, (int) (CEmilStm.readTypeMaxCho(theType)-CEmilStm.readTypeMinCho(theType)));
	choSaved[theType] = true;
}
public void SaveRevBuf(int theType, short theOfstID)
{
	if (theType >= ParameterManager.getPrmId("EMREV_TYPE_SIZE")) {		// for safety
		return;
	}
	BlockRead(revBufTbl[theType], (short) (CEmilStm.readTypeMinRev(theType)),theOfstID, (int) (CEmilStm.readTypeMaxRev(theType)-CEmilStm.readTypeMinRev(theType)));
	revSaved[theType] = true;
}

//--------------------------------------------------------------
// LoadEfxBuf
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Load sub parameters.
//	entry:	PrmValue	type
//		theOfstID	offset parameter ID
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
public void LoadEfxBuf(int theType, short theOfstID)
{
	if (theType >= ParameterManager.getPrmId("EMEFX_TYPE_SIZE") ) {		// for safety
		return;
	}
	if (efxSaved[theType]) {
		BlockWrite(efxBufTbl[theType], (short) (CEmilStm.readTypeMinEfx(theType)), theOfstID, (int) (CEmilStm.readTypeMaxEfx(theType)-CEmilStm.readTypeMinEfx(theType)));
	}
	else {
		Clear((short) (CEmilStm.readTypeMinEfx(theType)), theOfstID, (int) (CEmilStm.readTypeMaxEfx(theType)-CEmilStm.readTypeMinEfx(theType)));
	}
}
public void LoadChoBuf(int theType, short theOfstID)
{
	if (theType >= ParameterManager.getPrmId("EMCHO_TYPE_SIZE")) {		// for safety
		return;
	}
	if (choSaved[theType]) {
		BlockWrite(choBufTbl[theType], (short) (CEmilStm.readTypeMinCho(theType)),theOfstID, (int) (CEmilStm.readTypeMaxCho(theType)-CEmilStm.readTypeMinCho(theType)));
	}
	else {
		Clear((short) (CEmilStm.readTypeMinCho(theType)), theOfstID, (int) (CEmilStm.readTypeMaxCho(theType)-CEmilStm.readTypeMinCho(theType)));
	}
}
public void LoadRevBuf(int theType, short theOfstID)
{
	if (theType >= ParameterManager.getPrmId("EMREV_TYPE_SIZE")) {		// for safety
		return;
	}
	if (revSaved[theType]) {
		BlockWrite(revBufTbl[theType], (short) (CEmilStm.readTypeMinRev(theType)),theOfstID, (int) (CEmilStm.readTypeMaxRev(theType)-CEmilStm.readTypeMinRev(theType)));
	}
	else {
		Clear((short) (CEmilStm.readTypeMinRev(theType)), theOfstID, (int) (CEmilStm.readTypeMaxRev(theType)-CEmilStm.readTypeMinRev(theType)));
	}
}

//--------------------------------------------------------------
// Write
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Write parameter.
//	entry:	theValue	value
//		theID		parameter ID
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
    public void Write(long theValue, short theID) {
        if (mBuf == null) {
            return;
        }
        short excID;
        {
            excID = ParameterManager.gEmilPrm.EmilWriteValue(mBuf, theID, theValue);
        }
        StoreID(excID);
    }

    public void Write(long theValue, short theID, short theOfstID) {
        if (mBuf == null) {
            return;
        }
        short excID;
        {
            if (theID >= ParameterManager.getPrmId("EM_SUBPRMID_MIN")) {
                excID = (short) ParameterManager.gEmilPrm.EmilWriteSubValue(mBuf, theOfstID, theID, theValue);
            } else {
                excID = ParameterManager.gEmilPrm.EmilWriteValue(mBuf, theID, theValue);
            }
        }
        StoreID(excID);
    }

	public void Close() {}    
//--------------------------------------------------------------
// WriteExc
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Write exclusive.
//	entry:	theExc		exclusive data
//		theID		parameter ID
//		theSize		size
//	retrn:	size
//	remks:	---
//
//--------------------------------------------------------------
public int WriteExc(byte[] theExc, short theID, int theSize)
{
	if (mBuf == null) {
		return 0;
	}
	if (mProtect != 0) {
		return 0;
	}
	int size;
	{
		size = ParameterManager.gEmilPrm.EmilWriteExcData(mBuf, theID, theExc, theSize);
	}
	StoreID(theID, size);
	return size;
}

}
