package emil.user;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public final class COUserStmEMRC extends COEmilStm{
        @Override
    	public void Open(int theNum){
            SEmilRhy buf = ParameterManager.gVVUserRhy.GetEmilRhy(theNum);
            SetBuf((buf == null) ? null : buf.rc.data);
        }
public	COUserStmEMRC(int theNum) { Open(theNum); }

}
