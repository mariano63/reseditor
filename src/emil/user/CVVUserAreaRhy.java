package emil.user;

import config.ParameterManager;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public class CVVUserAreaRhy {
    SEmilRhy[]  gUserEmilRhy = null;	/* rhythm */
	SEmilRhy[]    mEmilRhy;
	short        mSize;
public	CVVUserAreaRhy(){
    
    int numOf = ParameterManager.getInstance().getMappaSize().get("NUMOF_EMUSER_PAT");
    gUserEmilRhy = new SEmilRhy[numOf];	/* rhythm */
    for (int i = 0; i<numOf; i++){
        gUserEmilRhy[i] = new SEmilRhy();
        ParameterManager.gEmilPrm.EmilClearRhy(gUserEmilRhy[i], null);
    }
	mEmilRhy = gUserEmilRhy;
	mSize = (short) gUserEmilRhy.length;
    
}

	public SEmilRhy GetEmilRhy(int theSlot, int theNum) { return (theSlot == 1) ? GetEmilRhy(theNum) : null; }
	public SEmilRhy GetEmilRhy(int theNum){
        if ((theNum < 1) || (theNum > mSize)) {
            return null;
        }
        return (mEmilRhy != null) ? (mEmilRhy[theNum - 1] ) : null;
    }
    
	public int GetNumMax(int theSlot ) { return (theSlot == 1) ? mSize : 0; }
	public int GetNumMax() { return GetNumMax(1); }
    
}
