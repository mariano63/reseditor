package emil.user;

import config.ParameterManager;
import emil.istm.CIEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public final class CIUserStmEMPT extends CIEmilStm{	// patch common
	public void Open(int theNum, int theTone){
        SEmilPat buf = ParameterManager.gVVUserPat.GetEmilPat(theNum);
        SetBuf((buf == null) ? null : buf.pt[theTone].data);
    }
	public CIUserStmEMPT(int theNum, int theTone) { Open(theNum, theTone); }
}
