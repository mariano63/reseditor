package emil.user;

import config.ParameterManager;
import emil.CVVPrmStm;
import emil.istm.CIEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public final class CIUserStmEMPC extends CIEmilStm{	// patch common
	public void Open(int theNum){
        SEmilPat buf = ParameterManager.gVVUserPat.GetEmilPat(theNum);
        SetBuf((buf == null) ? null : buf.pc.data);
    }
	public CIUserStmEMPC(int theNum) { Open(theNum); }
    
    
    public void Read(CVVPrmStm.SName theName)
    {
        int ii;
        for (ii = 0; ii < ParameterManager.getPrmId("EMPC_NAME_SIZE"); ii++) {
            String str = String.format("EMPC_NAME%d", ii+1);
//            theName.str[ii] = ReadValue(EMPC_NAME(ii + 1));
            theName.str[ii] = (byte) ReadValue( (short) ParameterManager.getPrmId(str));
        }
        theName.str[ii] = '\0';
    }
}
