package emil.user;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public final class COUserStmEMPX extends COEmilStm{
    	// patch common
    @Override
	public void Open(int theNum){
        
	SEmilPat buf = ParameterManager.gVVUserPat.GetEmilPat(theNum);
	SetBuf((buf == null) ? null : buf.px.data);
    }
public	COUserStmEMPX(int theNum) { Open(theNum); }
}
