package emil.user;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public final class COUserStmEMPT extends COEmilStm{
    	// patch common
    @Override
	public void Open(int theNum, int theTone){
        SEmilPat buf = ParameterManager.gVVUserPat.GetEmilPat(theNum);
        SetBuf((buf == null) ? null : buf.pt[theTone].data);
    }
    public	COUserStmEMPT(int theNum, int theTone) { Open(theNum, theTone); }
}
