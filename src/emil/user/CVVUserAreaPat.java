package emil.user;

import config.ParameterManager;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class CVVUserAreaPat {
    
    SEmilPat[] gUserEmilPat = null;	/* patch */

    SEmilPat[]	mEmilPat=null;
	short       mSize;
public  CVVUserAreaPat(){
    int numOf = ParameterManager.getInstance().getMappaSize().get("NUMOF_EMUSER_PAT");
    gUserEmilPat = new SEmilPat[numOf];
    for (int i = 0; i < numOf; i++){
        gUserEmilPat[i] = new SEmilPat();
        ParameterManager.gEmilPrm.EmilClearPat(gUserEmilPat[i], null);
    }
	mEmilPat = gUserEmilPat;
	mSize = (short) gUserEmilPat.length;
}

	public SEmilPat GetEmilPat(int theSlot, int theNum){
        int emUserPath = ParameterManager.getInstance().getMappaSize().get("NUMOF_EMUSER_PAT");
        int emUserBank = ParameterManager.getInstance().getMappaSize().get("NUMOF_EMUSER_BANK");
        if ((theSlot < 1) || (theSlot > (emUserPath * emUserBank / 128))) {
            return null;
        }
        if ((theNum < 1) || (theNum > 128)) {
            return null;
        }
        return (mEmilPat != null) ? (mEmilPat [(theSlot - 1) * 128 + theNum - 1]) : null;
    }
	public SEmilPat GetEmilPat(int theNum) {
		theNum--;
		return GetEmilPat((theNum / 128) + 1, (theNum & 0x7F) + 1);
	}
    
    public int GetNumMax(){return GetNumMax(1);}
	public int GetNumMax(int theSlot) { return ((theSlot == 1) || (theSlot == 2)) ? mSize : 0; }

}
