package emil.user;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public final class COUserStmEMRT extends COEmilStm{
        @Override
    	public void Open(int theNum, int theNote){
            SEmilRhy buf = ParameterManager.gVVUserRhy.GetEmilRhy(theNum);
            SetBuf((buf == null) ? null : buf.rt[theNote].data);
        }
public	COUserStmEMRT(int theNum, int theNote) { Open(theNum, theNote); }

}
