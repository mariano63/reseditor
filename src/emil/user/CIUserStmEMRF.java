package emil.user;

import config.ParameterManager;
import emil.istm.CIEmilStm;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public final class CIUserStmEMRF extends CIEmilStm{	// patch common
	public void Open(int theNum){
        SEmilRhy buf = ParameterManager.gVVUserRhy.GetEmilRhy(theNum);
        SetBuf((buf == null) ? null : buf.rf.data);
    }
	public CIUserStmEMRF(int theNum) { Open(theNum); }
}
