package emil.user;

import config.ParameterManager;
import emil.istm.CIEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public final class CIUserStmEMPV extends CIEmilStm{	// patch common
	public void Open(int theNum){
        SEmilPat buf = ParameterManager.gVVUserPat.GetEmilPat(theNum);
        SetBuf((buf == null) ? null : buf.pv.data);
    }
	public CIUserStmEMPV(int theNum) { Open(theNum); }
}
