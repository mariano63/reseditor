package emil.user;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public final class COUserStmEMRV extends COEmilStm{
        @Override
    	public void Open(int theNum){
            SEmilRhy buf = ParameterManager.gVVUserRhy.GetEmilRhy(theNum);
            SetBuf((buf == null) ? null : buf.rv.data);
        }
public	COUserStmEMRV(int theNum) { Open(theNum); }

}
