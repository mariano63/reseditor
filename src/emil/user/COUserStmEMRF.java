package emil.user;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public final class COUserStmEMRF extends COEmilStm{
        @Override
    	public void Open(int theNum){
            SEmilRhy buf = ParameterManager.gVVUserRhy.GetEmilRhy(theNum);
            SetBuf((buf == null) ? null : buf.rf.data);
        }
public	COUserStmEMRF(int theNum) { Open(theNum); }

}
