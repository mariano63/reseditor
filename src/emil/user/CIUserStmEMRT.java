package emil.user;

import config.ParameterManager;
import emil.CEmilLib;
import emil.istm.CIEmilStm;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public final class CIUserStmEMRT extends CIEmilStm{	// patch common
	public void Open(int theNum, int theNote){
        SEmilRhy buf = ParameterManager.gVVUserRhy.GetEmilRhy(theNum);
        SetBuf((buf == null) ? null : buf.rt[theNote - CEmilLib.EMIL_RHY_NOTE_MIN].data);
    }
	public CIUserStmEMRT(int theNum, int theNote) { Open(theNum, theNote); }
}
