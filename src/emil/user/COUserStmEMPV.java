package emil.user;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public final class COUserStmEMPV extends COEmilStm{
    	// patch common
    @Override
	public void Open(int theNum){
        
	SEmilPat buf = ParameterManager.gVVUserPat.GetEmilPat(theNum);
	SetBuf((buf == null) ? null : buf.pv.data);
    }
public	COUserStmEMPV(int theNum) { Open(theNum); }
}
