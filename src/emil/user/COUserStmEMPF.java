
package emil.user;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public final class COUserStmEMPF extends COEmilStm{	// patch common EFX
    @Override
	public void Open(int theNum){
        SEmilPat buf = ParameterManager.gVVUserPat.GetEmilPat(theNum);
        SetBuf((buf == null) ? null : buf.pf.data);
    }
public COUserStmEMPF(int theNum) { Open(theNum); }
    
}
