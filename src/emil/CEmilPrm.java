package emil;

import config.ParameterManager;
import emil.semil.SEmilSubTbl;
import emil.semil.SEmilPat;
import emil.semil.SEmilRhy;
import emil.semil.SEmilRhyRT;

/**
 *
 * @author mgiacomo
 */
public class CEmilPrm extends CEmilLib{

    final public int EM_SUBPRMID_MIN = ParameterManager.getPrmId("EM_SUBPRMID_MIN");
    
/*--------------------------------------------------------------
 * EmilReadValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read value with range check.
 *	entry:	theData		data
 *		theID		parameter ID
 *	retrn:	value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
    long EmilReadValue(byte[] theData, short theID)
    {
        return EmilLibReadValue(theData, ParameterManager.getInstance().prmTbl[theID]);
    }
/*--------------------------------------------------------------
 * EmilWriteValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write value with range check.
 *	entry:	theData		data
 *		theID		parameter ID
 *		theValue	value
 *	retrn:	parameter ID for exclusive
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
    short EmilWriteValue(byte[] theData, short theID, long theValue)
    {
        EmilLibWriteValue(theData, ParameterManager.getInstance().prmTbl[theID], theValue);
        return theID;
    }
    
/*--------------------------------------------------------------
 * EmilClearValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Initialize.
 *	entry:	theData		data
 *		theID		parameter ID
 *		theTimes	number of times
 *	retrn:	parameter ID for exclusive
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
short EmilClearValue(byte[] theData, short theID, int theTimes)
{
	EmilLibClearValue(theData, theID, theTimes);
	return theID;
}
    
/*--------------------------------------------------------------
 * EmilGetPrmInfo
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Get the parameter information.
 *	entry:	theID		parameter ID
 *		theMin		pointer of minimum value
 *		theMax		pointer of maximum value
 *	retrn:	initial value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
long EmilGetPrmInfo(short theID, long theMin, long theMax)
{
	return EmilLibGetPrmInfo(ParameterManager.getInstance().prmTbl[theID], theMin, theMax);
}

/*--------------------------------------------------------------
 * EmilReadInit
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read initial value.
 *	entry:	theID		parameter ID
 *	retrn:	value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
long EmilReadInit(short theID)
{
	return EmilLibReadInit(ParameterManager.getInstance().prmTbl[theID]);
}

/*--------------------------------------------------------------
 * EmilReadExcData
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read exclusive data.
 *	entry:	theData		data
 *		theID		parameter ID
 *		theExcData	exclusive data
 *		theSize		size
 *	retrn:	size / -1
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
public int EmilReadExcData(int ndx, byte[] theData, short theID, byte[] theExcData, int theSize)
{
	return EmilLibReadExcData( ndx, theData, theID, theExcData, theSize);
}

/*--------------------------------------------------------------
 * EmilWriteExcData
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write exclusive data.
 *	entry:	theData		data
 *		theID		parameter ID
 *		theExcData	exclusive data
 *		theSize		size
 *	retrn:	size
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
int EmilWriteExcData(byte[] theData, short theID, byte[] theExcData, int theSize)
{
    
	return EmilLibWriteExcData(theData, theID, theExcData, theSize);
}

/*--------------------------------------------------------------
 * EmilGetExcID
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Convert parameter ID to exclusive ID.
 *	entry:	theID		parameter ID
 *	retrn:	exclusive ID
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
short EmilGetExcID(short theID)
{
	if (ParameterManager.getInstance().prmTbl[theID].bit < 0) {
		while (theID > 0) {
			if (ParameterManager.getInstance().prmTbl[theID - 1].bit >= 0) {
				break;
			}
			theID--;
		}
	}
	return theID;
}

/*--------------------------------------------------------------
 * EmilReadSubValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read sub value with range check.
 *	entry:	theData		data
 *		theID		parameter ID
 *		theSubID	sub parameter ID
 *	retrn:	value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
long EmilReadSubValue(byte[] theData, short theID, short theSubID)
{
	return EmilLibReadSubValue(theData, theID, theSubID - EM_SUBPRMID_MIN);
}
/*--------------------------------------------------------------
 * EmilWriteSubValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write sub value with range check.
 *	entry:	theData		data
 *		theID		parameter ID
 *		theSubID	sub parameter ID
 *		theValue	value
 *	retrn:	parameter ID for exclusive
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
    int EmilWriteSubValue(byte[] theData, short theID, short theSubID, long theValue)
{
	EmilLibWriteSubValue(theData, theID, theSubID - EM_SUBPRMID_MIN, theValue);
	return theID + ParameterManager.getInstance().prmTblSub[theSubID - EM_SUBPRMID_MIN].ofstID;
}
/*--------------------------------------------------------------
 * EmilClearSubValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Initialize.
 *	entry:	theData		data
 *		theID		parameter ID
 *		theSubID	sub parameter ID
 *		theTimes	number of times
 *	retrn:	parameter ID for exclusive
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
short EmilClearSubValue(byte[] theData, short theID, short theSubID, int theTimes)
{
	SEmilSubTbl sub = ParameterManager.getInstance().prmTblSub[theSubID - EM_SUBPRMID_MIN];
	EmilLibClearSubValue(theData, theID, theSubID - EM_SUBPRMID_MIN, theTimes);
	return (short) (theID + sub.ofstID);
}
/*--------------------------------------------------------------
 * EmilGetSubPrmInfo
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Get the sub parameter information.
 *	entry:	theSubID	sub parameter ID
 *		theMin		pointer of minimum value
 *		theMax		pointer of maximum value
 *	retrn:	initial value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
long EmilGetSubPrmInfo(short theSubID, long theMin, long theMax)
{
	SEmilSubTbl	sub = ParameterManager.getInstance().prmTblSub[theSubID - EM_SUBPRMID_MIN];
	return EmilLibGetSubPrmInfo(sub, theMin, theMax);
}

/*--------------------------------------------------------------
 * EmilReadSubInit
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read sub initial value.
 *	entry:	theSubID	sub parameter ID
 *	retrn:	value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
long EmilReadSubInit(short theSubID)
{

	return EmilLibReadSubInit(ParameterManager.getInstance().prmTblSub[theSubID - EM_SUBPRMID_MIN]);
}
long EmilGetUvalue(short theID, long theValue)
{
	return EmilLibGetUvalue(ParameterManager.getInstance().prmTbl[theID], theValue);
}
void EmilWriteUvalue(byte[] theData, short theID, long theUvalue)
{
	EmilLibWriteUvalue(theData, ParameterManager.getInstance().prmTbl[theID], theUvalue);
}



/*--------------------------------------------------------------
 * EmilClearRhy
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Initialize rhythm parameter.
 *	entry:	theBuf		parameter
 *		theName		name
 *	retrn:	void
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
public void EmilClearRhy(SEmilRhy theBuf, byte[] theName)
{
	String	rhyInitName = "INIT RHYTHM";
	int	i;

	EmilClearValue(theBuf.rc.data, (short) ParameterManager.getPrmId("EMRC_MIN"), ParameterManager.getPrmId("EMRC_MAX") - ParameterManager.getPrmId("EMRC_MIN") + 1);
	EmilClearValue(theBuf.rf.data, (short) ParameterManager.getPrmId("EMRF_MIN"), ParameterManager.getPrmId("EMRF_MAX") - ParameterManager.getPrmId("EMRF_MIN") + 1);
	EmilClearValue(theBuf.rh.data, (short) ParameterManager.getPrmId("EMRH_MIN"), ParameterManager.getPrmId("EMRH_MAX") - ParameterManager.getPrmId("EMRH_MIN") + 1);
	EmilClearValue(theBuf.rv.data, (short) ParameterManager.getPrmId("EMRV_MIN"), ParameterManager.getPrmId("EMRV_MAX") - ParameterManager.getPrmId("EMRV_MIN") + 1);

    int numOfEmrt = ParameterManager.getInstance().getMappaSize().get("NUMOF_EMRT");
	for (i = 0; i < numOfEmrt; i++) {
		EmilClearValue(theBuf.rt[i].data, (short) ParameterManager.getPrmId("EMRT_MIN"), ParameterManager.getPrmId("EMRT_MAX") - ParameterManager.getPrmId("EMRT_MIN") + 1);
	}
	EmilWriteNameRC(theBuf.rc.data, (byte[]) ((theName == null) ? rhyInitName.getBytes() : theName));
	if (theName == null) {
		for (i = 0; i < numOfEmrt; i++) {
			EmilClearRhyRT(theBuf.rt[i], null);
		}
	}
}
/*--------------------------------------------------------------
 * EmilClearPat
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Initialize patch parameter.
 *	entry:	theBuf		parameter
 *		theName		name
 *	retrn:	void
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
public void EmilClearPat(SEmilPat theBuf, byte[] theName)
{
	String	patInitName= "INIT PATCH";
	
	int	i;
    int min = ParameterManager.getPrmId("EMPC_MIN");
    int max = ParameterManager.getPrmId("EMPC_MAX");
	EmilClearValue(theBuf.pc.data,(short)min , max - min + 1);
    min = ParameterManager.getPrmId("EMPF_MIN");
    max = ParameterManager.getPrmId("EMPF_MAX");
	EmilClearValue(theBuf.pf.data, (short)min, max - min + 1);
    min = ParameterManager.getPrmId("EMPH_MIN");
    max = ParameterManager.getPrmId("EMPH_MAX");
	EmilClearValue(theBuf.ph.data, (short)min, max - min + 1);
    min = ParameterManager.getPrmId("EMPV_MIN");
    max = ParameterManager.getPrmId("EMPV_MAX");
	EmilClearValue(theBuf.pv.data, (short)min, max - min + 1);
    min = ParameterManager.getPrmId("EMPX_MIN");
    max = ParameterManager.getPrmId("EMPX_MAX");
	EmilClearValue(theBuf.px.data, (short)min, max - min + 1);
	for (i = 0; i < ParameterManager.getInstance().getMappaSize().get("NUMOF_EMPT") ; i++) {
    min = ParameterManager.getPrmId("EMPT_MIN");
    max = ParameterManager.getPrmId("EMPT_MAX");
		EmilClearValue(theBuf.pt[i].data, (short)min, max - min + 1);
	}

	EmilWriteNamePC(theBuf.pc.data, (theName == null) ? patInitName.getBytes() : theName);
	if (theName == null) {
		EmilWriteValue(theBuf.px.data, (short) ParameterManager.getPrmId("EMPX_TMT1_SW"), 1);
	}
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL1_SRC"), CEmilH.EM_CTRL_SRC_CC01);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL1_DST1"), CEmilH.EM_CTRL_DST_PL1);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL1_SENS1"), 10);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL2_SRC"), CEmilH.EM_CTRL_SRC_AFT);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL2_DST1"), CEmilH.EM_CTRL_DST_LEV);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL2_SENS1"), 0);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL2_DST2"), CEmilH.EM_CTRL_DST_CUT);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL2_SENS2"), 0);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL3_SRC"), CEmilH.EM_CTRL_SRC_CC02);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL3_DST1"), CEmilH.EM_CTRL_DST_LEV);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL3_SENS1"), 0);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL3_DST2"), CEmilH.EM_CTRL_DST_CUT);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL3_SENS2"), 0);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL4_SRC"), CEmilH.EM_CTRL_SRC_CC04);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL4_DST1"), CEmilH.EM_CTRL_DST_LEV);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL4_SENS1"), 0);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL4_DST2"), CEmilH.EM_CTRL_DST_CUT);
	EmilWriteValue(theBuf.pc.data, (short) ParameterManager.getPrmId("EMPC_CTRL4_SENS2"), 0);

}

/*--------------------------------------------------------------
 * EmilWriteName??
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write name.
 *	entry:	theBuf		parameter
 *		theName		name
 *	retrn:	void
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
void EmilWriteNamePC(byte[] theBuf, byte[] theName)
{
	int	i;
    int ndx = 0;
    String name = new String(theName);
	for (i = 0; i < ParameterManager.getPrmId("EMPC_NAME_SIZE") ; i++) {
        String st = String.format("EMPC_NAME%d", i+1);
		if (!name.isEmpty() && ndx<theName.length) {
			EmilWriteValue(theBuf, (short) (ParameterManager.getPrmId(st)), theName[ndx++]);
		}
		else {
			EmilWriteValue(theBuf, (short) (ParameterManager.getPrmId(st)), ' ');
		}
	}
}
/*--------------------------------------------------------------
 * EmilClearRhyRT
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Initialize rhythm tone parameter.
 *	entry:	theBuf		parameter
 *		theName		name
 *	retrn:	void
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
void EmilClearRhyRT(SEmilRhyRT theBuf, byte[] theName)
{
	String	toneInitName = "INIT TONE";
	EmilClearValue(theBuf.data, (short) (ParameterManager.getPrmId("EMRT_MIN")), ParameterManager.getPrmId("EMRT_MAX") - ParameterManager.getPrmId("EMRT_MIN") + 1);

	EmilWriteNameRT(theBuf.data, (theName == null) ? toneInitName.getBytes() : theName);
	if (theName == null) {
		EmilWriteValue(theBuf.data, (short) (ParameterManager.getPrmId("EMRT_WMT1_SW")), 1);
	}
}

void EmilWriteNameRT(byte[] theBuf, byte[] theName)
{
	int	i;
    int ndx=0;

	for (i = 0; i < ParameterManager.getPrmId("EMRT_NAME_SIZE"); i++) {
		if (theName != null && ndx<theName.length) {
			EmilWriteValue(theBuf, (short) (ParameterManager.getPrmId("EMRT_NAME1")+i), theName[ndx]++);
		}
		else {
			EmilWriteValue(theBuf, (short) (ParameterManager.getPrmId("EMRT_NAME1")+i), ' ');
		}
	}
}
void EmilWriteNameRC(byte[] theBuf, byte[] theName)
{
	int	i;
    int ndx=0;
	for (i = 0; i < ParameterManager.getPrmId("EMRC_NAME_SIZE"); i++) {
		if (theName != null && ndx<theName.length) {
			EmilWriteValue(theBuf, (short) (ParameterManager.getPrmId("EMRC_NAME1")+i), theName[ndx++]);
		}
		else {
			EmilWriteValue(theBuf, (short) (ParameterManager.getPrmId("EMRC_NAME1")+i), ' ');
		}
	}
}



/*--------------------------------------------------------------
 * EmilReadName??
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read name.
 *	entry:	theBuf		parameter
 *		theName		name
 *	retrn:	void
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
void EmilReadNamePC(byte[] theBuf, byte[] theName)
{
	int	i;
    int ndx=0;
	for (i = 0; i < ParameterManager.getPrmId("EMPC_NAME_SIZE"); i++) {
		theName[ndx++] = (byte) EmilReadValue(theBuf, ((short)(ParameterManager.getPrmId("EMPC_NAME1")+i)));
	}
	theName[ndx] = 0;
}


void EmilReadNameRC(byte[] theBuf, byte[] theName)
{
	int	i;
    int ndx=0;
    
	for (i = 0; i < ParameterManager.getPrmId("EMRC_NAME_SIZE"); i++) {
		theName[ndx++] =(byte) EmilReadValue(theBuf,((short)(ParameterManager.getPrmId("EMRC_NAME1")+i)) );
	}
	theName[ndx] = 0;
}

void EmilReadNameRT(byte[] theBuf, byte[] theName)
{
	int	i;
    int ndx=0;
	for (i = 0; i < ParameterManager.getPrmId("EMRT_NAME_SIZE"); i++) {
		theName[ndx++] = (byte) EmilReadValue(theBuf,((short)(ParameterManager.getPrmId("EMRT_NAME1")+i)) );
	}
	theName[ndx] = 0;
}


/*--------------------------------------------------------------
 * EmilReadCatePC
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read category number.
 *	entry:	theBuf		parameter
 *		theCate		category number
 *	retrn:	void
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
void EmilReadCatePC(byte[] theBuf, byte[] theCate)
{
    //Ma che e' sta robba??? Non funzia---
//	*theCate = EmilReadValue(theBuf, EMPC_CATE);
}

/*--------------------------------------------------------------
 * EmilGetCateName
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Get the category name.
 *	entry:	theNum		category number
 *	retrn:	name
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
//static const union {
//	char	strings[4];
//	long	l;
//} 
String[] cateTbl =  {
	 "---" ,	/*  0	---	NO ASSIGN       No assign */
	 "PNO" ,	/*  1	PNO	AC.PIANO	Acoustic Piano, Layer */
	 "EP " ,	/*  2	EP	EL.PIANO	Electric Piano, Layer */
	 "KEY" ,	/*  3	KEY	KEYBOARDS	Other Keyboards (Clav, Harpsichord...) */
	 "BEL" ,	/*  4	BEL	BELL		Bell, Bell Pad */
	 "MLT" ,	/*  5	MLT	MALLET		Mallet, Chromatic Percussion */
	 "ORG" ,	/*  6	ORG	ORGAN		Electric and Church Organ */
	 "ACD" ,	/*  7	ACD	ACCORDION	Accordion, Bandneon */
	 "HRM" ,	/*  8	HRM	HARMONICA	Harmonica, Bluse Harp */
	 "AGT" ,	/*  9	AGT	AC.GUITAR	Acoustic Guitar, Layer */
	 "EGT" ,	/* 10	EGT	EL.GUITAR	Electric Guitar (Clean, Mute) */
	 "DGT" ,	/* 11	DGT	DIST.GUITAR	Distortion Guitar */
	 "BS " ,	/* 12	BS	BASS		Acoustic & Electric Bass */
	 "SBS" ,	/* 13	SBS	SYNTH BASS	Synth Bass */
	 "STR" ,	/* 14	STR	STRINGS		Bowed Solo Strings, Ensemble, Synth Strings */
	 "ORC" ,	/* 15	ORC	ORCHESTRA	Orchestra Ensemble */
	 "HIT" ,	/* 16	HIT	HIT&STAB	Orchestra Hit */
	 "WND" ,	/* 17	WND	WIND		Winds, Oboe, Clarinet.. */
	 "FLT" ,	/* 18	FLT	FLUTE		Flute, Piccolo.. */
	 "BRS" ,	/* 19	BRS	AC.BRASS	Acoustic Brass (include Brass Ensemble) */
	 "SBR" ,	/* 20	SBR	SYNTH BRASS	Synth Brass */
	 "SAX" ,	/* 21	SAX	SAX		Soprano, Alto,.. */
	 "HLD" ,	/* 22	HLD	HARD LEAD	Hard Synth Lead */
	 "SLD" ,	/* 23	SLD	SOFT LEAD	Soft Synth Lead */
	 "TEK" ,	/* 24	TEK	TECHNO SYNTH	Techno Synth (for Techno music) */
	 "PLS" ,	/* 25	PLS	PULSATING	Pulsating (LFO, Seq) */
	 "FX " ,	/* 26	FX	SYNTH FX	Synth FX (Noise, Ambient, no tonality sound) */
	 "SYN" ,	/* 27	SYN	OTHER SYNTH	Poly Synth, Other Synth (with Attack) */
	 "BPD" ,	/* 28	BPD	BRIGHT PAD	Bright Pad Synth */
	 "SPD" ,	/* 29	SPD	SOFT PAD	Soft Pad Synth */
	 "VOX" ,	/* 30	VOX	VOX		Vox, Choir */
	 "PLK" ,	/* 31	PLK	PLUCKED		Plucked (Harp, Kalimba...), Ethnic Pluck */
	 "ETH" ,	/* 32	ETH	ETHNIC		Other Ethnic */
	 "FRT" ,	/* 33	FRT	FRETTED		Fretted Inst (Mandolin..) */
	 "PRC" ,	/* 34	PRC	PERCUSSION	Percussion */
	 "SFX" ,	/* 35	SFX	SOUND FX	Sound FX */
	 "BTS" ,	/* 36	BTS	BEAT&GROOVE	Beat and Groove */
	 "DRM" ,	/* 37	DRM	DRUMS		Drum Set */
	 "CMB" 	/* 38	CMB	COMBINATION	Split, Layer.. */
};

byte[] EmilGetCateName(int theNum)
{
	if (theNum >= cateTbl.length) {
		theNum = 0;
	}
	else if (theNum < 0) {
		theNum = 0;
	}
	return cateTbl[theNum].getBytes();
}

/*--------------------------------------------------------------
 * EmilGetCateNum
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Get the category number.
 *	entry:	theName		name
 *	retrn:	category number
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
int EmilGetCateNum(byte[] theName)
{
//	union {
//		char	strings[4];
//		long	l;
//	} cate;
	int	num;
	byte[] p = new byte[4];

    p[0] = theName[0];
    p[1] = theName[1];
    p[2] = theName[2];
    p[3] = 0;
    String s = new String(p);
	for (num = cateTbl.length - 1; num > 0; num--) {
		if (cateTbl[num].equals(s)) {
			break;
		}
	}
	return num;
}


}
