
package emil;

import config.ParameterManager;
import emil.ostm.COEmilStmPC;
import emil.ostm.COEmilStmPF;
import emil.ostm.COEmilStmPH;
import emil.ostm.COEmilStmPT;
import emil.ostm.COEmilStmPV;
import emil.ostm.COEmilStmPX;
import emil.ostm.COEmilStmRC;
import emil.ostm.COEmilStmRF;
import emil.ostm.COEmilStmRH;
import emil.ostm.COEmilStmRT;
import emil.ostm.COEmilStmRV;
import emil.user.COUserStmEMPC;
import emil.user.COUserStmEMPF;
import emil.user.COUserStmEMPH;
import emil.user.COUserStmEMPT;
import emil.user.COUserStmEMPV;
import emil.user.COUserStmEMPX;
import emil.user.COUserStmEMRC;
import emil.user.COUserStmEMRF;
import emil.user.COUserStmEMRH;
import emil.user.COUserStmEMRT;
import emil.user.COUserStmEMRV;

/**
 *
 * @author mgiacomo
 */
public class CNexyRxDT1 extends CNexyExc{

	long			mLength;
//	union {
		byte	mErrFlag;
        public boolean isUserProtect(){ return (( (mErrFlag & 0x01) != 0) ? true : false);}
        public void setUserProtect(boolean fl){ if (fl) mErrFlag |= 0x01; else mErrFlag &= ~0x01;}
//		struct {
//			unsigned char	userProtect: 1;
//		} mErr;
//	};
	byte[]	mExc;

	int GetWriteSize()
{
	long remain = GetRemainSize();
	return (int) ((remain < mLength) ? remain : mLength);
}
    
//--------------------------------------------------------------
// CheckUserProtect
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Check user protect.
//	entry:	void
//	retrn:	possible to write?
//	remks:	---
//
//--------------------------------------------------------------
public boolean CheckUserProtect()
{
	if (! CVVPrmExc.IsExcProtect()) {
		return true;
	}
	if (! isUserProtect()) {
		setUserProtect(true);
		CEXCH.SendErrMesg(CErrMesg.ErrUserProtect);
	}
	return false;
}

    @Override
	void VTempSTP(long theOfst){}
    @Override
	void VTempSC(long theOfst){}
    @Override
	void VTempSL(long theOfst){}

    @Override
	public void VTempPC(long theOfst, int thePart){
        COEmilStmPC ostmPC = new COEmilStmPC();
        ostmPC.Open(thePart);
        ostmPC.WriteExc(mExc,  (short) (ParameterManager.getPrmId("EMPC_MIN")  + theOfst), GetWriteSize());
        ostmPC.Close(0);
//        new ObservableData("PC"); //Non credo vada bene, bisogna attendere che arrivino dati da midi
    }
    @Override
    public void VTempPF( long theOfst, int thePart)
{
	COEmilStmPF ostmPF = new COEmilStmPF();
	ostmPF.Open(thePart);
	ostmPF.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPF_MIN") + theOfst), GetWriteSize());
	ostmPF.Close(0);
}            

    @Override
    public void VTempPH(long theOfst, int thePart)
{
	COEmilStmPH ostmPH = new COEmilStmPH();
	ostmPH.Open(thePart);
	ostmPH.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPH_MIN") + theOfst), GetWriteSize());
	ostmPH.Close(0);
}
    @Override
    public void VTempPV(long theOfst, int thePart)
{
    COEmilStmPV ostmPV = new COEmilStmPV();
	ostmPV.Open(thePart);
	ostmPV.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPV_MIN") + theOfst), GetWriteSize());
	ostmPV.Close(0);
}
    @Override
    public void VTempPX( long theOfst, int thePart)
{
	COEmilStmPX ostmPX = new COEmilStmPX();
	ostmPX.Open(thePart);
	ostmPX.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPX_MIN") + theOfst), GetWriteSize());
	ostmPX.Close(0);
}
    @Override
    public void VTempPT(long theOfst, int thePart, int theTone)
{
	COEmilStmPT ostmPT = new COEmilStmPT();
	ostmPT.Open(thePart, theTone);
	ostmPT.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPT_MIN") + theOfst), GetWriteSize());
	ostmPT.Close(0);
}

    @Override
    public void VTempRC(long theOfst, int thePart)
{
	COEmilStmRC ostmRC = new COEmilStmRC();
	ostmRC.Open(thePart);
	ostmRC.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMRC_MIN") + theOfst), GetWriteSize());
	ostmRC.Close(0);
}
    @Override
    public void VTempRF(long theOfst, int thePart)
{
	COEmilStmRF ostmRF = new COEmilStmRF();
	ostmRF.Open(thePart);
	ostmRF.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMRF_MIN") + theOfst), GetWriteSize());
	ostmRF.Close(0);
}
    @Override
    public void VTempRH( long theOfst, int thePart)
{
	COEmilStmRH ostmRH = new COEmilStmRH();
	ostmRH.Open(thePart);
	ostmRH.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMRH_MIN") + theOfst), GetWriteSize());
	ostmRH.Close(0);
}
    @Override
    public void VTempRV(long theOfst, int thePart)
{
	COEmilStmRV ostmRV = new COEmilStmRV();
	ostmRV.Open(thePart);
	ostmRV.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMRV_MIN") + theOfst), GetWriteSize());
	ostmRV.Close(0);
}
    @Override
    public void VTempRT(long theOfst, int thePart, int theNote)
{
	COEmilStmRT ostmRT = new COEmilStmRT();
	ostmRT.Open(thePart, theNote);
	ostmRT.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMRT_MIN") + theOfst), GetWriteSize());
	ostmRT.Close(0);
}


    @Override
	public void VUserPC(long theOfst, int theNum){
    
	if (! CheckUserProtect()) {
		return;
	}
	COUserStmEMPC ostmPC = new COUserStmEMPC(theNum);
	ostmPC.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPC_MIN") + theOfst), GetWriteSize());
    ostmPC.Close();
}
    
    @Override
	public void VUserPF(long theOfst, int theNum){
    
	if (! CheckUserProtect()) {
		return;
	}
	COUserStmEMPF ostmPF = new COUserStmEMPF(theNum);
	ostmPF.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPF_MIN") + theOfst), GetWriteSize());
    ostmPF.Close();
}
    @Override
	public void VUserPH(long theOfst, int theNum){
    
	if (! CheckUserProtect()) {
		return;
	}
	COUserStmEMPH ostmPH = new COUserStmEMPH(theNum);
	ostmPH.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPH_MIN") + theOfst), GetWriteSize());
    ostmPH.Close();
}
    @Override
	public void VUserPV( long theOfst, int theNum){
        if (! CheckUserProtect()) {
            return;
        }
        COUserStmEMPV ostmPV = new COUserStmEMPV(theNum);
        ostmPV.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPV_MIN") + theOfst), GetWriteSize());
        ostmPV.Close();
}
    @Override
	public void VUserPX(long theOfst, int theNum){
    
	if (! CheckUserProtect()) {
		return;
	}
	COUserStmEMPX ostmPX = new COUserStmEMPX(theNum);
	ostmPX.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPX_MIN") + theOfst), GetWriteSize());
    ostmPX.Close();
}
    @Override
	public void VUserPT( long theOfst, int theNum, int theTone){
        if (! CheckUserProtect()) {
            return;
        }
        COUserStmEMPT ostmPT = new COUserStmEMPT(theNum, theTone);
        ostmPT.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMPT_MIN") + theOfst), GetWriteSize());
        ostmPT.Close();
    }
    @Override
	public void VUserRC( long theOfst, int theNum){
        if (! CheckUserProtect()) {
            return;
        }
        COUserStmEMRC ostmRC = new COUserStmEMRC(theNum);
        ostmRC.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMRC_MIN") + theOfst), GetWriteSize());
        ostmRC.Close();
    }
    @Override
	public void VUserRF( long theOfst, int theNum){

        if (! CheckUserProtect()) {
            return;
        }
        COUserStmEMRF ostmRF = new COUserStmEMRF(theNum);
        ostmRF.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMRF_MIN") + theOfst), GetWriteSize());
        ostmRF.Close();
    }
    @Override
	public void VUserRH( long theOfst, int theNum){
        if (! CheckUserProtect()) {
            return;
        }
        COUserStmEMRH ostmRH = new COUserStmEMRH(theNum);
        ostmRH.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMRH_MIN") + theOfst), GetWriteSize());
        ostmRH.Close();
    }
    @Override
	public void VUserRV( long theOfst, int theNum){
        if (! CheckUserProtect()) {
            return;
        }
        COUserStmEMRV ostmRV = new COUserStmEMRV(theNum);
        ostmRV.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMRV_MIN") + theOfst), GetWriteSize());      
        ostmRV.Close();
    }
    @Override
	public void VUserRT( long theOfst, int theNum, int theNote){
        
	if (! CheckUserProtect()) {
		return;
	}
	COUserStmEMRT ostmRT = new COUserStmEMRT(theNum, theNote);
	ostmRT.WriteExc(mExc, (short) (ParameterManager.getPrmId("EMRT_MIN") + theOfst), GetWriteSize());
    ostmRT.Close();
    }

    public	void ParseDT1(CEXCH.SInfoDT1 theInfo){
        mErrFlag = 0;			// clear all the error flag
        mExc = theInfo.data;
        mLength = theInfo.length;
        ParseAdrs(theInfo.adrs);
    } 
}
