
package emil;

import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class CEmilStm extends CVVPrmStm{
    				// tone #
		public static final int TonePatMin = 0;					//  patch
		public static final int TonePatMax = 3;
		public static final int TonePatSize = TonePatMax+1;
		public static final int ToneRhyMin = 0;					//  rhyrhm
		public static final int ToneRhyMax = 87;
		public static final int ToneRhySize = ToneRhyMax+1;
                
		public static final int NoteRhyMin = 21;
		public static final int NoteRhyMax = 108;
		public static final int NoteRhySize = 88;

        public class prmtbl{
            short prmId;
            byte times;
        }
        static boolean efxSaved[];
        static boolean choSaved[];
        static boolean revSaved[];
//        static prmtbl efxPrmTbl[];
//        static prmtbl choPrmTbl[];
//        static prmtbl revPrmTbl[];
        static long efxBufTbl[][];
        static long choBufTbl[][];
        static long revBufTbl[][];
        public CEmilStm(){
            int siz = ParameterManager.getPrmId("EMEFX_TYPE_SIZE");
            efxSaved = new boolean[siz];
//            efxPrmTbl = new prmtbl[siz];
            efxBufTbl = new long[siz][99];
            siz = ParameterManager.getPrmId("EMCHO_TYPE_SIZE");
            choSaved = new boolean[siz];
//            choPrmTbl = new prmtbl[siz];
            choBufTbl = new long[siz][99];
            siz = ParameterManager.getPrmId("EMREV_TYPE_SIZE");
            revSaved = new boolean[siz];
//            revPrmTbl = new prmtbl[siz];
            revBufTbl = new long[siz][99];
        }

    static long readTypeMinEfx(long theType) {
        String st = String.format("EMEFX_TYPE%ld_MIN",  theType);
        return ParameterManager.getPrmId(st);
    }
    static long readTypeMinCho(long theType) {
        String st = String.format("EMCHO_TYPE%ld_MIN", theType);
        return ParameterManager.getPrmId(st);
    }
    static long readTypeMinRev(long theType) {
        String st = String.format("EMREV_TYPE%ld_MIN", theType);
        return ParameterManager.getPrmId(st );
    }

    static long readTypeMaxEfx(long theType) {
        String st = String.format("EMEFX_TYPE%ld_MAX", theType);
        return ParameterManager.getPrmId(st);
    }
    static long readTypeMaxCho(long theType) {
        String st = String.format("EMCHO_TYPE%ld_MAX", theType);
        return ParameterManager.getPrmId(st);
    }
    static long readTypeMaxRev(long theType) {
        String st = String.format("EMREV_TYPE%ld_MAX", theType);
        return ParameterManager.getPrmId(st );
    }
	protected static long ReadValue(byte[] theBuf, short theID){
        if (theBuf == null) {
            return ParameterManager.gEmilPrm.EmilReadInit(theID);
        }
        return ParameterManager.gEmilPrm.EmilReadValue(theBuf, theID); 
    }
    
//--------------------------------------------------------------
// ReadSubValue
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Read parameter value.
//	entry:	theBuf		buffer
//		theID		parameter ID
//		theOfstID	sub parameter ID
//	retrn:	value
//	remks:	---
//
//--------------------------------------------------------------
protected static long ReadSubValue(byte[] theBuf, short theID, short theOfstID)
{
	if (theBuf == null) {
		return (theID >= ParameterManager.getPrmId("EM_SUBPRMID_MIN") ) ? ParameterManager.gEmilPrm.EmilReadSubInit(theID) : ParameterManager.gEmilPrm.EmilReadInit(theID);
	}
	if (theID >= ParameterManager.getPrmId("EM_SUBPRMID_MIN")) {
		return ParameterManager.gEmilPrm.EmilReadSubValue(theBuf, theOfstID, theID);
	}
	else {
		return ParameterManager.gEmilPrm.EmilReadValue(theBuf, theID);
	}
}
//--------------------------------------------------------------
// GetPrmInfo
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Get the parameter information.
//	entry:	theInfo		parameter information
//		theID		parameter ID
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
void GetPrmInfo(SPrmInfo theInfo, short theID)
{
	if (theID >= ParameterManager.getPrmId("EM_SUBPRMID_MIN")) {
		theInfo.init = ParameterManager.gEmilPrm.EmilGetSubPrmInfo(theID, theInfo.min, theInfo.max);
	}
	else {
		theInfo.init = ParameterManager.gEmilPrm.EmilGetPrmInfo(theID, theInfo.min, theInfo.max);
	}
}

//--------------------------------------------------------------
// GetExcID
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Get the parameter ID for exclusive.
//	entry:	theID		parameter ID
//	retrn:	exclusive ID
//	remks:	---
//
//--------------------------------------------------------------
public static short GetExcID(short theID)
{
	return ParameterManager.gEmilPrm.EmilGetExcID(theID);
}
//--------------------------------------------------------------
// GetCateName
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Get the category name.
//	entry:	theCate		category
//	retrn:	category name
//	remks:	---
//
//--------------------------------------------------------------
byte[] GetCateName(int theNum)
{
	return ParameterManager.gEmilPrm.EmilGetCateName(theNum);
}
}
