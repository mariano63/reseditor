package emil;

import config.ParameterManager;
import emil.user.CVVUserAreaPat;
import emil.user.CVVUserAreaRhy;

/**
 *
 * @author mgiacomo
 */
public class MainDebug {

    
    public static CEmilPrm gEmilPrm;   //1   
    public static CVVTempAreaPart gVVTempAreaPart;
    public static CVVUserAreaPat gVVUserPat;
    public static CVVUserAreaRhy gVVUserRhy;
    public static CVVPrmExcTx gVVPrmExcTx;    
    public static CVVPRMHOut gVVPrmhOut;
    public static CVVPrmExcRx gVVPrmExcRx;
    public static CExcData gTestExcC;
    
    
    void SendMidiExc(String theExc, int theSize) {
        System.out.printf("Size: %d \n", theSize);
        System.out.println(theExc);
    }
    
    public void startDebug(){
        System.out.println("Write EMPC_LEVEL to 127 by Exc.");
        gTestExcC.data = hexStringToByteArray("f0411000003c121f00000e7f54f7");
        gVVPrmExcRx.ParseExcC(gTestExcC.data);
    }
    
    
    public static void main(String[] args) {
        
        gEmilPrm = new CEmilPrm();   //1   
        gVVTempAreaPart = new CVVTempAreaPart();
        gVVUserPat = new CVVUserAreaPat();
        gVVUserRhy = new CVVUserAreaRhy();
        gVVPrmExcTx = new CVVPrmExcTx();    
        gVVPrmhOut = new CVVPRMHOut();
        gVVPrmExcRx = new CVVPrmExcRx();
        gTestExcC = new CExcData();
        
        new MainDebug().startDebug();
    }
    
    static public int toCmp(byte b){
        int val = b;
        val &= 0xFF;
        return val;
    }
    
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }    
}
