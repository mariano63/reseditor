
package emil;

/**
 *
 * @author mgiacomo
 */
public class CVVPrmExc {

//==============================================================
// Exclusive parse base class.
//==============================================================
	public static byte	mDevID = 0x10;
	public static boolean mRxExc = true;
	public static boolean mRxGM1 = true;
	public static boolean mRxGM2 = true;
	public static boolean mRxGS = true;
	public static boolean mExcProtect = false;
	public static boolean mTxEdit = true;

    public static int GetDevID() { return mDevID; }
	public static boolean IsRxExc() { return mRxExc; }
	public static boolean IsRxGM1() { return mRxGM1; }
	public static boolean IsRxGM2() { return mRxGM2; }
	public static boolean IsRxGS() { return mRxGS; }
	public static boolean IsExcProtect() { return mExcProtect; }
	public static boolean IsTxEdit() { return mTxEdit; }
	public static boolean IsValidDevID(int theDevID) {
		if (theDevID == CEXCH.DeviceBroadcast) {
			return true;
		}
		if (theDevID == mDevID) {
			return true;
		}
		return false;
	}
    

//--------------------------------------------------------------
// SendErrMesg
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Send error message.
//	entry:	theCode		error code
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
static public void SendErrMesg(int theCode)
{

}    
}
