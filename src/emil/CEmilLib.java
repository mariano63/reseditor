
package emil;

import config.ParameterManager;
import emil.semil.SEmilSubTbl;
import emil.semil.SEmilPrmTbl;
import main.Main;

/**
 *
 * @author mgiacomo
 */
public class CEmilLib {

public static final int SIZEOF_EMPL = 18;
public static final int  SIZEOF_EMRL = 16;
public static final int EMIL_RHY_NOTE_MIN = 21;					/* note # of lowest rhythm key */
public static final int  EMIL_PART_PAT=16;/* part # for the patch mode */
public static final int CHAR_BIT = 8;
public static int EMIL_RHY_NOTE_MAX; //= (EMIL_RHY_NOTE_MIN + Main.pman.getMappaSize().get("NUMOF_EMRT") - 1);	/* note # of highest rhythm key */ inizializzato in ParameterManager

//public class SEmilPrmTbl {			/* parameter access table */
//	public short	adrs;
//	public byte	bit;
//	public byte	blank;
//	public short	ofst;
//	public short	uinit;
//	public short	umin;
//	public short	umax;
//} 
//
//public class  SEmilSubTbl{			/* parameter sub table */
//	public byte	ofstID;
//	public short		init;
//	public short		min;
//	public short		max;
//}

public static final int  SIGNED_OFFSET=64;
private	static long[]	maskTbl = new long[]{
    0x00000000,
	0x00000001, 0x00000003, 0x00000007, 0x0000000F, 0x0000001F, 0x0000003F, 0x0000007F, 0x000000FF,
	0x000001FF, 0x000003FF, 0x000007FF, 0x00000FFF, 0x00001FFF, 0x00003FFF, 0x00007FFF, 0x0000FFFF,
	0x0001FFFF, 0x0003FFFF, 0x0007FFFF, 0x000FFFFF, 0x001FFFFF, 0x003FFFFF, 0x007FFFFF, 0x00FFFFFF,
	0x01FFFFFF, 0x03FFFFFF, 0x07FFFFFF, 0x0FFFFFFF, 0x1FFFFFFF, 0x3FFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF,
};

static public byte ABS(byte b){
    return (byte) ((b<0) ? -b : b);
}
 static public int toUnsigned(byte i){
     return i & 0xff;
 }
 static public long toUnsigned(int i){
     long l = i & 0xffffffffL;
     return l;
 }

/*--------------------------------------------------------------
 * ReadUvalue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read unsigned value.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *	retrn:	value (unsigned)
 *	remks:	---
 *
 *--------------------------------------------------------------
 */    
	private long ReadUvalue(byte[] theData, SEmilPrmTbl theTbl){
        int length, shift;
        int ubit;
        long uvalue;

        int ndx = theTbl.adrs;
        ubit = ABS(theTbl.bit);
        length = ubit + theTbl.blank;
        shift = -length;
        uvalue = 0;
        do {
            uvalue <<= CHAR_BIT;
            long l = toUnsigned(theData[ndx++]);
            uvalue |=  l;
            shift += CHAR_BIT;
        } while ((length -= CHAR_BIT) > 0);
        uvalue = (uvalue >> (long) shift) & maskTbl[ubit];
        return uvalue;        
    }
    
    
/*--------------------------------------------------------------
 * WriteUvalue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write unsigned value.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theUvalue	value (unsigned)
 *	retrn:	void
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
	private void WriteUvalue(byte[] theData, SEmilPrmTbl theTbl, long theUvalue){
    
	int		length, shift;
	long	uvalue, mask;

	int ndx = theTbl.adrs;
	int ubit = ABS(theTbl.bit);
	length = ubit + theTbl.blank;
	shift = -length;

	uvalue = 0;
	do {
		uvalue <<= CHAR_BIT;
		uvalue |= toUnsigned(theData[ndx]); ndx++;
		shift += CHAR_BIT;
	} while ((length -= CHAR_BIT) > 0);
	mask = maskTbl[ubit] << toUnsigned(shift);
	uvalue = (uvalue & ~mask) | ((theUvalue << shift) & mask);

	do {
        --ndx;
		theData[ndx] = (byte)uvalue;
		uvalue >>= CHAR_BIT;
	} while (ndx != theTbl.adrs);
}
/*--------------------------------------------------------------
 * ReadExcValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read unsigned exclusive value.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *	retrn:	exclusive value (unsigned)
 *	remks:	No range check.
 *
 *--------------------------------------------------------------
 */
	private long ReadExcValue(byte[] theData, SEmilPrmTbl theTbl){

	long	uvalue;
	long		ofst;

	uvalue = ReadUvalue(theData, theTbl);
	if ((theTbl.umin - theTbl.ofst < 0) && (SIGNED_OFFSET > theTbl.ofst)) {
		ofst = SIGNED_OFFSET - theTbl.ofst;
	}
	else {
		ofst = 0;
	}
	return uvalue + ofst;
    
}
/*--------------------------------------------------------------
 * WriteExcValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write unsigned exclusive value.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theExcValue	exclusive value (unsigned)
 *	retrn:	void
 *	remks:	Out of bit range replaced to initial value.
 *
 *--------------------------------------------------------------
 */
	private void WriteExcValue(byte[] theData, SEmilPrmTbl theTbl, long theExcValue){

	int		ofst;
	int	ubit;
    int excVal = (int) (theExcValue & 0xff);
    
	if ((theTbl.umin - theTbl.ofst < 0) && (SIGNED_OFFSET > theTbl.ofst)) {
		ofst = SIGNED_OFFSET - theTbl.ofst;
	}
	else {
		ofst = 0;
	}
	ubit = ABS(theTbl.bit);
	if (excVal - ofst > maskTbl[ubit]) {
		excVal = theTbl.uinit + ofst;
	}
	WriteUvalue(theData, theTbl, excVal - ofst);
    
}
    
/*--------------------------------------------------------------
 * WriteExcValueOut
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write unsigned exclusive value.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theExcValue	exclusive value (unsigned)
 *	retrn:	void
 *	remks:	Out of range ignored.
 *
 *--------------------------------------------------------------
 */
	private void WriteExcValueOut(byte[] theData, SEmilPrmTbl theTbl, long theExcValue){

	long	ofst;

	if ((theTbl.umin - theTbl.ofst < 0) && (SIGNED_OFFSET > theTbl.ofst)) {
		ofst = SIGNED_OFFSET - theTbl.ofst;
	}
	else {
		ofst = 0;
	}
	if (theTbl.umin == theTbl.umax) {
		/* nop */
	}
	else if (theExcValue - ofst < theTbl.umin) {
		return;
	}
	else if (theExcValue - ofst > theTbl.umax) {
		return;
	}
	WriteUvalue(theData, theTbl, theExcValue - ofst);
    
}
/*--------------------------------------------------------------
 * EmilLibReadValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read value with range check.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *	retrn:	value
 *	remks:	Out of range replaced to initial value.
 *
 *--------------------------------------------------------------
 */
	public long EmilLibReadValue(byte[] theData, SEmilPrmTbl theTbl){
        long	uvalue;

        uvalue = ReadUvalue(theData, theTbl);
        if (theTbl.umin == theTbl.umax) {
            /* nop */
        }
        else if (uvalue < theTbl.umin) {
            uvalue = theTbl.uinit;
        }
        else if (uvalue > theTbl.umax) {
            uvalue = theTbl.uinit;
        }
        return uvalue - theTbl.ofst;
    }
/*--------------------------------------------------------------
 * EmilLibWriteValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write value with range check.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theValue	value
 *	retrn:	void
 *	remks:	Out of range replaced to boarder value.
 *
 *--------------------------------------------------------------
 */
	public void EmilLibWriteValue(byte[] theData, SEmilPrmTbl theTbl, long theValue){

	long	uvalue;

	uvalue = theValue + theTbl.ofst;
	if (theTbl.umin == theTbl.umax) {
		/* nop */
	}
	else if (uvalue < theTbl.umin) {
		uvalue = theTbl.umin;
	}
	else if (uvalue > theTbl.umax) {
		uvalue = theTbl.umax;
	}
	WriteUvalue(theData, theTbl, uvalue);    
}
/*--------------------------------------------------------------
 * EmilLibClearValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Initialize.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theTimes	number of times
 *	retrn:	void
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
	public void EmilLibClearValue(byte[] theData, int ndx, int theTimes){
        while (theTimes-- > 0) {
            if ((ParameterManager.getInstance().prmTbl[ndx].bit >= 0) || (-ParameterManager.getInstance().prmTbl[ndx].bit >= CHAR_BIT)) {
                WriteUvalue(theData, ParameterManager.getInstance().prmTbl[ndx], ParameterManager.getInstance().prmTbl[ndx].uinit);
            }
            ndx++;
        }    
}
/*--------------------------------------------------------------
 * EmilLibGetPrmInfo
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Get the parameter information.
 *		theTbl		parameter access table
 *		theMin		pointer of minimum value
 *		theMax		pointer of maximum value
 *	retrn:	initial value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
	public long EmilLibGetPrmInfo(SEmilPrmTbl theTbl, long theMin, long theMax){
        if (theTbl.umin == theTbl.umax) {
            theMin = theTbl.umin - theTbl.ofst;
            theMax = (1 << toUnsigned(theTbl.bit)) - 1 - theTbl.ofst;
        }
        else {
            theMin = theTbl.umin - theTbl.ofst;
            theMax = theTbl.umax - theTbl.ofst;
        }
        return theTbl.uinit - theTbl.ofst;        
    }
/*--------------------------------------------------------------
 * EmilLibReadInit
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read initial value.
 *		theTbl		parameter access table
 *	retrn:	value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
	public long EmilLibReadInit(SEmilPrmTbl theTbl){
        return theTbl.uinit - theTbl.ofst;        
    }
/*--------------------------------------------------------------
 * EmilLibReadExcData
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read exclusive data.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theExcData	exclusive data
 *		theSize		size
 *	retrn:	size / -1
 *	remks:	No range check.
 *
 *--------------------------------------------------------------
 */
	public int EmilLibReadExcData(int ndx, byte[] theData, int ndxTheTbl, byte[] theExcData, int theSize){

	int	size = 0;
//    int ndx=0;

	while (theSize-- > 0) {
		int	bit;

		if ((bit = ParameterManager.getInstance().prmTbl[ndxTheTbl].bit) < 0) {
			int	bitsum = 0, nsize = 0;

			while (-bit < CHAR_BIT) {
				bitsum += -bit;
				theExcData[ndx++] = (byte)ReadUvalue(theData, ParameterManager.getInstance().prmTbl[ndxTheTbl++]);		/* no range check */
                nsize++;
				if (theSize-- == 0) {
					return size;
				}
				bit = ParameterManager.getInstance().prmTbl[ndxTheTbl].bit;
			}
			if ((bit = -bit - bitsum) >= CHAR_BIT) {
				return -1;
			}
			theExcData[ndx++] = (byte) ((byte)ReadUvalue(theData, ParameterManager.getInstance().prmTbl[ndxTheTbl]) & maskTbl[bit]);	/* no range check */
			size += nsize;
		}
		else {
			theExcData[ndx++] = (byte) ReadExcValue(theData, ParameterManager.getInstance().prmTbl[ndxTheTbl]);			/* no range check */
		}
		ndxTheTbl++;
		size++;
	}
	return size;    
}
/*--------------------------------------------------------------
 * EmilLibWriteExcData
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write exclusive data.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theExcData	exclusive data
 *		theSize		size
 *	retrn:	size
 *	remks:	Out of bit range replaced to initial value.
 *
 *--------------------------------------------------------------
 */
	public int EmilLibWriteExcData(byte[] theData, int ndxTheTbl, byte[] theExcData, int theSize){

    	int	size = 0;
    int ndxExcData=0;

	while (theSize-- > 0) {
		int	bit;

		if ((bit = ParameterManager.getInstance().prmTbl[ndxTheTbl].bit) < 0) {
			int		bitsum = 0, nsize = 0;
			long	uvalue = 0;
			byte	data;

			while (-bit < CHAR_BIT) {
				bitsum += -bit;
				data = theExcData[ndxExcData];
                ndxExcData++;
				if (data > maskTbl[-bit]) {
					uvalue = 0xffffffffl;;	/* always out of range */
				}
				uvalue = (uvalue | data) << (-((int)bit&0xff));
				ndxTheTbl++;
				nsize++;
				if (theSize--==0) {
					return size;
				}
				bit = ParameterManager.getInstance().prmTbl[ndxTheTbl].bit;
			}
			if ((bit = -bit - bitsum) >= CHAR_BIT) {
				ndxExcData++;
				ndxTheTbl++;
				continue;
			}
			data = theExcData[ndxExcData]; ndxExcData++;
			if (data > maskTbl[bit]) {
				uvalue = 0xffffffffl;;		/* always out of range */
			}
			uvalue |= data;
			WriteExcValue(theData, ParameterManager.getInstance().prmTbl[ndxTheTbl], uvalue);
			size += nsize;
		}
		else {
			WriteExcValue(theData, ParameterManager.getInstance().prmTbl[ndxTheTbl], theExcData[ndxExcData]);
            ndxExcData++;
		}
		ndxTheTbl++;
		size++;
	}
	return size;        
    }
    
/*--------------------------------------------------------------
 * EmilLibWriteExcDataOut
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write exclusive data.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theExcData	exclusive data
 *		theSize		size
 *	retrn:	size
 *	remks:	Out of range ignored.
 *
 *--------------------------------------------------------------
 */
	public int EmilLibWriteExcDataOut(byte[] theData, SEmilPrmTbl[] theTbl, byte[] theExcData, int theSize){

	int	size = 0;
    int ndxTheTbl = 0;
    int ndxTheExcData = 0;
    
	while (theSize-- > 0) {
		int	bit;

		if ((bit = theTbl[ndxTheTbl].bit) < 0) {
			int		bitsum = 0, nsize = 0;
			long	uvalue = 0;
			byte data;

			while (-bit < CHAR_BIT) {
				bitsum += -bit;
				data = theExcData[ndxTheExcData]; ndxTheExcData++;
				if (data > maskTbl[-bit]) {
					uvalue = 0xffffffffl;;	/* always out of range */
				}
				uvalue = (uvalue | data) << (-((int)bit&0xff));
				ndxTheTbl++;
				nsize++;
				if (theSize--==0) {
					return size;
				}
				bit = theTbl[ndxTheTbl].bit;
			}
			if ((bit = -bit - bitsum) >= CHAR_BIT) {
				ndxTheExcData++;
				ndxTheTbl++;
				continue;
			}
			data = theExcData[ndxTheExcData]; ndxTheExcData++;
			if (data > maskTbl[bit]) {
				uvalue = 0xffffffffl;		/* always out of range */
			}
			uvalue |= data;
			WriteExcValueOut(theData, theTbl[ndxTheTbl], uvalue);
			size += nsize;
		}
		else {
			WriteExcValueOut(theData, theTbl[ndxTheTbl], theExcData[ndxTheExcData]);
            ndxTheExcData++;
		}
		ndxTheTbl++;
		size++;
	}
	return size;    
}
/*--------------------------------------------------------------
 * EmilLibReadSubValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read sub value with range check.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theSub		sub parameter table
 *	retrn:	value
 *	remks:	Out of range replaced to initial value.
 *
 *--------------------------------------------------------------
 */
	public long EmilLibReadSubValue(byte[] theData, int ndxPrm, int ndxSub){

        long	value;
        SEmilSubTbl prmSub = ParameterManager.getInstance().prmTblSub[ndxSub];
                
        value = EmilLibReadValue(theData, ParameterManager.getInstance().prmTbl[ndxPrm+prmSub.ofstID]);
        if (value < prmSub.min) {
            value = prmSub.init;
        }
        else if (value > prmSub.max) {
            value = prmSub.init;
        }
        return value;    
}
/*--------------------------------------------------------------
 * EmilLibWriteSubValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Write sub value with range check.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theSub		sub parameter table
 *		theValue	value
 *	retrn:	void
 *	remks:	Out of range replaced to boarder value.
 *
 *--------------------------------------------------------------
 */
	public void EmilLibWriteSubValue(byte[] theData, int ndxPrmTab, int ndxSub, long theValue){

       SEmilSubTbl theSub = ParameterManager.getInstance().prmTblSub[ndxSub];
        
        if (theValue < theSub.min) {
            theValue = theSub.min;
        }
        else if (theValue > theSub.max) {
            theValue = theSub.max;
        }
        EmilLibWriteValue(theData, ParameterManager.getInstance().prmTbl[ndxPrmTab+theSub.ofstID], theValue);
}
/*--------------------------------------------------------------
 * EmilLibClearSubValue
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Initialize.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theSub		sub parameter table
 *		theTimes	number of times
 *	retrn:	void
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
	public void EmilLibClearSubValue(byte[] theData, int ndxTbl, int ndxSub, int theTimes){
        int ndxTheSub = 0;
	while (theTimes-- > 0) {
        SEmilSubTbl semSub = ParameterManager.getInstance().prmTblSub[ndxSub+ndxTheSub];
        SEmilPrmTbl sem = ParameterManager.getInstance().prmTbl[ndxTbl+semSub.ofstID];
		EmilLibWriteValue(theData, sem, semSub.init);
		ndxTheSub++;
	}
}
/*--------------------------------------------------------------
 * EmilLibGetSubPrmInfo
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Get the sub parameter information.
 *	entry:	theSub		sub parameter table
 *		theMin		pointer of minimum value
 *		theMax		pointer of maximum value
 *	retrn:	initial value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
	public long EmilLibGetSubPrmInfo(SEmilSubTbl theSub, long theMin, long theMax){
        theMin = theSub.min;
        theMax = theSub.max;
        return theSub.init;    
}
/*--------------------------------------------------------------
 * EmilLibReadSubInit
 *--------------------------------------------------------------
 *
 *	prog :	tsuyo
 *	func :	Read sub initial value.
 *	entry:	theSub		sub parameter table
 *	retrn:	value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
	public long EmilLibReadSubInit(SEmilSubTbl theSub){
        return theSub.init;
    }
/*--------------------------------------------------------------
 * EmilLibGetUvalue
 *--------------------------------------------------------------
 *
 *	prog :	isogawa
 *	func :	Get unsigned value.
 *	entry:	theTbl		parameter access table
 *		theValue	value
 *	retrn:	value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
	public long EmilLibGetUvalue(SEmilPrmTbl theTbl, long theValue){
        long	uvalue;
        theValue += theTbl.ofst;
        uvalue = (theValue < 0) ? 0 : theValue;
        if (theTbl.umin == theTbl.umax) {
            return uvalue;
        }
        else if (uvalue < theTbl.umin) {
            return theTbl.umin;
        }
        else if (uvalue > theTbl.umax) {
            return theTbl.umax;
        }
        return uvalue;
}
/*--------------------------------------------------------------
 * EmilLibWriteUvalue
 *--------------------------------------------------------------
 *
 *	prog :	isogawa
 *	func :	Write unsigned value.
 *	entry:	theData		data
 *		theTbl		parameter access table
 *		theUvalue	value (unsigned)
 *	retrn:	value
 *	remks:	---
 *
 *--------------------------------------------------------------
 */
	public void EmilLibWriteUvalue(byte[] theData, SEmilPrmTbl theTbl, long theUvalue){
    	WriteUvalue(theData, theTbl, theUvalue);
    }
    
    
    
    public static void main(String[] args) {
        System.out.println(toUnsigned((byte)-127)+"");   //OK
        System.out.println(toUnsigned(-2)+"");   //OK
    }
}