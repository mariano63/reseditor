package emil;

import config.ParameterManager;
import emil.istm.CIEmilStmPC;
import emil.istm.CIEmilStmPF;
import emil.istm.CIEmilStmPH;
import emil.istm.CIEmilStmPT;
import emil.istm.CIEmilStmPV;
import emil.istm.CIEmilStmPX;
import emil.istm.CIEmilStmRC;
import emil.istm.CIEmilStmRF;
import emil.istm.CIEmilStmRH;
import emil.istm.CIEmilStmRT;
import emil.istm.CIEmilStmRV;
import emil.user.CIUserStmEMPC;
import emil.user.CIUserStmEMPF;
import emil.user.CIUserStmEMPH;
import emil.user.CIUserStmEMPT;
import emil.user.CIUserStmEMPV;
import emil.user.CIUserStmEMPX;
import emil.user.CIUserStmEMRC;
import emil.user.CIUserStmEMRF;
import emil.user.CIUserStmEMRH;
import emil.user.CIUserStmEMRT;
import emil.user.CIUserStmEMRV;

/**
 *
 * @author mgiacomo
 */
public class CNexyRxRQ1 extends CNexyExc {
    
	long			mReqSize;
	long		mAdrs;
	long		mEndAdrs;
	long			mSendSize;
    
//		union {
//		unsigned char	mErrFlag;
//		struct {
//			unsigned char	txSysEx: 1;
//		} mErr;
//	};
        byte mErrFlag;
	CExcData		mExcC = new CExcData();

//--------------------------------------------------------------
// ParseRQ1
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Parse RQ1.
//	entry:	CEXCH::SInfoRQ1&	exclusive information
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
void ParseRQ1(SInfoRQ1 theInfo)
{
	long size = BlockParseRQ1(theInfo);
	while (size > 0) {
		size = BlockParseRQ1();
	}
}
//--------------------------------------------------------------
// BlockParseRQ1
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Parse RQ1 by each block.
//	entry:	CEXCH::SInfoRQ1&	exclusive information
//	retrn:	send size
//	remks:	---
//
//--------------------------------------------------------------
long BlockParseRQ1(SInfoRQ1 theInfo)
{
	mErrFlag = (byte)0xff;			// set all the error flag
	return BlockParseInfo(theInfo);
}

long BlockParseRQ1()
{
	return BlockParseInfo();
}

//--------------------------------------------------------------
// BlockParseInfo
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Parse and send DT1.
//	entry:	CEXCH::SInfoRQ1&	exclusive information
//	retrn:	send size
//	remks:	---
//
//--------------------------------------------------------------
long BlockParseInfo(SInfoRQ1 theInfo)
{
	mReqSize = theInfo.size;
	mAdrs = theInfo.adrs;
	mEndAdrs = mAdrs + mReqSize;
	return BlockParseInfo();
}

long BlockParseInfo()
{
	while (mAdrs < mEndAdrs) {
		mSendSize = 0;
 		ParseAdrs(mAdrs);
		if (mSendSize < 0) {			// skip this address when size < 0
			mReqSize--;
			mAdrs++;
			continue;
		}
		long nextAdrs = GetNextAdrs();
		mReqSize -= nextAdrs - mAdrs;
		mAdrs = nextAdrs;
		if (mSendSize > 0) {
			return mSendSize;		// return once
		}
	}
	return 0;
}

//--------------------------------------------------------------
// GetReadSize
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Get the size to read.
//	entry:	void
//	retrn:	read size
//	remks:	---
//
//--------------------------------------------------------------
int GetReadSize()
{
	long remain = GetRemainSize();
	return (int) ((remain < mReqSize) ? remain : mReqSize);
}


//--------------------------------------------------------------
// SetExcHeader
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Set exclusive header.
//	entry:	theDevID	device ID
//	retrn:	exclusive data
//	remks:	---
//
//--------------------------------------------------------------
byte[] SetExcHeader(int theDevID)
{
	return CEXCH.SetDT1Header(mExcC, (short)CEXCH.ModelAX, (byte)theDevID, mAdrs);
}





//--------------------------------------------------------------
// SetExcFooter
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Set exclusive header and send.
//	entry:	theExc		exclusive data
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
void SetExcFooter(byte[] theExc)
{
	if (mSendSize <= 0) {
		return;
	}
    int ndx = (int) (0 + mSendSize);
	
	theExc[ndx++] = 0x00;			// check sum (dummy)
	theExc[ndx] = (byte) 0xF7;				// EOX
	CEXCH.SetDT1Footer(mExcC);

	if ( (mErrFlag & (byte)0x80) == 0) {
		mErrFlag |= (byte) 0x80;
		CVVPrmExc.SendErrMesg(CErrMesg.ErrTxSysEx);
	}
	ParameterManager.gVVPrmhOut.PutExc(mExcC);
}
            

	void SendWriteStart(byte[] theData){}
	
    @Override
	public void VTempSTP(long theOfst){}
    @Override
	public void VTempSC(long theOfst){}
    @Override
	public void VTempSL(long theOfst){}

    @Override
	public void VTempPC(long theOfst, int thePart){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmPC istmPC = new CIEmilStmPC();
        istmPC.Open(thePart);
        mSendSize = istmPC.ReadExc(mExcC.length, p, (short) (ParameterManager.getPrmId("EMPC_MIN")  + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        istmPC.Close();
        SetExcFooter(p);        
    }
    @Override
	public void VTempPF(long theOfst, int thePart){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmPF istmPF = new CIEmilStmPF();
        istmPF.Open(thePart);
        mSendSize = istmPF.ReadExc(mExcC.length, p, (short)(ParameterManager.getPrmId("EMPF_MIN")  + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        istmPF.Close();
        SetExcFooter(p);
    }
    @Override
	public void VTempPH(long theOfst, int thePart){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmPH istmPH = new CIEmilStmPH();
        istmPH.Open(thePart);
        mSendSize = istmPH.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMPH_MIN")  + theOfst) , GetReadSize());
        mSendSize +=mExcC.length;
        istmPH.Close();
        SetExcFooter(p);
    }
    @Override
	public void VTempPV(long theOfst, int thePart){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmPV istmPV = new CIEmilStmPV();
        istmPV.Open(thePart);
        mSendSize = istmPV.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMPV_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        istmPV.Close();
        SetExcFooter(p);
    }
    @Override
	public void VTempPX(long theOfst, int thePart){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmPX istmPX= new CIEmilStmPX();
        istmPX.Open(thePart);
        mSendSize = istmPX.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMPX_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        istmPX.Close();
        SetExcFooter(p);
    }
    @Override
	public void VTempPT(long theOfst, int thePart, int theTone){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmPT istmPT = new CIEmilStmPT();
        istmPT.Open(thePart, theTone);
        mSendSize = istmPT.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMPT_MIN") + theOfst) , GetReadSize());
        mSendSize +=mExcC.length;
        istmPT.Close();
        SetExcFooter(p);
    }
    @Override
	public void VTempRC(long theOfst, int thePart){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmRC istmRC = new CIEmilStmRC();
        istmRC.Open(thePart);
        mSendSize = istmRC.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMRC_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        istmRC.Close();
        SetExcFooter(p);
}
    @Override
	public void VTempRF(long theOfst, int thePart){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmRF istmRF = new CIEmilStmRF();
        istmRF.Open(thePart);
        mSendSize = istmRF.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMRF_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        istmRF.Close();
        SetExcFooter(p);
    }
    @Override
	public void VTempRH(long theOfst, int thePart){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmRH istmRH = new CIEmilStmRH();
        istmRH.Open(thePart);
        mSendSize = istmRH.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMRH_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        istmRH.Close();
        SetExcFooter(p);        
    }
    @Override
	public void VTempRV(long theOfst, int thePart){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmRV istmRV = new CIEmilStmRV();
        istmRV.Open(thePart);
        mSendSize = istmRV.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMRV_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        istmRV.Close();
        SetExcFooter(p);
    }
    @Override
	public void VTempRT(long theOfst, int thePart, int theNote){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIEmilStmRT istmRT = new CIEmilStmRT();
        istmRT.Open(thePart, theNote);
        mSendSize = istmRT.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMRT_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        istmRT.Close();
        SetExcFooter(p);
    }

    @Override
	public void VUserPC(long theOfst, int theNum){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMPC istmPC = new CIUserStmEMPC(theNum);
        mSendSize = istmPC.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMPC_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmPC.Close();
    }
    @Override
	public void VUserPF(long theOfst, int theNum){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMPF istmPF = new CIUserStmEMPF(theNum);
        mSendSize = istmPF.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMPF_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmPF.Close();
    }
    @Override
	public void VUserPH(long theOfst, int theNum){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMPH istmPH = new CIUserStmEMPH(theNum);
        mSendSize = istmPH.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMPH_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmPH.Close();
    }
    @Override
	public void VUserPV(long theOfst, int theNum){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMPV istmPV = new CIUserStmEMPV(theNum);
        mSendSize = istmPV.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMPV_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmPV.Close();
    }
    @Override
	public void VUserPX(long theOfst, int theNum){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMPX istmPX = new CIUserStmEMPX(theNum);
        mSendSize = istmPX.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMPX_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmPX.Close();
    }
    @Override
	public void VUserPT(long theOfst, int theNum, int theTone){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMPT istmPT = new CIUserStmEMPT(theNum, theTone);
        mSendSize = istmPT.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMPT_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmPT.Close();
    }
    @Override
	public void VUserRC(long theOfst, int theNum){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMRC istmRC = new CIUserStmEMRC(theNum);
        mSendSize = istmRC.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMRC_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmRC.Close();
    }
    @Override
	public void VUserRF(long theOfst, int theNum){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMRF istmRF = new CIUserStmEMRF(theNum);
        mSendSize = istmRF.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMRF_MIN")  + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmRF.Close();
    }
    @Override
	public void VUserRH(long theOfst, int theNum){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMRH istmRH = new CIUserStmEMRH(theNum);
        mSendSize = istmRH.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMRH_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmRH.Close();
    }
    @Override
	public void VUserRV(long theOfst, int theNum){
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMRV istmRV = new CIUserStmEMRV(theNum);
        mSendSize = istmRV.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMRV_MIN")  + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmRV.Close();
    }
    @Override
	public void VUserRT(long theOfst, int theNum, int theNote){  
        byte[] p = SetExcHeader(CVVPrmExc.GetDevID());
        CIUserStmEMRT istmRT = new CIUserStmEMRT(theNum, theNote);
        mSendSize = istmRT.ReadExc(mExcC.length,p, (short)(ParameterManager.getPrmId("EMRT_MIN") + theOfst), GetReadSize());
        mSendSize +=mExcC.length;
        SetExcFooter(p);
        istmRT.Close();
    }







}
