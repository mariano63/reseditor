package emil.istm;

import config.ParameterManager;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public class CIEmilStmRT extends CIEmilStm{
public void Open(int thePart, int theNote)
    {
        SEmilRhy buf = ParameterManager.gVVTempAreaPart.GetEmilRhy(thePart);
        SetBuf((buf == null) ? null : buf.rt[theNote - NoteRhyMin].data);
    }
	public void Read(long theValue, short theID) { theValue = ReadValue(theID); }
	void Read(SName theName){
        int i;
        for (i = 0; i < ParameterManager.getPrmId("EMRT_NAME_SIZE"); i++) {
            String str = String.format("EMRT_NAME%d", i+1);
            theName.str[i] = (byte) ReadValue((short)ParameterManager.getPrmId(str));
        }
        theName.str[i] = '\0';        
    }
	void Read(SVRange theRange, int theWMT)		// WMT # = 0 - 3
    {
        String strLow = String.format("EMRT_WMT%d_VRANGE_LO", theWMT + 1);
        long lower = ReadValue((short)ParameterManager.getPrmId(strLow));
//        PrmValue lower = ReadValue(EMRT_WMT(theWMT + 1) + EMRT_WMTx_VRANGE_LO);
        
        String strHigh = String.format("EMRT_WMT%d_VRANGE_UP", theWMT + 1);
        long upper = ReadValue((short)ParameterManager.getPrmId(strHigh));
        
//        PrmValue upper = ReadValue(EMRT_WMT(theWMT + 1) + EMRT_WMTx_VRANGE_UP);
        theRange.lower = (byte) lower;
        theRange.upper = (byte) ((upper < lower) ? lower : upper);
    }    
    
    
	

}
