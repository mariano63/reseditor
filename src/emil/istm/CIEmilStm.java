package emil.istm;

import config.ParameterManager;
import emil.CEmilStm;

/**
 *
 * @author mgiacomo
 */
public class CIEmilStm extends CEmilStm{
    byte[] mBuf;
    byte mProtect;
    protected	void SetBuf(byte[] theBuf) { 
		mBuf = theBuf;
		mProtect = 0;
	}
	protected void SetProtect(int theSw) { mProtect = (byte) theSw; }

	public void Close() {}    
    
    public long ReadValue(short theID) {
		return CEmilStm.ReadValue(mBuf, theID);
	}
	public long ReadSubValue(short theID, short theOfstID) {
		return CEmilStm.ReadSubValue(mBuf, theID, theOfstID);
	}
    public int ReadExc(int ndx, byte[] theExc, short theID, int theSize)
    {
        if (mBuf == null) {
            return 0;
        }
        if (mProtect != 0) {
            return 0;
        }
        return ParameterManager.gEmilPrm.EmilReadExcData(ndx, mBuf, theID, theExc, theSize);
    }
}
