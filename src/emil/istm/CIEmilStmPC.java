package emil.istm;

import config.ParameterManager;
import emil.CVVPrmStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class CIEmilStmPC extends CIEmilStm{
    public void Open(int thePart)
    {
        SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
        SetBuf((buf == null) ? null : buf.pc.data);
    }
    
    public void Read(long theValue, short theID) { theValue = ReadValue(theID); }
    public void Read(CVVPrmStm.SName theName)
    {
        int ii;
        for (ii = 0; ii < ParameterManager.getPrmId("EMPC_NAME_SIZE"); ii++) {
            String str = String.format("EMPC_NAME%d", ii+1);
//            theName.str[ii] = ReadValue(EMPC_NAME(ii + 1));
            theName.str[ii] = (byte) ReadValue( (short) ParameterManager.getPrmId(str));
        }
        theName.str[ii] = '\0';
    }
}
