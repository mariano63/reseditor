package emil.istm;

import config.ParameterManager;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public class CIEmilStmRV extends CIEmilStm{
    				// rhythm common reverb
	static short	mOfstID;
public 	void Open(int thePart){
    
	SEmilRhy buf = ParameterManager.gVVTempAreaPart.GetEmilRhy(thePart);
	SetBuf((buf == null) ? null : buf.rv.data);
}
	public void Read(long theValue, short theID) { theValue = ReadSubValue(theID, mOfstID); }
    @Override
	public long ReadValue(short theID) { return ReadSubValue(theID, mOfstID); }
}
