
package emil.istm;

import config.ParameterManager;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public class CIEmilStmRC extends CIEmilStm{				// patch TMT (Tone Mix Table)
    public 	void Open(int thePart){
        SEmilRhy buf = ParameterManager.gVVTempAreaPart.GetEmilRhy(thePart);
        SetBuf((buf == null) ? null : buf.rc.data);
    }
	void Read(long theValue, short theID) { theValue = ReadValue(theID); }
	void Read(SName theName){
        int ii;
        for (ii = 0; ii < ParameterManager.getPrmId("EMRC_NAME_SIZE"); ii++) {
            String str = String.format("EMRC_NAME%d", ii+1);
            theName.str[ii] = (byte) ReadValue( (short) ParameterManager.getPrmId(str));
        }
        theName.str[ii] = '\0';
    }
   
}
