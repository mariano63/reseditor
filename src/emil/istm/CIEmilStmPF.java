
package emil.istm;

import config.ParameterManager;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class CIEmilStmPF extends CIEmilStm{
	static short	mOfstID;
    public void Open(int thePart){
        SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
        SetBuf((buf == null) ? null : buf.pf.data);
    }
	public void Read(long theValue, short theID) { theValue = ReadSubValue(theID, mOfstID); }
    @Override
	public long ReadValue(short theID) { return ReadSubValue(theID, mOfstID); }

}
