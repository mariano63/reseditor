/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package emil.istm;

import config.ParameterManager;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class CIEmilStmPH extends CIEmilStm{
    				// patch common chorus
	static short	mOfstID;
public void Open(int thePart){
    
	SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
	SetBuf((buf == null) ? null : buf.ph.data);
}
	public void Read(long theValue, short theID) { theValue = ReadSubValue(theID, mOfstID); }
    @Override
	public long ReadValue(short theID) { return ReadSubValue(theID, mOfstID); }

}
