package emil.istm;

import config.ParameterManager;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public class CIEmilStmRF extends CIEmilStm{
			// rhythm common EFX
	static short	mOfstID;
    public void Open(int thePart){
        SEmilRhy buf = ParameterManager.gVVTempAreaPart.GetEmilRhy(thePart);
        SetBuf((buf == null) ? null : buf.rf.data);
    }
	public void Read(long theValue, short theID) { theValue = ReadSubValue(theID, mOfstID); }
    @Override
	public long ReadValue(short theID) { return ReadSubValue(theID, mOfstID); }    
}
