package emil.istm;

import config.ParameterManager;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class CIEmilStmPT extends CIEmilStm {				// patch tone
public void Open(int thePart, int theTone){
	SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
	SetBuf((buf == null) ? null : buf.pt[theTone].data);    
}
public void Read(long theValue, short theID) { theValue = ReadValue(theID); }
    
}
