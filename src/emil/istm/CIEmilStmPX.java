package emil.istm;

import config.ParameterManager;
import emil.CVVPrmStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class CIEmilStmPX extends CIEmilStm{				// patch TMT (Tone Mix Table)
public 	void Open(int thePart){
	SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
	SetBuf((buf == null) ? null : buf.px.data);
}
	public void Read(long theValue, short theID) { theValue = ReadValue(theID); }
	public void Read(CVVPrmStm.SKRange theRange, int theTone)		// tone# = 0 - 3
    {
        
        String str = String.format("EMPX_TMT%d_KRANGE_LO", theTone+1);
        long lower = ReadValue((short) ParameterManager.getPrmId(str));
        str = String.format("EMPX_TMT%d_KRANGE_UP", theTone+1);
        long upper = ReadValue((short) ParameterManager.getPrmId(str));
        
//	long lower = ReadValue(EMPX_TMT(theTone + 1) + EMPX_TMTx_VRANGE_LO);
//	PrmValue upper = ReadValue(EMPX_TMT(theTone + 1) + EMPX_TMTx_VRANGE_UP);
        theRange.lower = (byte) lower;
        theRange.upper = (byte) ((upper < lower) ? lower : upper);
    }
    
}
