/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package emil.semil;

import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class SEmilPatPX {
    
    public byte[] data;

    public SEmilPatPX() {
        int size = ParameterManager.getInstance().getMappaSize().get("SIZEOF_EMPX");
        data = new byte[size];
    }   
}
