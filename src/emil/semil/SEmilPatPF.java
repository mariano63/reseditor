
package emil.semil;

import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class SEmilPatPF {
    
    public byte[] data;

    public SEmilPatPF() {
        int size = ParameterManager.getInstance().getMappaSize().get("SIZEOF_EMPF");
        data = new byte[size];
    }
}
