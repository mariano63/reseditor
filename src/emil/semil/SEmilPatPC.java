
package emil.semil;

import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class SEmilPatPC {
    public byte[] data;

    public SEmilPatPC() {
//        int size = Util.readSize("SIZEOF_EMPC");
        int size = ParameterManager.getInstance().getMappaSize().get("SIZEOF_EMPC");
        data = new byte[size];
    }
    
}
