
package emil.semil;

import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class SEmilRhyRH {
    
    public byte[] data;

    public SEmilRhyRH() {
        int size = ParameterManager.getInstance().getMappaSize().get("SIZEOF_EMRH");
        data = new byte[size];
    }   
}
