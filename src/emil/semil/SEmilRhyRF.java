
package emil.semil;

import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class SEmilRhyRF {
    
    public byte[] data;

    public SEmilRhyRF() {
        int size = ParameterManager.getInstance().getMappaSize().get("SIZEOF_EMRF");
        data = new byte[size];
    }   
}
