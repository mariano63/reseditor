package emil.semil;

/**
 *
 * @author mgiacomo
 */
public class SEmilSubTbl {		/* parameter sub table */
	public byte     ofstID;
	public short	init;
	public short	min;
	public short	max;

    public SEmilSubTbl(byte ofstID, short init, short min, short max) {
        this.ofstID = ofstID;
        this.init = init;
        this.min = min;
        this.max = max;
    }

    
}
