
package emil.semil;

import config.ParameterManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author mgiacomo
 */
public class Util {
    
    static int readSizeTmp(String name){
        try {
            try (InputStream in = Util.class.getResourceAsStream(ParameterManager.emilPath+ParameterManager.emPrmTableSize); BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
                String line;
                final Pattern pattern = Pattern.compile("[-|\\+]*\\d+");
                while ((line = br.readLine()) != null) {
                    if (line.indexOf(name)==-1) continue;
                    Matcher matcher = pattern.matcher(line);
                    if (matcher.find()){
                        return Integer.parseInt(matcher.group());
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null,
                "Missing file:",
                ParameterManager.emilPath+ParameterManager.emPrmTableSize+" ???",
            JOptionPane.ERROR_MESSAGE);
        }
            JOptionPane.showMessageDialog(null,
                "Size return 0",
                ParameterManager.emilPath+ParameterManager.emPrmTableSize+" Some trouble???",
            JOptionPane.ERROR_MESSAGE);
        return 0;
    }
}
