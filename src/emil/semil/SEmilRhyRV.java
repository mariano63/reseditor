
package emil.semil;

import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class SEmilRhyRV {
    
    public byte[] data;

    public SEmilRhyRV() {
        int size = ParameterManager.getInstance().getMappaSize().get("SIZEOF_EMRV");
        data = new byte[size];
    }   
}
