package emil.semil;

import config.ParameterManager;
import emil.CEmilLib;

/**
 *
 * @author mgiacomo
 */
public class SEmilPat {
    
    public static SEmilPat[] gEmilTempPat = new SEmilPat[CEmilLib.EMIL_PART_PAT];

	public SEmilPatPC		pc;			/*  common */
	public SEmilPatPF		pf;			/*  efx */
	public SEmilPatPH		ph;			/*  chorus */
	public SEmilPatPV		pv;			/*  reverb */
	public SEmilPatPX		px;			/*  TMT */
	public SEmilPatPT[]		pt;         /*  tone */
	public SEmilPatPL		pl;         /*Dummy*/

    public SEmilPat() {
        pc = new SEmilPatPC();
        pf = new SEmilPatPF();
        ph = new SEmilPatPH();
        pv = new SEmilPatPV();
        px = new SEmilPatPX();
        int siz = ParameterManager.getInstance().getMappaSize().get("NUMOF_EMPT");
        pt = new SEmilPatPT[siz];
        for (int i = 0; i< siz; i++)
            pt[i] = new SEmilPatPT();
        pl = new SEmilPatPL();
    }
    
//    public long getMySize(){
//        
//    }
}
