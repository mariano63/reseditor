package emil.semil;

import emil.CEmilLib;
import main.Main;

/**
 *
 * @author mgiacomo
 */
public class SEmilPatPL {
    public byte[] data;

    public SEmilPatPL() {
        int size = CEmilLib.SIZEOF_EMPL;
        data = new byte[size];
    }       
}
