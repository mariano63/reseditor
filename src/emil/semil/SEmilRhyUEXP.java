package emil.semil;

/**
 *
 * @author mgiacomo
 */
public class SEmilRhyUEXP {
   
	public SEmilRhyRC		rc;			/*  common */
	public SEmilRhyRF		rf;			/*  efx */
	public SEmilRhyRH		rh;			/*  chorus */
	public SEmilRhyRV		rv;			/*  reverb */
	public SEmilRhyRT[]		rt;		/*  tone */

    public SEmilRhyUEXP() {
        rc = new SEmilRhyRC();
        rf = new SEmilRhyRF();
        rh = new SEmilRhyRH();
        rv = new  SEmilRhyRV();
        rt = new SEmilRhyRT[88];
        for (int i = 0; i< 88; i++)
            rt[i] = new SEmilRhyRT();
    }
}
