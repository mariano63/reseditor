package emil.semil;

import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class SEmilRhyCARD {
   
	public SEmilRhyRC		rc;			/*  common */
	public SEmilRhyRF		rf;			/*  efx */
	public SEmilRhyRH		rh;			/*  chorus */
	public SEmilRhyRV		rv;			/*  reverb */
	public SEmilRhyRT[]		rt;		/*  tone */
	public SEmilRhyRL		rl;

    public SEmilRhyCARD() {
        rc = new SEmilRhyRC();
        rf = new SEmilRhyRF();
        rh = new SEmilRhyRH();
        rv = new  SEmilRhyRV();
        int siz = ParameterManager.getInstance().getMappaSize().get("NUMOF_EMRT");
        rt = new SEmilRhyRT[siz];
        for (int i=0; i< siz; i++)
            rt[i] = new SEmilRhyRT();
        rl = new SEmilRhyRL();
    }
    
    
}
