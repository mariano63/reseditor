package emil.semil;

import config.ParameterManager;
import emil.CEmilLib;

/**
 *
 * @author mgiacomo
 */
public class SEmilRhy {
   
    public static SEmilRhy[] gEmilTempRhy = new SEmilRhy[CEmilLib.EMIL_PART_PAT];

	public SEmilRhyRC		rc;			/*  common */
	public SEmilRhyRF		rf;			/*  efx */
	public SEmilRhyRH		rh;			/*  chorus */
	public SEmilRhyRV		rv;			/*  reverb */
	public SEmilRhyRT[]		rt;		/*  tone */
	public SEmilRhyRL		rl;

    public SEmilRhy() {
        rc = new SEmilRhyRC();
        rf = new SEmilRhyRF();
        rh = new SEmilRhyRH();
        rv = new  SEmilRhyRV();
        int siz = ParameterManager.getInstance().getMappaSize().get("NUMOF_EMRT");
        rt = new SEmilRhyRT[siz];
        for (int i = 0; i<siz; i++){
            rt[i] = new SEmilRhyRT();
        }
        rl = new SEmilRhyRL();
    }
    
    
}
