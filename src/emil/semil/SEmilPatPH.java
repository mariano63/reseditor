package emil.semil;

import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class SEmilPatPH {
  
    public byte[] data;

    public SEmilPatPH() {
        int size = ParameterManager.getInstance().getMappaSize().get("SIZEOF_EMPH");
        data = new byte[size];
    }  
}
