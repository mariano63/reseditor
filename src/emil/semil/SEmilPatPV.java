package emil.semil;

import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class SEmilPatPV {
   
    public byte[] data;

    public SEmilPatPV() {
        int size = ParameterManager.getInstance().getMappaSize().get("SIZEOF_EMPV");
        data = new byte[size];
    }   
}
