/*
 * Coincide con Mappa di ParameterManager
 */
package emil.semil;

import config.Parameter;
import config.ParameterManager;

/**
 *
 * @author mgiacomo
 */
public class SEmilPrmTbl {
    public short	adrs;
	public byte     bit;
	public byte     blank;
	public short	ofst;
	public short	uinit;  //+ofst
	public short	umin;   //+ofst
	public short	umax;   //+ofst
	public short	swUinit;  // No ofst
	public short	swUmin;   // No ofst
	public short	swUmax;   // No ofst
    

    public SEmilPrmTbl(int adrs, int bit, int blank, int ofst, int uinit, int umin, int umax) {
        this.adrs = (short)adrs;
        this.bit = (byte)bit;
        this.blank = (byte)blank;
        this.ofst = (short)ofst;
        this.swUinit = (short) uinit;
        this.swUmin = (short) umin;
        this.swUmax = (short) umax;
        this.uinit = (short) ((short)uinit+ofst);
        this.umin = (short) ((short)umin+ofst);
        this.umax = (short) ((short)umax+ofst);
    }
    
    
    public SEmilPrmTbl get(String ndx){
        Parameter prm = ParameterManager.getInstance().getMappa().get(ndx);
        return new SEmilPrmTbl(prm.getAdrs(), prm.getBit(), prm.getBlank(), prm.getOfst(), prm.getSwInit(), prm.getSwMin(), prm.getSwMax());
    }
}
