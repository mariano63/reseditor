package emil.ostm;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class COEmilStmPV extends COEmilStm{     // patch common reverb
	char mPart;

	static short mOfstID;
	void ClearSubPrm(long theType){
        
	if (theType >= ParameterManager.getPrmId("EMREV_TYPE_SIZE") ) {			// for safety
		return;
	}
    ClearRevBuf();
	long oldType = ReadValue((short)ParameterManager.getPrmId("EMPV_REV_TYPE"));
	SaveRevBuf((int) oldType, mOfstID);
	LoadRevBuf((int) theType, mOfstID);
    }
public void Open(int thePart){
	SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
	SetBuf((buf == null) ? null : buf.pv.data);
	mPart = (char) thePart;    
}
	public void Close(int theTx ){
        if (GetSizeofID() > 0) {
            if (theTx != 0) {
                ParameterManager.gVVPrmExcTx.EditPV(GetMinID(), GetMaxID(), mPart);
            }
        }
    }
    @Override
	public void Close(){Close(1);}
    @Override
	public void Write(long theValue, short theID){
        if (theID == ParameterManager.getPrmId("EMPV_REV_TYPE")){
            ClearSubPrm(theValue);
        }
        super.Write(theValue, theID, mOfstID);        
    }
	void Write() {}
}
