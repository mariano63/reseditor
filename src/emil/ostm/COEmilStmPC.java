package emil.ostm;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class COEmilStmPC extends COEmilStm{ // patch common
	byte mPart;
    
//--------------------------------------------------------------
// Open
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Open.
//	entry:	thePart		part number
//		theTone		tone number
//		theNote		note number
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
public void Open(int thePart)
{
	SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
	SetBuf((buf == null) ? null : buf.pc.data);
	mPart = (byte) thePart;
}
//--------------------------------------------------------------
// Close
//--------------------------------------------------------------
//
//	prog :	
//	func :	Close.
//	entry:	theTx		tx edit flag (OFF, ON)
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
public void Close(int theTx)
{
	if (GetSizeofID() > 0) {
		if (theTx != 0) {
			ParameterManager.gVVPrmExcTx.EditPC(GetMinID(), GetMaxID(), mPart);
		}
	}
}
}
