package emil.ostm;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public class COEmilStmRC extends COEmilStm{				// rhythm common
	byte mPart;
public	void Open(int thePart){
	SEmilRhy buf = ParameterManager.gVVTempAreaPart.GetEmilRhy(thePart);
	SetBuf((buf == null) ? null : buf.rc.data);
	mPart = (byte) thePart;
}
    @Override
    public void Close(){Close(1);}
	public void Close(int theTx){
        if (GetSizeofID() > 0) {
            if (theTx != 0) {
                ParameterManager.gVVPrmExcTx.EditRC(GetMinID(), GetMaxID(), mPart);
            }
        }
    }
    @Override
	public void Write(long theValue, short theID) { super.Write(theValue, theID); }
	public void Write() {}
	public void Write(SName theName){
        for (int i = 0; i < ParameterManager.getPrmId("EMRC_NAME_SIZE") ; i++) {
            if (theName.str[i] == '\0') {
                break;
            }
            String stNam = String.format("EMRT_NAME%d", i+1);
            Write(theName.str[i], (short) ParameterManager.getPrmId(stNam));
        }
    }
}
