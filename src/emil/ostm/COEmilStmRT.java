package emil.ostm;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public class COEmilStmRT extends COEmilStm {    			// rhythm tone

    byte mPart;
    byte mNote;
    byte mEditFlag;
    final byte vrangL = 0x01;
    final byte vrangU = 0x02;
//	union {
//		unsigned char	mEditFlag;
//		struct {
//			unsigned char	vrangL:	1;
//			unsigned char	vrangU:	1;
//		} mEdit;
//	};

    void EditVrangL() {
        for (int i = 0; i < ParameterManager.getPrmId("EMRT_WMT_SIZE"); i++) {
            String stLo = String.format("EMRT_WMT%d_VRANGE_LO", i + 1);
            String stup = String.format("EMRT_WMT%d_VRANGE_UP", i + 1);
            long lo = ReadValue((short) ParameterManager.getPrmId(stLo));
            long up = ReadValue((short) ParameterManager.getPrmId(stup));

            if (up < lo) {
                super.Write(lo, (short) ParameterManager.getPrmId(stup));
            }
        }
    }

    void EditVrangU() {
        for (int i = 0; i < ParameterManager.getPrmId("EMRT_WMT_SIZE"); i++) {
            String stLo = String.format("EMRT_WMT%d_VRANGE_LO", i + 1);
            String stup = String.format("EMRT_WMT%d_VRANGE_UP", i + 1);
            long lo = ReadValue((short) ParameterManager.getPrmId(stLo));
            long up = ReadValue((short) ParameterManager.getPrmId(stup));
            if (up < lo) {
                super.Write(up, (short) ParameterManager.getPrmId(stLo));
            }
        }
    }

    @Override
    public void Open(int thePart, int theNote) {

        SEmilRhy buf = ParameterManager.gVVTempAreaPart.GetEmilRhy(thePart);
        SetBuf((buf == null) ? null : buf.rt[theNote - ParameterManager.getPrmId("EMIL_RHY_NOTE_MIN")].data);
        mPart = (byte) thePart;
        mNote = (byte) theNote;
        mEditFlag = 0;		// clear all thr edit flag
    }

    @Override
    public void Close() {
        Close(1);
    }

    public void Close(int theTx) {
        if ((mEditFlag & vrangL) != 0) {
            EditVrangL();
        }
        if ((mEditFlag & vrangU) != 0) {
            EditVrangU();
        }
        if (GetSizeofID() > 0) {
            if (theTx != 0) {
                ParameterManager.gVVPrmExcTx.EditRT(GetMinID(), GetMaxID(), mPart, mNote);
            }
        }
    }

    @Override
    public void Write(long theValue, short theID) {

        if ((theID == ParameterManager.getPrmId("EMRT_WMT1_VRANGE_LO"))
                || (theID == ParameterManager.getPrmId("EMRT_WMT2_VRANGE_LO"))
                || (theID == ParameterManager.getPrmId("EMRT_WMT3_VRANGE_LO"))
                || (theID == ParameterManager.getPrmId("EMRT_WMT4_VRANGE_LO"))) {
            mEditFlag |= vrangL;
        } else if ((theID == ParameterManager.getPrmId("EMRT_WMT1_VRANGE_UP"))
                || (theID == ParameterManager.getPrmId("EMRT_WMT2_VRANGE_UP"))
                || (theID == ParameterManager.getPrmId("EMRT_WMT3_VRANGE_UP"))
                || (theID == ParameterManager.getPrmId("EMRT_WMT4_VRANGE_UP"))) {
            mEditFlag |= vrangU;
        }

        super.Write(theValue, theID);
    }

    public void Write(long theValue, int theID) {
        super.Write(theValue, (short) theID);
    }	// fot the BUG of SHC5.0

    public void Write() {
        mEditFlag = (byte) 0xff;
    }			// set all the edit flag

    public void Write(SName theName) {
        for (int i = 0; i < ParameterManager.getPrmId("EMRT_NAME_SIZE"); i++) {
            if (theName.str[i] == '\0') {
                break;
            }
            String st = String.format("EMRT_NAME%d", i + 1);
            Write(theName.str[i], ParameterManager.getPrmId(st));
        }
    }

    public void Write(SVRange theRange, int theWMT) // WMT # = 0 - 3
    {
        String stLo = String.format("EMRT_WMT%d_VRANGE_LO", theWMT + 1);
        String stUp = String.format("EMRT_WMT%d_VRANGE_UP", theWMT + 1);
        Write(theRange.lower, ParameterManager.getPrmId(stLo));
        Write(theRange.upper, ParameterManager.getPrmId(stUp));
        mEditFlag |= vrangL;
    }

    @Override
    public int WriteExc(byte[] theExc, short theID, int theSize) {

        int size = super.WriteExc(theExc, theID, theSize);
        short nextID = (short) (theID + size);

        if ((theID <= ParameterManager.getPrmId("EMRT_WMT1_VRANGE_LO")) && (ParameterManager.getPrmId("EMRT_WMT1_VRANGE_LO") < nextID)) {
            mEditFlag |= vrangL;
        }
        if ((theID <= ParameterManager.getPrmId("EMRT_WMT2_VRANGE_LO")) && (ParameterManager.getPrmId("EMRT_WMT2_VRANGE_LO") < nextID)) {
            mEditFlag |= vrangL;
        }
        if ((theID <= ParameterManager.getPrmId("EMRT_WMT3_VRANGE_LO")) && (ParameterManager.getPrmId("EMRT_WMT3_VRANGE_LO") < nextID)) {
            mEditFlag |= vrangL;
        }
        if ((theID <= ParameterManager.getPrmId("EMRT_WMT4_VRANGE_LO")) && (ParameterManager.getPrmId("EMRT_WMT4_VRANGE_LO") < nextID)) {
            mEditFlag |= vrangL;
        }
        if ((theID <= ParameterManager.getPrmId("EMRT_WMT1_VRANGE_UP")) && (ParameterManager.getPrmId("EMRT_WMT1_VRANGE_LO") < nextID)) {
            mEditFlag |= vrangU;
        }
        if ((theID <= ParameterManager.getPrmId("EMRT_WMT2_VRANGE_UP")) && (ParameterManager.getPrmId("EMRT_WMT2_VRANGE_LO") < nextID)) {
            mEditFlag |= vrangU;
        }
        if ((theID <= ParameterManager.getPrmId("EMRT_WMT3_VRANGE_UP")) && (ParameterManager.getPrmId("EMRT_WMT3_VRANGE_LO") < nextID)) {
            mEditFlag |= vrangU;
        }
        if ((theID <= ParameterManager.getPrmId("EMRT_WMT4_VRANGE_UP")) && (ParameterManager.getPrmId("EMRT_WMT4_VRANGE_LO") < nextID)) {
            mEditFlag |= vrangU;
        }
        return size;
    }
}
