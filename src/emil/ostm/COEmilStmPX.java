package emil.ostm;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class COEmilStmPX extends COEmilStm{
    byte mPart;
	byte mEditFlag;
    
//	union {
//		unsigned char	mEditFlag;
//		struct {
//			unsigned char	krangL:	1;
//			unsigned char	krangU:	1;
//			unsigned char	vrangL:	1;
//			unsigned char	vrangU:	1;
//		} mEdit;
//	};
    final byte krangL=0x01;
    final byte krangU=0x02;
    final byte vrangL=0x04;
    final byte vrangU=0x08;
    
public void EditKrangU()
{
	for (int i = 0; i < ParameterManager.getPrmId("EMPX_TMT_SIZE")  ; i++) {
        String stLo = String.format("EMPX_TMT%d_KRANGE_LO", i+1);
        String stHi = String.format("EMPX_TMT%d_KRANGE_UP", i+1);
        int lo = ParameterManager.getPrmId(stLo);
        int up = ParameterManager.getPrmId(stHi);
		if (up < lo) {
			super.Write(up, (short) lo);
		}
	}
}    
public void EditKrangL()
{
	for (int i = 0; i < ParameterManager.getPrmId("EMPX_TMT_SIZE")  ; i++) {
        String stLo = String.format("EMPX_TMT%d_KRANGE_LO", i+1);
        String stHi = String.format("EMPX_TMT%d_KRANGE_UP", i+1);
        int lo = ParameterManager.getPrmId(stLo);
        int up = ParameterManager.getPrmId(stHi);
//		short ofstID = EMPX_TMT(i + 1);
//		int lo = ReadValue(ofstID + EMPX_TMTx_KRANGE_LO);
//		int up = ReadValue(ofstID + EMPX_TMTx_KRANGE_UP);
		if (up < lo) {
			super.Write(lo, (short) up);
		}
	}
}    
public void EditVrangU()
{
	for (int i = 0; i < ParameterManager.getPrmId("EMPX_TMT_SIZE")  ; i++) {
        String stLo = String.format("EMPX_TMT%d_VRANGE_LO", i+1);
        String stHi = String.format("EMPX_TMT%d_VRANGE_UP", i+1);
        int lo = ParameterManager.getPrmId(stLo);
        int up = ParameterManager.getPrmId(stHi);
		if (up < lo) {
			super.Write(up, (short) lo);
		}
	}
}    
public void EditVrangL()
{
	for (int i = 0; i < ParameterManager.getPrmId("EMPX_TMT_SIZE")  ; i++) {
        String stLo = String.format("EMPX_TMT%d_VRANGE_LO", i+1);
        String stHi = String.format("EMPX_TMT%d_VRANGE_UP", i+1);
        int lo = ParameterManager.getPrmId(stLo);
        int up = ParameterManager.getPrmId(stHi);
		if (up < lo) {
			super.Write(lo, (short) up);
		}
	}
}   

public void Open(int thePart)
{
	SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
	SetBuf((buf == null) ? null : buf.px.data);
	mPart = (byte) thePart;
	mEditFlag = 0;		// clear all thr edit flag
}
    @Override
    public void Close(){Close(1);}
public void Close(int theTx)
{
	if ( (mEditFlag & krangL) != 0) {
		EditKrangL();
	}
	if ((mEditFlag & krangU) != 0) {
		EditKrangU();
	}
	if ( (mEditFlag & vrangL) != 0) {
		EditVrangL();
	}
	if ((mEditFlag & vrangU) != 0) {
		EditVrangU();
	}
	if (GetSizeofID() > 0) {
		if (theTx != 0) {
			ParameterManager.gVVPrmExcTx.EditPX(GetMinID(), GetMaxID(), mPart);
		}
	}
}
    
    @Override
    public void Write(long theValue, short theID)
{
    if (
            (theID == ParameterManager.getPrmId("EMPX_TMT1_KRANGE_LO")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT2_KRANGE_LO")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT3_KRANGE_LO")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT4_KRANGE_LO"))
            )
    {
        mEditFlag |= krangL;
    } else if (
            (theID == ParameterManager.getPrmId("EMPX_TMT1_KRANGE_UP")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT2_KRANGE_UP")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT3_KRANGE_UP")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT4_KRANGE_UP"))
            ) 
    {
        mEditFlag |= krangU;
    }else if (
            (theID == ParameterManager.getPrmId("EMPX_TMT1_VRANGE_LO")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT2_VRANGE_LO")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT3_VRANGE_LO")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT4_VRANGE_LO"))
            ) 
    {
        mEditFlag |= vrangL;
    } else if (
            (theID == ParameterManager.getPrmId("EMPX_TMT1_VRANGE_UP")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT2_VRANGE_UP")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT3_VRANGE_UP")) ||
            (theID == ParameterManager.getPrmId("EMPX_TMT4_VRANGE_UP"))
            )
    {
        mEditFlag |= vrangU;
    }
	super.Write(theValue, theID);
}
    
public void Write(long theValue, int theID) { Write(theValue, (short)theID); }	// for the BUG of SHC5.0

//--------------------------------------------------------------
// Write (keyboard range)
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Write the keyboard range.
//	entry:	theRange	keyboard range
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
public void Write(SKRange theRange, int theTone)
{
    String stLo = String.format("EMPX_TMT%d_KRANGE_LO", theTone + 1);
    String stUp = String.format("EMPX_TMT%d_KRANGE_UP", theTone + 1);
	Write(theRange.lower, ParameterManager.getPrmId(stLo));
	Write(theRange.upper, ParameterManager.getPrmId(stUp));
	mEditFlag |= krangL;
}

//--------------------------------------------------------------
// Write (velocity range)
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Write the velocity range.
//	entry:	theRange	velocity range
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
public void Write(SVRange theRange, int theTone)
{
    String stLo = String.format("EMPX_TMT%d_VRANGE_LO", theTone + 1);
    String stUp = String.format("EMPX_TMT%d_VRANGE_UP", theTone + 1);
	Write(theRange.lower, ParameterManager.getPrmId(stLo));
	Write(theRange.upper, ParameterManager.getPrmId(stUp));
    
	Write(theRange.lower, ParameterManager.getPrmId(stLo));
	Write(theRange.upper, ParameterManager.getPrmId(stUp));
	mEditFlag |= vrangL;
}
	public void Write() { mEditFlag = ~0; }			// set all the edit flag
    
    @Override
    public int WriteExc(byte[] theExc, short theID, int theSize)
{
	int size = super.WriteExc(theExc, theID, theSize);
	short nextID = (short) (theID + size);
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT1_KRANGE_LO")) && (ParameterManager.getPrmId("EMPX_TMT1_KRANGE_LO") < nextID)) {
		mEditFlag |= krangL;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT2_KRANGE_LO")) && (ParameterManager.getPrmId("EMPX_TMT2_KRANGE_LO") < nextID)) {
		mEditFlag |= krangL;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT3_KRANGE_LO")) && (ParameterManager.getPrmId("EMPX_TMT3_KRANGE_LO") < nextID)) {
		mEditFlag |= krangL;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT3_KRANGE_LO")) && (ParameterManager.getPrmId("EMPX_TMT3_KRANGE_LO") < nextID)) {
		mEditFlag |= krangL;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT1_KRANGE_UP")) && (ParameterManager.getPrmId("EMPX_TMT1_KRANGE_UP") < nextID)) {
		mEditFlag |= krangU;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT2_KRANGE_UP")) && (ParameterManager.getPrmId("EMPX_TMT2_KRANGE_UP") < nextID)) {
		mEditFlag |= krangU;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT3_KRANGE_UP")) && (ParameterManager.getPrmId("EMPX_TMT3_KRANGE_UP") < nextID)) {
		mEditFlag |= krangU ;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT3_KRANGE_UP")) && (ParameterManager.getPrmId("EMPX_TMT3_KRANGE_UP") < nextID)) {
		mEditFlag |= krangU;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT1_VRANGE_LO")) && (ParameterManager.getPrmId("EMPX_TMT1_VRANGE_LO") < nextID)) {
		mEditFlag |= vrangL;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT2_VRANGE_LO")) && (ParameterManager.getPrmId("EMPX_TMT2_VRANGE_LO") < nextID)) {
		mEditFlag |= vrangL ;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT3_VRANGE_LO")) && (ParameterManager.getPrmId("EMPX_TMT3_VRANGE_LO") < nextID)) {
		mEditFlag |= vrangL;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT3_VRANGE_LO")) && (ParameterManager.getPrmId("EMPX_TMT3_VRANGE_LO") < nextID)) {
		mEditFlag |= vrangL;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT1_VRANGE_UP")) && (ParameterManager.getPrmId("EMPX_TMT1_VRANGE_UP") < nextID)) {
		mEditFlag |= vrangU ;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT2_VRANGE_UP")) && (ParameterManager.getPrmId("EMPX_TMT2_VRANGE_UP") < nextID)) {
		mEditFlag |= vrangU;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT3_VRANGE_UP")) && (ParameterManager.getPrmId("EMPX_TMT3_VRANGE_UP") < nextID)) {
		mEditFlag |= vrangU;
	}
	if ((theID <= ParameterManager.getPrmId("EMPX_TMT3_VRANGE_UP")) && (ParameterManager.getPrmId("EMPX_TMT3_VRANGE_UP") < nextID)) {
		mEditFlag |= vrangU;
	}
	return size;
}
}
