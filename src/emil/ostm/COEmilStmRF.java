package emil.ostm;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public class COEmilStmRF extends COEmilStm{				// rhythm common EFX
	byte mPart;

	static short mOfstID;
        
void ClearSubPrm(long theType)
{
	if (theType >= ParameterManager.getPrmId("EMEFX_TYPE_SIZE") ) {			// for safety
		return;
	}
	ClearEfxBuf();
	long oldType = ReadValue((short)ParameterManager.getPrmId("EMRF_EFX_TYPE"));
	SaveEfxBuf((int) oldType, mOfstID);
	LoadEfxBuf((int) theType, mOfstID);
}
	public void Open(int thePart){
        SEmilRhy buf = ParameterManager.gVVTempAreaPart.GetEmilRhy(thePart);
        SetBuf((buf == null) ? null : buf.rf.data);
        mPart = (byte) thePart;
    }
    @Override
    public void Close(){Close(1);}
	public void Close(int theTx){
        if (GetSizeofID() > 0) {
            if (theTx != 0) {
                ParameterManager.gVVPrmExcTx.EditRF(GetMinID(), GetMaxID(), mPart);
            }
        }
    }
    @Override
	public void Write(long theValue, short theID){
        if (theID == ParameterManager.getPrmId("EMRF_EFX_TYPE")){
            ClearSubPrm(theValue);
        }
        super.Write(theValue, theID, mOfstID);
    }
	public void Write() {}
    
}
