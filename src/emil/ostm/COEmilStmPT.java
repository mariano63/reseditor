package emil.ostm;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class COEmilStmPT extends COEmilStm {				// patch tone

    byte mPart;
    byte mTone;

    @Override
    public void Open(int thePart, int theTone) {
        SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
        SetBuf((buf == null) ? null : buf.pt[theTone].data);
        mPart = (byte) thePart;
        mTone = (byte) theTone;
    }

    @Override
    public void Close() {
        Close(1);
    }

    public void Close(int theTx) {
        if (GetSizeofID() > 0) {
            if (theTx != 0) {
                ParameterManager.gVVPrmExcTx.EditPT(GetMinID(), GetMaxID(), mPart, mTone);
            }
        }
    }

    @Override
    public void Write(long theValue, short theID) {
        super.Write(theValue, theID);
    }

    void Write() {
    }

    @Override
    public int WriteExc(byte[] theExc, short theID, int theSize) {
        int size = super.WriteExc(theExc, theID, theSize);
        return size;
    }
}
