package emil.ostm;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public class COEmilStmRH extends COEmilStm{				// rhythm common chorus
	byte mPart;

	static short    mOfstID;
	void ClearSubPrm(long theType){

        if (theType >= ParameterManager.getPrmId("EMCHO_TYPE_SIZE") ) {			// for safety
            return;
        }
        ClearChoBuf();
        long oldType = ReadValue((short)ParameterManager.getPrmId("EMRH_CHO_TYPE"));
        SaveChoBuf((int) oldType, mOfstID);
        LoadChoBuf((int) theType, mOfstID);
    }
public void Open(int thePart){
    
	SEmilRhy buf = ParameterManager.gVVTempAreaPart.GetEmilRhy(thePart);
	SetBuf((buf == null) ? null : buf.rh.data);
	mPart = (byte) thePart;
}
    @Override
    public void Close(){ Close(1);}
	public void Close(int theTx){
        if (GetSizeofID() > 0) {
            if (theTx != 0) {
                ParameterManager.gVVPrmExcTx.EditRH(GetMinID(), GetMaxID(), mPart);
            }
        }
    }
    @Override
	public void Write(long theValue, short theID){
        if (theID != ParameterManager.getPrmId("EMRH_CHO_TYPE")){
            ClearSubPrm(theValue);
        }
        super.Write(theValue, theID, mOfstID);        
    }
	public void Write() {}    
}
