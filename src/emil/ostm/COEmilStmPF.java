
package emil.ostm;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
    public class COEmilStmPF extends COEmilStm{
    			// patch common EFX
	byte    mPart;

	static short	mOfstID;

//--------------------------------------------------------------
// ClearSubPrm
//--------------------------------------------------------------
//
//	prog :	tsuyo
//	func :	Initialize sub parameters.
//	entry:	PrmValue	type
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
void ClearSubPrm(long theType)
{
	if (theType >= ParameterManager.getPrmId("EMEFX_TYPE_SIZE") ) {			// for safety
		return;
	}
//	CEmilXferTempPat aXfer(mPart);
//	if (aXfer.SetEditEfx() == CVVPrmXfer::Unchanged) {	// patch changed?
		ClearEfxBuf();
//	}
	long oldType = ReadValue((short)(ParameterManager.getPrmId("EMPF_EFX_TYPE")));
	SaveEfxBuf((int)oldType, mOfstID);
	LoadEfxBuf((int)theType, mOfstID);
}
	public void Open(int thePart){
        SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
        SetBuf((buf == null) ? null : buf.pf.data);
        mPart = (byte) thePart;        
    }
	public void Close(int theTx){
        if (GetSizeofID() > 0) {
            if (theTx!=0) {
                ParameterManager.gVVPrmExcTx.EditPF(GetMinID(), GetMaxID(), mPart);
            }
        }        
    }
    @Override
	public void Write(long theValue, short theID){
        if (theID == ParameterManager.getPrmId("EMPF_EFX_TYPE")){
            ClearSubPrm(theValue);
        } 
        super.Write(theValue, theID, mOfstID);
    }
    
	public void Write() {}
}
