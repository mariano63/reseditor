package emil.ostm;

import config.ParameterManager;
import emil.COEmilStm;
import emil.semil.SEmilPat;

/**
 *
 * @author mgiacomo
 */
public class COEmilStmPH extends COEmilStm{				// patch common chorus
	char mPart;
	static short mOfstID;
	void ClearSubPrm(long theType){
        if (theType >= ParameterManager.getPrmId("EMCHO_TYPE_SIZE")) {			// for safety
            return;
        }
    //	CEmilXferTempPat aXfer(mPart);
    //	if (aXfer.SetEditCho() == CVVPrmXfer::Unchanged) {	// patch changed?
            ClearChoBuf();
    //	}
        int oldType = (int) ReadValue((short)ParameterManager.getPrmId("EMPH_CHO_TYPE"));
        SaveChoBuf(oldType, mOfstID);
        LoadChoBuf((int) theType, mOfstID);

    }
public	void Open(int thePart){

	SEmilPat buf = ParameterManager.gVVTempAreaPart.GetEmilPat(thePart);
	SetBuf((buf == null) ? null : buf.ph.data);
	mPart = (char) thePart;
}

	public void Close(int theTx){

        if (GetSizeofID() > 0) {
            if (theTx != 0) {
                ParameterManager.gVVPrmExcTx.EditPH(GetMinID(), GetMaxID(), mPart);
            }
        }
    }
    @Override
	public void Close(){
        Close(1);
    }
    
    @Override
	public void Write(long theValue, short theID){
        
        if (theID == ParameterManager.getPrmId("EMPH_CHO_TYPE")){
            
		ClearSubPrm(theValue);
        }
    	super.Write(theValue, theID, mOfstID);
    }
	void Write() {}
    
}
