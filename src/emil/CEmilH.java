/*
 * .H classe Emil
 */
package emil;

/**
 *
 * @author mgiacomo
 */
public class CEmilH {
    
    /* Wave Group Type */
final static public int EM_WAV_GTYPE_WAVE_ROM=0;		/*  WAVE-ROM */
final static public int EM_WAV_GTYPE_UNIV_EXP=1;		/*  UNIV-EXP */
final static public int EM_WAV_GTYPE_VALUES=2;

    /* Performance/Patch Group ID */
	final static public int EM_PAT_GID_NONE=0;				/*  NO-ASSIGN */
	final static public int EM_PAT_GID_INTA=1;				/*  INT-A */
	final static public int EM_PAT_GID_INTB=2;				/*  INT-B */
	final static public int EM_PAT_GID_VALUES=128;

    /* Wave Group ID */
	final static public int EM_WAV_GID_NONE=0;				/*  NO-ASSIGN */
	final static public int EM_WAV_GID_VALUES = 128;

    /* Patch Attribute */
	final static public int EM_PAT_ATTR_UNDEF=0;				/*  UNDEFINED */
	final static public int EM_PAT_ATTR_PAT=1;				/*  PATCH */
	final static public int EM_PAT_ATTR_RHY=2;				/*  RHYTHM */
	final static public int M_PAT_ATTR_VALUES=3;
            
    /* Control Source */
	final static public int EM_CTRL_SRC_OFF=0;				/*  OFF */
	final static public int EM_CTRL_SRC_CC01=1;				/*  CC01 */
	final static public int EM_CTRL_SRC_CC02=2;			/*  CC02 */
	final static public int EM_CTRL_SRC_CC04 = 4;				/*  CC04 */
	final static public int EM_CTRL_SRC_CC32 = 32;				/*  CC32 */
	final static public int EM_CTRL_SRC_CC95 = 95;				/*  CC95 */
	final static public int EM_CTRL_SRC_BEND = 96;				/*  BEND */
	final static public int EM_CTRL_SRC_AFT=97;				/*  AFT */
	final static public int EM_CTRL_SRC_SYS1=98;				/*  SYS1 */
	final static public int EM_CTRL_SRC_SYS2=99;				/*  SYS2 */
	final static public int EM_CTRL_SRC_SYS3=100;				/*  SYS3 */
	final static public int EM_CTRL_SRC_SYS4=101;				/*  SYS4 */
	final static public int EM_CTRL_SRC_VELOCITY=102;				/*  VELOCITY */
	final static public int EM_CTRL_SRC_KEYFOLLOW=103;				/*  KEYFOLLOW */
	final static public int EM_CTRL_SRC_TEMPO=104;				/*  TEMPO */
	final static public int EM_CTRL_SRC_LFO1=105;				/*  LFO1 */
	final static public int EM_CTRL_SRC_LFO2=106;				/*  LFO2 */
	final static public int EM_CTRL_SRC_PENV=107;				/*  PIT-ENV */
	final static public int EM_CTRL_SRC_FENV=108;				/*  TVF-ENV */
	final static public int EM_CTRL_SRC_AENV=109;			/*  TVA-ENV */
	final static public int EM_CTRL_SRC_VALUES=110;
    
    /* Control Destination */
	final static public int EM_CTRL_DST_OFF=0;				/*  OFF */
	final static public int EM_CTRL_DST_PCH=1;				/*  PCH */
	final static public int EM_CTRL_DST_CUT=2;				/*  CUT */
	final static public int EM_CTRL_DST_RES=3;				/*  RES */
	final static public int EM_CTRL_DST_LEV=4;				/*  LEV */
	final static public int EM_CTRL_DST_PAN=5;				/*  PAN */
	final static public int EM_CTRL_DST_DRY=6;				/*  DRY */
	final static public int EM_CTRL_DST_CHO=7;				/*  CHO */
	final static public int EM_CTRL_DST_REV=8;				/*  REV */
	final static public int EM_CTRL_DST_PL1=9;				/*  PL1 */
	final static public int EM_CTRL_DST_PL2=10;				/*  PL2 */
	final static public int EM_CTRL_DST_FL1=11;				/*  FL1 */
	final static public int EM_CTRL_DST_FL2=12;				/*  FL2 */
	final static public int EM_CTRL_DST_AL1=13;				/*  AL1 */
	final static public int EM_CTRL_DST_AL2=14;				/*  AL2 */
	final static public int EM_CTRL_DST_NL1=15;				/*  NL1 */
	final static public int EM_CTRL_DST_NL2=16;				/*  NL2 */
	final static public int EM_CTRL_DST_L1R=17;				/*  L1R */
	final static public int EM_CTRL_DST_L2R=18;				/*  L2R */
	final static public int EM_CTRL_DST_PIT_ATK=19;				/*  PIT-ATK */
	final static public int EM_CTRL_DST_PIT_DCY=20;				/*  PIT-DCY */
	final static public int EM_CTRL_DST_PIT_REL=21;				/*  PIT-REL */
	final static public int EM_CTRL_DST_TVF_ATK=22;				/*  TVF-ATK */
	final static public int EM_CTRL_DST_TVF_DCY=23;				/*  TVF-DCY */
	final static public int EM_CTRL_DST_TVF_REL=24;				/*  TVF-REL */
	final static public int EM_CTRL_DST_TVA_ATK=25;				/*  TVA-ATK */
	final static public int EM_CTRL_DST_TVA_DCY=26;				/*  TVA-DCY */
	final static public int EM_CTRL_DST_TVA_REL=27;				/*  TVA-REL */
	final static public int EM_CTRL_DST_TMT=28;				/*  TMT */
	final static public int EM_CTRL_DST_WMT = EM_CTRL_DST_TMT;		/*  WMT */
	final static public int EM_CTRL_DST_FXM = 29;				/*  FXM */
	final static public int EM_CTRL_DST_EFX1=30;				/*  EFX1 */
	final static public int EM_CTRL_DST_EFX2=31;				/*  EFX2 */
	final static public int EM_CTRL_DST_EFX3=32;				/*  EFX3 */
	final static public int EM_CTRL_DST_EFX4=33;				/*  EFX4 */
	final static public int EM_CTRL_DST_SMP_SPD=34;				/*  SMP_SPD */
	final static public int EM_CTRL_DST_VALUES=35;

    /* EFX Output Assign */
	final static public int EM_EFX_OUT_ASGN_A=0;				/*  A */
	final static public int EM_EFX_OUT_ASGN_B=1;				/*  B */
	final static public int EM_EFX_OUT_ASGN_C=2;				/*  C */
	final static public int EM_EFX_OUT_ASGN_D=3;				/*  D */
	final static public int EM_EFX_OUT_ASGN_VALUES=4;

    /* Chorus Output Assign */
	final static public int EM_CHO_OUT_ASGN_A=0;				/*  A */
	final static public int EM_CHO_OUT_ASGN_B=1;				/*  B */
	final static public int EM_CHO_OUT_ASGN_C=2;				/*  C */
	final static public int EM_CHO_OUT_ASGN_D=3;				/*  D */
	final static public int EM_CHO_OUT_ASGN_VALUES=4;

    /* Reverb Output Assign */
	final static public int EM_REV_OUT_ASGN_A=0;				/*  A */
	final static public int EM_REV_OUT_ASGN_B=1;				/*  B */
	final static public int EM_REV_OUT_ASGN_C=2;				/*  C */
	final static public int EM_REV_OUT_ASGN_D=3;				/*  D */
	final static public int EM_REV_OUT_ASGN_VALUES=4;

    /* Output Assign */
	final static public int EM_OUT_ASGN_EFX=0;				/*  EFX */
	final static public int EM_OUT_ASGN_A=1;					/*  A */
	final static public int EM_OUT_ASGN_B=2;					/*  B */
	final static public int EM_OUT_ASGN_C=3;					/*  C */
	final static public int EM_OUT_ASGN_D=4;					/*  D */
	final static public int EM_OUT_ASGN_1=5;					/*  1 */
	final static public int EM_OUT_ASGN_2=6;					/*  2 */
	final static public int EM_OUT_ASGN_3=7;					/*  3 */
	final static public int EM_OUT_ASGN_4=8;					/*  4 */
	final static public int EM_OUT_ASGN_5=9;					/*  5 */
	final static public int EM_OUT_ASGN_6=10;					/*  6 */
	final static public int EM_OUT_ASGN_7=11;					/*  7 */
	final static public int EM_OUT_ASGN_8=12;					/*  8 */
	final static public int EM_OUT_ASGN_PATCH=13;				/*  PATCH */
	final static public int EM_OUT_ASGN_TONE = EM_OUT_ASGN_PATCH;		/*  TONE */
	final static public int EM_OUT_ASGN_VALUES=14;

    /* Assign Type */
	final static public int EM_ASGN_TYPE_MULTI=0;				/*  MULTI */
	final static public int EM_ASGN_TYPE_SINGLE=1;				/*  SINGLE */
	final static public int EM_ASGN_TYPE_ALTERNATE=2;				/*  ALTERNATE */
	final static public int EM_ASGN_TYPE_VALUES=3;
    
    /* Random Pitch Depth */
	final static public int EM_PIT_RND_0=0;					/*  0 */
	final static public int EM_PIT_RND_1=1;					/*  1 */
	final static public int EM_PIT_RND_2=2;					/*  2 */
	final static public int EM_PIT_RND_3=3;					/*  3 */
	final static public int EM_PIT_RND_4=4;					/*  4 */
	final static public int EM_PIT_RND_5=5;					/*  5 */
	final static public int EM_PIT_RND_6=6;					/*  6 */
	final static public int EM_PIT_RND_7=7;					/*  7 */
	final static public int EM_PIT_RND_8=8;					/*  8 */
	final static public int EM_PIT_RND_9=9;					/*  9 */
	final static public int EM_PIT_RND_10=10;					/*  10 */
	final static public int EM_PIT_RND_20=11;					/*  20 */
	final static public int EM_PIT_RND_30=12;					/*  30 */
	final static public int EM_PIT_RND_40=13;					/*  40 */
	final static public int EM_PIT_RND_50=14;					/*  50 */
	final static public int EM_PIT_RND_60=15;					/*  60 */
	final static public int EM_PIT_RND_70=16;					/*  70 */
	final static public int EM_PIT_RND_80=17;					/*  80 */
	final static public int EM_PIT_RND_90=18;					/*  90 */
	final static public int EM_PIT_RND_100=19;					/*  100 */
	final static public int EM_PIT_RND_200=20;					/*  200 */
	final static public int EM_PIT_RND_300=21;					/*  300 */
	final static public int EM_PIT_RND_400=22;					/*  400 */
	final static public int EM_PIT_RND_500=23;					/*  500 */
	final static public int EM_PIT_RND_600=24;					/*  600 */
	final static public int EM_PIT_RND_700=25;					/*  700 */
	final static public int EM_PIT_RND_800=26;					/*  800 */
	final static public int EM_PIT_RND_900=27;					/*  900 */
	final static public int EM_PIT_RND_1000=28;				/*  1000 */
	final static public int EM_PIT_RND_1100=29;				/*  1100 */
	final static public int EM_PIT_RND_1200=30;				/*  1200 */
	final static public int EM_PIT_RND_VALUES=31;

    
/* Env Mode */
	final static public int EM_ENV_MODE_NO_SUS=0;				/*  NO-SUS */
	final static public int EM_ENV_MODE_SUSTAIN=1;				/*  SUSTAIN */
	final static public int EM_ENV_MODE_VALUES=2;

    /* Delay Mode */
	final static public int EM_DELAY_MODE_NORMAL=0;				/*  NORMAL */
	final static public int EM_DELAY_MODE_HOLD=1;				/*  HOLD */
	final static public int EM_DELAY_MODE_KEY_OFF_N=2;			/*  KEY-OFF-N */
	final static public int EM_DELAY_MODE_KEY_OFF_D=3;			/*  KEY-OFF-D */
	final static public int EM_DELAY_MODE_VALUES=4;

    /* Receive Pan Mode */
	final static public int EM_RX_PAN_MODE_CONTINUOUS=0;			/*  CONTINUOUS */
	final static public int EM_RX_PAN_MODE_KEY_ON=1;				/*  KEY-ON */
	final static public int EM_RX_PAN_MODE_VALUES=2;

    /* Wave Gain  */
	final static public int EM_WAV_GAIN_M6=0;					/*  -6 */
	final static public int EM_WAV_GAIN_0=1;					/*  0 */
	final static public int EM_WAV_GAIN_P6=2;					/*  +6 */
	final static public int EM_WAV_GAIN_P12=3;				/*  +12 */
	final static public int EM_WAV_GAIN_VALUES=4;
    
    /* TVF Filter Type */
	final static public int EM_FILTER_TYPE_OFF=0;				/*  OFF */
	final static public int EM_FILTER_TYPE_LPF=1;				/*  LPF */
	final static public int EM_FILTER_TYPE_BPF=2;				/*  BPF */
	final static public int EM_FILTER_TYPE_HPF=3;				/*  HPF */
	final static public int EM_FILTER_TYPE_PKG=4;				/*  PKG */
	final static public int EM_FILTER_TYPE_LPF2=5;				/*  LPF2 */
	final static public int EM_FILTER_TYPE_LPF3=6;				/*  LPF3 */
	final static public int EM_FILTER_TYPE_LBST=7;				/*  LBST */
	final static public int EM_FILTER_TYPE_VALUES=8;

    /* TVA Keyfollow Direction */
	final static public int EM_LEVEL_KF_DIR_LOWER=0;				/*  LOWER */
	final static public int EM_LEVEL_KF_DIR_UPPER=1;				/*  UPPER */
	final static public int EM_LEVEL_KF_DIR_LOW_UP=2;				/*  LOW & UP */
	final static public int EM_LEVEL_KF_DIR_ALL=3;				/*  ALL */
	final static public int EM_LEVEL_KF_DIR_VALUES=4;

    /* LFO Wave Form */
	final static public int EM_LFO_FORM_SIN=0;				/*  SIN */
	final static public int EM_LFO_FORM_TRI=1;				/*  TRI */
	final static public int EM_LFO_FORM_SAW_UP=2;				/*  SAW-UP */
	final static public int EM_LFO_FORM_SAW_DW=3;				/*  SAW-DW */
	final static public int EM_LFO_FORM_SQR=4;				/*  SQR */
	final static public int EM_LFO_FORM_RND=5;				/*  RND */
	final static public int EM_LFO_FORM_BEND_UP=6;				/*  BEND-UP */
	final static public int EM_LFO_FORM_BEND_DW=7;				/*  BEND-DW */
	final static public int EM_LFO_FORM_TRP=8;				/*  TRP */
	final static public int EM_LFO_FORM_S_H=9;				/*  S&H */
	final static public int EM_LFO_FORM_CHS=10;				/*  CHS */
	final static public int EM_LFO_FORM_VSIN=11;				/*  VSIN */
	final static public int EM_LFO_FORM_STEP=12;				/*  STEP */
	final static public int EM_LFO_FORM_XSIN=13;			/*  XSIN */
	final static public int EM_LFO_FORM_TWM=14;				/*  TWM */
	final static public int EM_LFO_FORM_STRS=15;				/*  STP */
	final static public int EM_LFO_FORM_TRMN=16;				/*  TRMN */
	final static public int EM_LFO_FORM_VALUES=17;

/* LFO Offset */
	final static public int EM_LFO_OFST_M100=0;				/*  -100 */
	final static public int EM_LFO_OFST_M50=1;				/*  -50 */
	final static public int EM_LFO_OFST_0=2;					/*  0 */
	final static public int EM_LFO_OFST_P50=3;				/*  +50 */
	final static public int EM_LFO_OFST_P100=4;				/*  +100 */
	final static public int EM_LFO_OFST_VALUES=5;

    /* LFO Fade Mode */
	final static public int EM_LFO_FADE_MODE_KEY_ON_IN=0;			/*  KEY-ON-IN */
	final static public int EM_LFO_FADE_MODE_KEY_ON_OUT=1;			/*  KEY-ON-OUT */
	final static public int EM_LFO_FADE_MODE_KEY_OFF_IN=2;			/*  KEY-OFF-IN */
	final static public int EM_LFO_FADE_MODE_KEY_OFF_OUT=3;			/*  KEY-OFF-OUT */
	final static public int EM_LFO_FADE_MODE_VALUES=4;

    /* LFO Step Type */
	final static public int EM_LFO_STEP_TYPE_FLAT=0;				/*  FLAT */
	final static public int EM_LFO_STEP_TYPE_LINEAR=1;				/*  LINEAR */
    
    /* WMT Wave Pan Keyfollow / Alternate Pan / Pan LFO Switch */
	final static public int EM_WMT_PAN_SW_OFF=0;				/*  OFF */
	final static public int EM_WMT_PAN_SW_ON=1;				/*  ON */
	final static public int EM_WMT_PAN_SW_REVERSE=2;				/*  REVERSE */
	final static public int EM_WMT_PAN_VALUES=3;

    /* Mono/Poly */
	final static public int EM_MONO_POLY_MONO=0;				/*  MONO */
	final static public int EM_MONO_POLY_POLY=1;				/*  POLY */
	final static public int EM_MONO_POLY_PATCH=2;				/*  PATCH */
	final static public int EM_MONO_POLY_VALUES=3;

     /* Musical Notes */
	final static public int EM_MUSICAL_NOTE_64TH_T = 128;			/*  Sixty-fourth triplet */
	final static public int EM_MUSICAL_NOTE_64TH=129;				/*  Sixty-fourth note */
	final static public int EM_MUSICAL_NOTE_32ND_T=130;			/*  Thirty-second triplet */
	final static public int EM_MUSICAL_NOTE_32ND=131;				/*  Thirty-second note */
	final static public int EM_MUSICAL_NOTE_16TH_T=132;				/*  Sixteenth-note triplet */
	final static public int EM_MUSICAL_NOTE_32ND_D=133;				/*  Dotted thirty-second note */
	final static public int EM_MUSICAL_NOTE_16TH=134;			/*  Sixteenth note */
	final static public int EM_MUSICAL_NOTE_8TH_T=135;				/*  Eighth-note triplet */
	final static public int EM_MUSICAL_NOTE_16TH_D=136;				/*  Dotted sixteenth note */
	final static public int EM_MUSICAL_NOTE_8TH=137;			/*  Eighth note */
	final static public int EM_MUSICAL_NOTE_QUARTER_T=138;			/*  Quarter-note triplet */
	final static public int EM_MUSICAL_NOTE_8TH_D=139;				/*  Dotted eighth note */
	final static public int EM_MUSICAL_NOTE_QUARTER=140;			/*  Quarter note */
	final static public int EM_MUSICAL_NOTE_HALF_T=141;				/*  Half-note triplet */
	final static public int EM_MUSICAL_NOTE_QUARTER_D=142;			/*  Dotted quarter note */
	final static public int EM_MUSICAL_NOTE_HALF=143;				/*  Half note */
	final static public int EM_MUSICAL_NOTE_WHOLE_T=144;			/*  Whole-note triplet */
	final static public int EM_MUSICAL_NOTE_HALF_D=145;				/*  Dotted half note */
	final static public int EM_MUSICAL_NOTE_WHOLE=146;				/*  Whole note */
	final static public int EM_MUSICAL_NOTE_DOUBLE_T=147;		/*  Double-note triplet */
	final static public int EM_MUSICAL_NOTE_WHOLE_D=148;			/*  Dotted whole note */
	final static public int EM_MUSICAL_NOTE_DOUBLE=149;				/*  Double note */
	final static public int EM_MUSICAL_NOTE_MIN = EM_MUSICAL_NOTE_32ND_T;
	final static public int EM_MUSICAL_NOTE_MAX = EM_MUSICAL_NOTE_DOUBLE;

    /* Velocity Control */
	final static public int EM_VELO_CTRL_OFF=0;				/*  OFF */
	final static public int EM_VELO_CTRL_ON=1;				/*  ON */
	final static public int EM_VELO_CTRL_RANDOM=2;				/*  RANDOM */
	final static public int EM_VELO_CTRL_ROTARY=3;				/*  ROTARY */
	final static public int EM_VELO_CTRL_VALUES=4;

    
    
/*
 * Patch Common
 */
    /* Tone Type */
	final static public int EMPC_TONE_TYPE_4TONES=0;				/*  4TONES */
	final static public int EMPC_TONE_TYPE_MULTI=1;			/*  MULTI-PARTIAL */
	final static public int EMPC_TONE_TYPE_VALUES=2;

    /* Patch Priority */
	final static public int EMPC_PRIORITY_LAST=0;				/*  LAST */
	final static public int EMPC_PRIORITY_LOUDEST=1;				/*  LOUDEST */
	final static public int EMPC_PRIORITY_VALUES=2;

    /* Portamento Mode */
	final static public int EMPC_PORT_MODE_NORMAL=0;				/*  NORMAL */
	final static public int EMPC_PORT_MODE_LEGATO=1;				/*  LEGATO */
	final static public int EMPC_PORT_MODE_VALUES=2;

    /* Portamento Type */
	final static public int EMPC_PORT_TYPE_RATE=0;				/*  RATE */
	final static public int EMPC_PORT_TYPE_TIME=1;				/*  TIME */
	final static public int EMPC_PORT_TYPE_VALUES=2;

    /* Portamento Start */
	final static public int EMPC_PORT_START_PITCH=0;				/*  PITCH */
	final static public int EMPC_PORT_START_NOTE=1;				/*  NOTE */
	final static public int EMPC_PORT_START_VALUES=2;

    /* Clock Source */
	final static public int EMPC_CLK_SRC_PAT=0;				/*  PATCH */
	final static public int EMPC_CLK_SRC_SYS=1;				/*  SYSTEM */
	final static public int EMPC_CLK_SRC_SEQ = EMPC_CLK_SRC_SYS;		/*  SEQUENCER */
	final static public int EMPC_CLK_SRC_VALUES=2;

    
	final static public int EMRC_CLK_SRC_RHY=0;				/*  RHYTHM */
	final static public int EMRC_CLK_SRC_SYS=1;				/*  SYSTEM */
	final static public int EMRC_CLK_SRC_SEQ = EMRC_CLK_SRC_SYS;		/*  SEQUENCER */
	final static public int EMRC_CLK_SRC_VALUES=2;

    
/*
 * Patch Tone Mix Table
 */

    /* Booster */
	final static public int EMPX_BOOST_0=0;					/*  0 */
	final static public int EMPX_BOOST_P6=1;					/*  +6 */
	final static public int EMPX_BOOST_P12=2;					/*  +12 */
	final static public int EMPX_BOOST_P18=3;					/*  +18 */
	final static public int EMPX_BOOST_VALUES=4;

/*
 * Patch Tone
 */
    /* Tone Control Switch */
	final static public int EMPT_CTRL_SW_OFF=0;				/*  OFF */
	final static public int EMPT_CTRL_SW_ON=1;				/*  ON */
	final static public int EMPT_CTRL_SW_REVERSE=2;				/*  REVERSE */
	final static public int EMPT_CTRL_SW_VALUES=3;

/*
 * For the old revisions
 */
    /* Output Assign (PATCH-EFX) */
	final static public int EM_OUT_ASGN1_EFX=0;				/*  EFX */
	final static public int EM_OUT_ASGN1_A=1;					/*  A */
	final static public int EM_OUT_ASGN1_C=2;					/*  C */
	final static public int EM_OUT_ASGN1_D=3;					/*  D */
	final static public int EM_OUT_ASGN1_1=4;					/*  1 */
	final static public int EM_OUT_ASGN1_2=5;					/*  2 */
	final static public int EM_OUT_ASGN1_5=6;					/*  5 */
	final static public int EM_OUT_ASGN1_6=7;					/*  6 */
	final static public int EM_OUT_ASGN1_7=8;					/*  7 */
	final static public int EM_OUT_ASGN1_8=9;					/*  8 */
	final static public int EM_OUT_ASGN1_PATCH=10;				/*  PATCH */
	final static public int EM_OUT_ASGN1_TONE = EM_OUT_ASGN1_PATCH;		/*  TONE */
	final static public int EM_OUT_ASGN1_VALUES=11;

    /* Output Assign (1STEREO+4OUTS) */
	final static public int EM_OUT_ASGN2_EFX=0;				/*  EFX */
	final static public int EM_OUT_ASGN2_A=1;					/*  A */
	final static public int EM_OUT_ASGN2_C=2;					/*  C */
	final static public int EM_OUT_ASGN2_D=3;					/*  D */
	final static public int EM_OUT_ASGN2_1=4;					/*  1 */
	final static public int EM_OUT_ASGN2_2=5;					/*  2 */
	final static public int EM_OUT_ASGN2_5=6;					/*  5 */
	final static public int EM_OUT_ASGN2_6=7;					/*  6 */
	final static public int EM_OUT_ASGN2_7=8;					/*  7 */
	final static public int EM_OUT_ASGN2_8=9;					/*  8 */
	final static public int EM_OUT_ASGN2_PATCH=10;				/*  PATCH */
	final static public int EM_OUT_ASGN2_TONE = EM_OUT_ASGN2_PATCH;		/*  TONE */
	final static public int EM_OUT_ASGN2_VALUES=11;

            
    /* Output Assign (1STEREO+6OUTS) */
	final static public int EM_OUT_ASGN3_EFX=0;				/*  EFX */
	final static public int EM_OUT_ASGN3_B=1;					/*  B */
	final static public int EM_OUT_ASGN3_C=2;					/*  C */
	final static public int EM_OUT_ASGN3_D=3;					/*  D */
	final static public int EM_OUT_ASGN3_3=4;					/*  3 */
	final static public int EM_OUT_ASGN3_4=5;					/*  4 */
	final static public int EM_OUT_ASGN3_5=6;					/*  5 */
	final static public int EM_OUT_ASGN3_6=7;					/*  6 */
	final static public int EM_OUT_ASGN3_7=8;					/*  7 */
	final static public int EM_OUT_ASGN3_8=9;					/*  8 */
	final static public int EM_OUT_ASGN3_PATCH=10;				/*  PATCH */
	final static public int EM_OUT_ASGN3_TONE = EM_OUT_ASGN3_PATCH;		/*  TONE */
	final static public int EM_OUT_ASGN3_VALUES=11;

    /* Output Assign (8OUTS) */
	final static public int EM_OUT_ASGN4_A=0;					/*  A */
	final static public int EM_OUT_ASGN4_B=1;					/*  B */
	final static public int EM_OUT_ASGN4_C=2;					/*  C */
	final static public int EM_OUT_ASGN4_D=3;					/*  D */
	final static public int EM_OUT_ASGN4_1=4;					/*  1 */
	final static public int EM_OUT_ASGN4_2=5;					/*  2 */
	final static public int EM_OUT_ASGN4_3=6;					/*  3 */
	final static public int EM_OUT_ASGN4_4=7;					/*  4 */
	final static public int EM_OUT_ASGN4_5=8;					/*  5 */
	final static public int EM_OUT_ASGN4_6=9;					/*  6 */
	final static public int EM_OUT_ASGN4_7=10;					/*  7 */
	final static public int EM_OUT_ASGN4_8=11;					/*  8 */
	final static public int EM_OUT_ASGN4_PATCH=12;				/*  PATCH */
	final static public int EM_OUT_ASGN4_TONE = EM_OUT_ASGN4_PATCH;		/*  TONE */
	final static public int EM_OUT_ASGN4_VALUES=13;

    /* Trigger Mode */
	final static public int EMPC_TRIG_MODE_OFF=0;				/*  OFF */
	final static public int EMPC_TRIG_MODE_ONE_SHOT=1;			/*  ONE-SHOT */
	final static public int EMPC_TRIG_MODE_INFINIT=2;				/*  INFINIT */
	final static public int EMPC_TRIG_MODE_VALUES=3;

    /* LFO Rate Sync */
	final static public int EM_LFO_RATE_SYNC_OFF=0;				/*  OFF */
	final static public int EM_LFO_RATE_SYNC_CLOCK=1;				/*  CLOCK */
	final static public int EM_LFO_RATE_SYNC_VALUES=2;

    /* Delay Time Sync */
	final static public int EM_DELAY_TIME_SYNC_OFF=0;				/*  OFF */
	final static public int EM_DELAY_TIME_SYNC_CLOCK=1;			/*  CLOCK */
	final static public int EM_DELAY_TIME_SYNC_PLAYMATE=2;			/*  PLAYMATE */
	final static public int EM_DELAY_TIME_SYNC_VALUES=3;

            
            
final static public int  EM_WAV_GTYPE_INT = EM_WAV_GTYPE_WAVE_ROM;

//final static public int  EM_WAV_GTYPE_WEXP = EM_WAV_GTYPE_WAVE_EXP;
//final static public int  EM_WAV_GTYPE_WRAM = EM_WAV_GTYPE_SAMPLE;

final static public int  EM_CTRL_SRC_PLAYMATE = EM_CTRL_SRC_TEMPO;

final static public int  EMPC_SOLO_SW_POLY = EM_MONO_POLY_POLY;
final static public int  EMPC_SOLO_SW_SOLO = EM_MONO_POLY_MONO;

}
