package emil;

/**
 *
 * @author mgiacomo
 */
public class CVVPrmStm {

    public class CVVPrmProgNum {

        char pc;		//  program number
        char bankM;		//  bank select MSB
        char bankL;		//  bank select LSB
        char rhyMap;		//  GS rhythm map (OFF, 1, 2)
//	union {
//		long			mAlign;		//  for alignment
//		struct {
//			unsigned char	pc;		//  program number
//			unsigned char	bankM;		//  bank select MSB
//			unsigned char	bankL;		//  bank select LSB
//			unsigned char	rhyMap;		//  GS rhythm map (OFF, 1, 2)
//		} mByte;
//	};
    }
    public static final int PartRegEns = 0;
    public static final int PartRegVoc = 1;
    public static final int PartRegPrc = 2;
    public static final int PartRegMin = PartRegEns;
    public static final int PartRegMax = PartRegPrc;
    public static final int PartRegSize = PartRegMax + 1;
    // part #
    public static final int PartMin = 0;
    public static final int PartMax = 15;
    public static final int PartSize = PartMax + 1;
    short PrmID;				// parameter ID	
    
    public SPrmInfo sPrmInfo = new SPrmInfo();
    public SName sName = new SName();
    public SCate sCate = new SCate();
    public SMFXType sMFXType = new SMFXType();
    public SPatProg sPatProg = new SPatProg();
    public SRegProg sRegProg = new SRegProg();
    public SMasterTune sMasterTune = new SMasterTune();
    public SReserv sReserv = new SReserv();
    public SKRange sKRange = new SKRange();
    public SVRange sVRange = new SVRange();
    
    // parameter value
	public class SPrmInfo {					// parameter information
		public long	min;				//  maximum value
		public long	max;				//  minimum value
		public long	init;				//  initial value
	};
    
	public class SName {						// name
		public byte[] str=new byte[16+1];			//  16 characters + zero termination
	};
    
	public class SCate {						// category
		public byte num;				//  category number
	};
	public class SMFXType {					// MFX type
		public byte type;				//  MFX type 
	};
	public class SPatProg {					// patch (rhythm) program number
		public CVVPrmProgNum	prog;
	};
	public class SRegProg {					// registration program number
		public CVVPrmProgNum	prog;
	};
	public class SMasterTune {					// master tune
		public short	deciHz;				//  440.0[Hz] = 4400
	};
	public class SReserv {					// voice reserve
		public byte[] voices = new byte[PartSize];
	};
	public class SKRange {					// keyboard range
		public byte lower;				//  lower
		public byte upper;				//  upper
	};
	public class SVRange {					// velocity range
		public byte lower;				//  lower
		public byte upper;				//  upper
	};
}
