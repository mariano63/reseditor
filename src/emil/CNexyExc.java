package emil;

/**
 *
 * @author mgiacomo
 */
public class CNexyExc {

    static final long SIZEOF_EMEXC_PC = 0x004F; /* Patch Common							*/

    static final long SIZEOF_EMEXC_PF = 0x0091; /* Patch Common MFX						*/

    static final long SIZEOF_EMEXC_PH = 0x0054; /* Patch Common Chorus						*/

    static final long SIZEOF_EMEXC_PV = 0x0053; /* Patch Common Reverb						*/

    static final long SIZEOF_EMEXC_PX = 0x0029; /* Patch TMT (Tone Mix Table)					*/

    static final long SIZEOF_EMEXC_PT = 0x009A; /* Patch Tone							*/

    static final long SPACEOF_NEEXC_RESERV = 0x00200000;	/*!(reserved)							*/

    static final long SPACEOF_NEEXC_SETUP = 0x00200000;	/* Setup							*/

    static final long SPACEOF_NEEXC_SYS = 0x01A00000;	/* System							*/

    static final long SPACEOF_NEEXC_EDITOR = 0x02000000;	/* (for editor)							*/

    static final long OFSTOF_NEEXC_TEMP_PPART = 0x00080000;	/* Temporary Patch						*/

    static final long NUMOF_NEEXC_TEMP_PPART = 1;		/*  x 1								*/

    static final long SPACEOF_NEEXC_TEMP_PPART = 0x02200000;	/*!Temporary Patch						*/

    static final long OFSTOF_NEEXC_USER_PAT = 0x00004000;	/* User Patch (01  - 256)					*/

    static final long NUMOF_NEEXC_USER_PAT = 256;		/*  x 256							*/

    static final long SPACEOF_NEEXC_USER_PAT = 0x09FFFFFF;	/*!User Patch							*/

    static final long SPACEOF_NEEXC_SC = 0x002000;	/* System Common						*/

    static final long SPACEOF_NEEXC_SL = 0x000800;/* System Controller						*/

    static final long SPACEOF_NEEXC_TEMP_PAT = 0x040000;	/* Temporary Patch						*/

    static final long SPACEOF_NEEXC_PC = 0x000100;	/* Patch Common							*/

    static final long SPACEOF_NEEXC_PF = 0x000100;	/* Patch Common MFX						*/

    static final long SPACEOF_NEEXC_PH = 0x000100; /* Patch Common Chorus						*/

    static final long SPACEOF_NEEXC_PV = 0x000500;	/* Patch Common Reverb						*/

    static final long SPACEOF_NEEXC_PX = 0x000800;	/* Patch TMT (Tone Mix Table)					*/

    static final long OFSTOF_NEEXC_PT = 0x000100;	/* Patch Tone (Tone 1 - 4)					*/

    static final long NUMOF_NEEXC_PT = 4;		/*  x 4								*/

    static final long SPACEOF_NEEXC_PT = 0x000400;	/*!Patch Tone							*/

    static final long SPACEOF_NEEXC_EDS = 0x000080;	/* (system information)						*/

    static final long SPACEOF_NEEXC_EDW = 0x000100;	/* (wave information)						*/

    static final long SPACEOF_NEEXC_EDP = 0x000680;	/* (patch information)						*/

    static final long SPACEOF_NEEXC_EDWM = 0x000800;	/* (write message)						*/

    static final long SPACEOF_NEEXC_EDUP = 0x002F80;	/* (panel)							*/

    static final long SPACEOF_NEEXC_EDPC = 0x000000;	/* (PC mode)							*/

    static final long SIZEOF_NEEXC_STP = 0x0034;		/* Setup							*/

    static final long SIZEOF_NEEXC_SC = 0x001E;	/* System Common						*/

    static final long SIZEOF_NEEXC_SL = 0x0051;		/* System Controller						*/

    static final long SIZEOF_NEEXC_EDS = 0x0003;		/* (for editor) System Information				*/

    static final long SIZEOF_NEEXC_EDW = 0x0005;    /* (for editor) Internal Wave Information			*/

    static final long SIZEOF_NEEXC_EDP = 0x0003; 	/* (for editor) Patch Information				*/

    static final long SIZEOF_NEEXC_EDWM = 0x0002; /* (for editor) Write Message					*/

    static final long SIZEOF_NEEXC_EDUP = 0x0001; /* (for editor) Panel						*/

    static final long SIZEOF_NEEXC_EDPC = 0x0001; /* (for editor) PC Mode						*/

    static final long startOfNexy = 0x00000000;
    static final long startOfNexyReserv = startOfNexy;
    static final long startOfNexyStp = startOfNexyReserv + SPACEOF_NEEXC_RESERV;
    static final long startOfNexySys = startOfNexyStp + SPACEOF_NEEXC_SETUP;
    static final long startOfNexyEdt = startOfNexySys + SPACEOF_NEEXC_SYS;
    static final long startOfNexyPPart = startOfNexyEdt + SPACEOF_NEEXC_EDITOR;
    static final long startOfNexyUserPat = startOfNexyPPart + SPACEOF_NEEXC_TEMP_PPART;
    static final long startOfNexyUserRhy = startOfNexyUserPat + SPACEOF_NEEXC_USER_PAT;
    static final long startOfNexySysSc = 0x00000000;
    static final long startOfNexySysSl = startOfNexySysSc + SPACEOF_NEEXC_SC;
    static final long startOfNexyPatPc = 0x00000000;
    static final long startOfNexyPatPf = startOfNexyPatPc + SPACEOF_NEEXC_PC;
    static final long startOfNexyPatPh = startOfNexyPatPf + SPACEOF_NEEXC_PF;
    static final long startOfNexyPatPv = startOfNexyPatPh + SPACEOF_NEEXC_PH;
    static final long startOfNexyPatPx = startOfNexyPatPv + SPACEOF_NEEXC_PV;
    static final long startOfNexyPatPt = startOfNexyPatPx + SPACEOF_NEEXC_PX;
    static final long startOfNexyTempPat = 0x00000000;
    static final long startOfNexyTempRhy = startOfNexyTempPat + SPACEOF_NEEXC_TEMP_PAT;
    public static final int AreaTemp = 0;
    public static final int AreaUser = 1;
    byte mArea;			// area
    short mNum;			// number (for user area)
    long mRemainSize;		// remain size of the block
    long mNextAdrs;          // top address of the next block

static public void GetInfoTempPat(long theAdrs, long theSize, int thePart)
{
	theAdrs = startOfNexyPPart; 
	theSize = SPACEOF_NEEXC_PC + SPACEOF_NEEXC_PF + SPACEOF_NEEXC_PH + SPACEOF_NEEXC_PV + SPACEOF_NEEXC_PX + SPACEOF_NEEXC_PT;
}
static public long GetInfoTempPatAdrs(int thePart)
{
	return startOfNexyPPart; 
}
static public long GetInfoTempPatSize(int thePart)
{
	return SPACEOF_NEEXC_PC + SPACEOF_NEEXC_PF + SPACEOF_NEEXC_PH + SPACEOF_NEEXC_PV + SPACEOF_NEEXC_PX + SPACEOF_NEEXC_PT;
}
    void CallSTP(long theOfst) {
        VTempSTP(theOfst);
    }

    void CallSC(long theOfst) {
        VTempSC(theOfst);
    }

    void CallSL(long theOfst) {
        VTempSL(theOfst);
    }

    void CallPC(long theOfst) {
        if (mArea == AreaTemp) {
            VTempPC(theOfst, 0);
        } else {
            VUserPC(theOfst, mNum);
        }
    }

    void CallPF(long theOfst) {
        if (mArea == AreaTemp) {
            VTempPF(theOfst, 0);
        } else {
            VUserPF(theOfst, mNum);
        }
    }

    void CallPH(long theOfst) {
        if (mArea == AreaTemp) {
            VTempPH(theOfst, 0);
        } else {
            VUserPH(theOfst, mNum);
        }
    }

    void CallPV(long theOfst) {
        if (mArea == AreaTemp) {
            VTempPV(theOfst, 0);
        } else {
            VUserPV(theOfst, mNum);
        }
    }

    void CallPX(long theOfst) {
        if (mArea == AreaTemp) {
            VTempPX(theOfst, 0);
        } else {
            VUserPX(theOfst, mNum);
        }
    }

    void CallPT(long theOfst, int theTone) {
        if (mArea == AreaTemp) {
            VTempPT(theOfst, 0, theTone);
        } else {
            VUserPT(theOfst, mNum, theTone);
        }
    }

    void CallRC(long theOfst) {
        if (mArea == AreaTemp) {
            VTempRC(theOfst, 0);
        } else {
            VUserRC(theOfst, mNum);
        }
    }

    void CallRF(long theOfst) {
        if (mArea == AreaTemp) {
            VTempRF(theOfst, 0);
        } else {
            VUserRF(theOfst, mNum);
        }
    }

    void CallRH(long theOfst) {
        if (mArea == AreaTemp) {
            VTempRH(theOfst, 0);
        } else {
            VUserRH(theOfst, mNum);
        }
    }

    void CallRV(long theOfst) {
        if (mArea == AreaTemp) {
            VTempRV(theOfst, 0);
        } else {
            VUserRV(theOfst, mNum);
        }
    }

    void CallRT(long theOfst, int theNote) {
        if (mArea == AreaTemp) {
            VTempRT(theOfst, 0, theNote);
        } else {
            VUserRT(theOfst, mNum, theNote);
        }
    }

    void VTempSTP(long theOfst) {
    }

    void VTempSC(long theOfst) {
    }

    void VTempSL(long theOfst) {
    }

    public void VTempPC(long theOfst, int thePart) {
    }

    public void VUserPC(long theOfst, int theNum) {
    }

    public void VTempPF(long theOfst, int thePart) {
    }

    public void VUserPF(long theOfst, int theNum) {
    }

    public void VTempPH(long theOfst, int thePart) {
    }

    public void VUserPH(long theOfst, int theNum) {
    }

    public void VTempPV(long theOfst, int thePart) {
    }

    public void VTempPX(long theOfst, int thePart) {
    }

    public void VTempPT(long theOfst, int thePart, int theTone) {
    }

    public void VTempRC(long theOfst, int thePart) {
    }

    public void VTempRF(long theOfst, int thePart) {
    }

    public void VTempRH(long theOfst, int thePart) {
    }

    public void VTempRV(long theOfst, int thePart) {
    }

    public void VTempRT(long theOfst, int thePart, int theNote) {
    }

    public void VUserPV(long theOfst, int theNum) {
    }

    public void VUserPX(long theOfst, int theNum) {
    }

    public void VUserPT(long theOfst, int theNum, int theTone) {
    }

    public void VUserRC(long theOfst, int theNum) {
    }

    public void VUserRF(long theOfst, int theNum) {
    }

    public void VUserRH(long theOfst, int theNum) {
    }

    public void VUserRV(long theOfst, int theNum) {
    }

    public void VUserRT(long theOfst, int theNum, int theNote) {
    }

//--------------------------------------------------------------
// ParseAdrs
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Parse the Nexy exclusive address.
//	entry:	theAdrs		address
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
void ParseAdrs(long theAdrs)
{
	ClearRemainSize();

	long p = theAdrs;
	long end;
	if (p < (end = (startOfNexyReserv + SPACEOF_NEEXC_RESERV))) {
		mArea = AreaTemp;
		mNextAdrs = end;
	}
	else if (p < (end = (startOfNexyStp + SPACEOF_NEEXC_SETUP))) {
		mArea = AreaTemp;
		mNextAdrs = end;
		ParseStp(p, startOfNexyStp);
	}
	else if (p < (end = (startOfNexySys + SPACEOF_NEEXC_SYS))) {
		mArea = AreaTemp;
		mNextAdrs = end;
		ParseSys(p, startOfNexySys);
	}
	else if (p < (end = (startOfNexyEdt + SPACEOF_NEEXC_EDITOR))) {
		mArea = AreaTemp;
		mNextAdrs = end;
		ParseEdt(p, startOfNexyEdt);
	}
	else if (p < (end = (startOfNexyPPart + SPACEOF_NEEXC_TEMP_PPART))) {
		mArea = AreaTemp;
		mNextAdrs = end;
		ParseTempPart(p, startOfNexyPPart);
	}
	else if (p < (end = (startOfNexyUserPat + SPACEOF_NEEXC_USER_PAT))) {
		mArea = AreaUser;
		mNextAdrs = end;
		for (int i = 0; i < NUMOF_NEEXC_USER_PAT; i++) {
			if (p < (end = (startOfNexyUserPat + OFSTOF_NEEXC_USER_PAT*(i+1)))) {
				mNum = (short) (i + 1);
				mNextAdrs = end;
				ParsePat(p, (startOfNexyUserPat + OFSTOF_NEEXC_USER_PAT*i));
				break;
			}
		}
	}
}

//--------------------------------------------------------------
// ParseTempPart
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Parse exclusive address at temporary part parameter.
//	entry:	thePtr		virtual exclusive address
//		SNEExcTempPart*	virtual exclusive block
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
void ParseTempPart(long thePtr, long thePart)
{
	long end;
	if (thePtr < (end = thePart + startOfNexyTempPat + SPACEOF_NEEXC_TEMP_PAT)) {
		mNextAdrs = end;
		ParsePat(thePtr, thePart + startOfNexyTempPat);
	}
}

//--------------------------------------------------------------
// ParseEdt
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Parse exclusive address at editor parameter.
//	entry:	thePtr		virtual exclusive address
//		SNEExcEdt*	virtual exclusive block
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
void ParseEdt(long thePtr, long theEdt)
{
	mNextAdrs = 0xFFFFFFF;
}

//--------------------------------------------------------------
// ParsePat
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Parse exclusive address at patch parameter.
//	entry:	thePtr		virtual exclusive address
//		SNEExcPat*	virtual exclusive block
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
void ParsePat(long thePtr, long thePat)
{
	long end;
	long bound;
	if (thePtr < (end = thePat + startOfNexyPatPc + SPACEOF_NEEXC_PC)) {
		mNextAdrs = end;
		if (thePtr < (bound = thePat + startOfNexyPatPc + SIZEOF_EMEXC_PC)) {
			mRemainSize = bound - thePtr;
			CallPC(thePtr - (thePat + startOfNexyPatPc));
		}
	}
	else if (thePtr < (end = thePat + startOfNexyPatPf + SPACEOF_NEEXC_PF)) {
		mNextAdrs = end;
		if (thePtr < (bound = thePat + startOfNexyPatPf + SIZEOF_EMEXC_PF)) {
			mRemainSize = bound - thePtr;
			CallPF(thePtr - (thePat + startOfNexyPatPf));
		}
	}
	else if (thePtr < (end = thePat + startOfNexyPatPh + SPACEOF_NEEXC_PH)) {
		mNextAdrs = end;
		if (thePtr < (bound = thePat + startOfNexyPatPh + SIZEOF_EMEXC_PH)) {
			mRemainSize = bound - thePtr;
			CallPH(thePtr - (thePat + startOfNexyPatPh));
		}
	}
	else if (thePtr < (end = thePat + startOfNexyPatPv + SPACEOF_NEEXC_PV)) {
		mNextAdrs = end;
		if (thePtr < (bound = thePat + startOfNexyPatPv + SIZEOF_EMEXC_PV)) {
			mRemainSize = bound - thePtr;
			CallPV(thePtr - (thePat + startOfNexyPatPv));
		}
	}
	else if (thePtr < (end = thePat + startOfNexyPatPx + SPACEOF_NEEXC_PX)) {
		mNextAdrs = end;
		if (thePtr < (bound = thePat + startOfNexyPatPx + SIZEOF_EMEXC_PX)) {
			mRemainSize = bound - thePtr;
			CallPX(thePtr - (thePat + startOfNexyPatPx));
		}
	}
	else if (thePtr < (end = thePat + startOfNexyPatPt + SPACEOF_NEEXC_PT)) {
		mNextAdrs = end;
		for (int i = 0; i < NUMOF_NEEXC_PT; i++) {
			if (thePtr < (end = thePat + startOfNexyPatPt + OFSTOF_NEEXC_PT*(i+1))) {
				mNextAdrs = end;
				if (thePtr < (bound = thePat + startOfNexyPatPt + OFSTOF_NEEXC_PT*i + SIZEOF_EMEXC_PT)) {
					mRemainSize = bound - thePtr;
					CallPT(thePtr - (thePat + startOfNexyPatPt + OFSTOF_NEEXC_PT*i), i);
				}
				break;
			}
		}
	}
}

//--------------------------------------------------------------
// ParseSys
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Parse exclusive address at system parameter.
//	entry:	thePtr		virtual exclusive address
//		SNEExcSys*	virtual exclusive block
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
void ParseSys(long thePtr, long theSys)
{
	long end;
	long bound;
	if (thePtr < (end = theSys + startOfNexySysSc + SPACEOF_NEEXC_SC)) {
		mNextAdrs = end;
		if (thePtr < (bound = theSys + startOfNexySysSc + SIZEOF_NEEXC_SC)) {
			mRemainSize = bound - thePtr;
			VTempSC(thePtr - (theSys + startOfNexySysSc));
		}
	}
	else if (thePtr < (end = theSys + startOfNexySysSl + SPACEOF_NEEXC_SL)) {
		mNextAdrs = end;
		if (thePtr < (bound = theSys + startOfNexySysSl + SIZEOF_NEEXC_SL)) {
			mRemainSize = bound - thePtr;
			CallSL(thePtr - (theSys + startOfNexySysSl));
		}
	}
}

//--------------------------------------------------------------
// ParseStp
//--------------------------------------------------------------
//
//	prog :	isogawa
//	func :	Parse exclusive address at setup parameter.
//	entry:	thePtr		virtual exclusive address
//		SNEExcStp*	virtual exclusive block
//	retrn:	void
//	remks:	---
//
//--------------------------------------------------------------
    void ParseStp(long thePtr, long theStp) {
        long bound;
        if (thePtr < (bound = theStp + SIZEOF_NEEXC_STP)) {
            mRemainSize = bound - thePtr;
            CallSTP(thePtr - theStp);
        }
    }

    void ClearRemainSize() {
        mRemainSize = 0;
        mNextAdrs = 0xffffffffl;
//        mNextAdrs = Long.MAX_VALUE;   //Java differ from C
    }

    long GetRemainSize() {
        return mRemainSize;
    }		// get the remain size of the block

    long GetNextAdrs() {
        return mNextAdrs;
    }	// top address of the next block

    public static void GetInfoTempPC(long theAdrs, long theSize, int thePart) {
        theAdrs = startOfNexyPPart + startOfNexyPatPc;
        theSize = SIZEOF_EMEXC_PC;
    }
    public static long GetInfoTempPCAdrs(int thePart) {
        return startOfNexyPPart + startOfNexyPatPc;
    }
    public static long GetInfoTempPCSize(int thePart) {
        return SIZEOF_EMEXC_PC;
    }

    public static void GetInfoTempPF(long theAdrs, long theSize, int thePart) {
        theAdrs = startOfNexyPPart + startOfNexyPatPf;
        theSize = SIZEOF_EMEXC_PF;
    }
    public static long GetInfoTempPFAdrs(int thePart) {
        return startOfNexyPPart + startOfNexyPatPf;
    }
    public static long GetInfoTempPFSize(int thePart) {
        return SIZEOF_EMEXC_PF;
    }

    public static void GetInfoTempPH(long theAdrs, long theSize, int thePart) {
        theAdrs = startOfNexyPPart + startOfNexyPatPh;
        theSize = SIZEOF_EMEXC_PH;
    }
    public static long GetInfoTempPHAdrs(int thePart) {
        return startOfNexyPPart + startOfNexyPatPh;
    }
    public static long GetInfoTempPHSize(int thePart) {
        return SIZEOF_EMEXC_PH;
    }

    public static void GetInfoTempPV(long theAdrs, long theSize, int thePart) {
        theAdrs = startOfNexyPPart + startOfNexyPatPv;
        theSize = SIZEOF_EMEXC_PV;
    }
    public static long GetInfoTempPVAdrs(int thePart) {
        return startOfNexyPPart + startOfNexyPatPv;
    }
    public static long GetInfoTempPVSize(int thePart) {
        return SIZEOF_EMEXC_PV;
    }

    public static void GetInfoTempPX(long theAdrs, long theSize, int thePart) {
        theAdrs = startOfNexyPPart + startOfNexyPatPx;
        theSize = SIZEOF_EMEXC_PX;
    }
    public static long GetInfoTempPXAdrs(int thePart) {
        return startOfNexyPPart + startOfNexyPatPx;
    }
    public static long GetInfoTempPXSize(int thePart) {
        return SIZEOF_EMEXC_PX;
    }

    public static void GetInfoTempPT(long theAdrs, long theSize, int thePart, int theTone) {
        theAdrs = startOfNexyPPart + startOfNexyPatPt + OFSTOF_NEEXC_PT * theTone;
        theSize = SIZEOF_EMEXC_PT;
    }
    public static long GetInfoTempPTAdr(int thePart, int theTone) {
        return startOfNexyPPart + startOfNexyPatPt + OFSTOF_NEEXC_PT * theTone;
    }
    public static long GetInfoTempPTSize(int thePart, int theTone) {
        return SIZEOF_EMEXC_PT;
    }

    public static void GetInfoTempRhy(long theAdrs, long theSize, int thePart) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoTempRC(long theAdrs, long theSize, int thePart) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoTempRF(long theAdrs, long theSize, int thePart) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoTempRH(long theAdrs, long theSize, int thePart) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoTempRV(long theAdrs, long theSize, int thePart) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoTempRT(long theAdrs, long theSize, int thePart, int theNote) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoUserPat(long theAdrs, long theSize, int theNum) {
        theAdrs = startOfNexyUserPat + OFSTOF_NEEXC_USER_PAT * (theNum - 1);
        theSize = SPACEOF_NEEXC_PC + SPACEOF_NEEXC_PF + SPACEOF_NEEXC_PH + SPACEOF_NEEXC_PV + SPACEOF_NEEXC_PX + SPACEOF_NEEXC_PT;
    }

    public static void GetInfoUserPC(long theAdrs, long theSize, int theNum) {
        theAdrs = startOfNexyUserPat + OFSTOF_NEEXC_USER_PAT * (theNum - 1) + startOfNexyPatPc;
        theSize = SIZEOF_EMEXC_PC;
    }
    public static long GetInfoUserPCAdrs(int theNum) {
        return startOfNexyUserPat + OFSTOF_NEEXC_USER_PAT * (theNum - 1) + startOfNexyPatPc;
    }
    public static long GetInfoUserPCSize(int theNum) {
        return SIZEOF_EMEXC_PC;
    }

    public static void GetInfoUserPF(long theAdrs, long theSize, int theNum) {
        theAdrs = startOfNexyUserPat + OFSTOF_NEEXC_USER_PAT * (theNum - 1) + startOfNexyPatPf;
        theSize = SIZEOF_EMEXC_PF;
    }

    public static void GetInfoUserPH(long theAdrs, long theSize, int theNum) {
        theAdrs = startOfNexyUserPat + OFSTOF_NEEXC_USER_PAT * (theNum - 1) + startOfNexyPatPh;
        theSize = SIZEOF_EMEXC_PH;
    }

    public static void GetInfoUserPV(long theAdrs, long theSize, int theNum) {
        theAdrs = startOfNexyUserPat + OFSTOF_NEEXC_USER_PAT * (theNum - 1) + startOfNexyPatPv;
        theSize = SIZEOF_EMEXC_PV;
    }

    public static void GetInfoUserPX(long theAdrs, long theSize, int theNum) {
        theAdrs = startOfNexyUserPat + OFSTOF_NEEXC_USER_PAT * (theNum - 1) + startOfNexyPatPx;
        theSize = SIZEOF_EMEXC_PX;
    }

    public static void GetInfoUserPT(long theAdrs, long theSize, int theNum, int theTone) {
        theAdrs = startOfNexyUserPat + OFSTOF_NEEXC_USER_PAT * (theNum - 1) + startOfNexyPatPt + OFSTOF_NEEXC_PT * theTone;
        theSize = SIZEOF_EMEXC_PT;
    }

    public static void GetInfoUserRhy(long theAdrs, long theSize, int theNum) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoUserRC(long theAdrs, long theSize, int theNum) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoUserRF(long theAdrs, long theSize, int theNum) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoUserRH(long theAdrs, long theSize, int theNum) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoUserRV(long theAdrs, long theSize, int theNum) {
        theAdrs = 0;
        theSize = 0;
    }

    public static void GetInfoUserRT(long theAdrs, long theSize, int theNum, int theNote) {
        theAdrs = 0;
        theSize = 0;
    }
}
