package emil;

import config.ParameterManager;
import emil.semil.SEmilPat;
import emil.semil.SEmilRhy;

/**
 *
 * @author mgiacomo
 */
public class CVVTempAreaPart {


    public CVVTempAreaPart() {
        for (int i = 0; i < CEmilLib.EMIL_PART_PAT; i++) {
            SEmilPat.gEmilTempPat[i] = new SEmilPat();
            ParameterManager.gEmilPrm.EmilClearPat(SEmilPat.gEmilTempPat[i], null);
        }
        for (int i = 0; i < CEmilLib.EMIL_PART_PAT; i++) {
            SEmilRhy.gEmilTempRhy[i] = new SEmilRhy();
            ParameterManager.gEmilPrm.EmilClearRhy(SEmilRhy.gEmilTempRhy[i], null);
        }
    }

    public SEmilPat GetEmilPat(int thePart) {
        return SEmilPat.gEmilTempPat[thePart];
    }

    public SEmilRhy GetEmilRhy(int thePart) {
        return SEmilRhy.gEmilTempRhy[thePart];
    }
}
