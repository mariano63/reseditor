/**
 * La classe parametro
 */
package config;

import java.io.Serializable;

/**
 *
 * @author mgiacomo
 */
public class Parameter implements Serializable {

    private int emilId = -1;        //Deve combaciare con ID in em_prmid.h
    private String emilTitle = "";  //Letto dal file em_prmstr
    private String emilRange = "";  //da em_prmrng
    private String emilOnRange = ""; //Se c'e' un on" nel range lo leggo qui. da emilRange
            //Questi vanno letti da em_prmtbl.h
    int adrs;
    int bit;
    int blank;
    int ofst;
    int swInit; 
    int swMin;  
    int swMax;  

    public int getSwInit() {
        return swInit;
    }

    public void setSwInit(int swInit) {
        this.swInit = swInit;
    }

    public int getSwMin() {
        return swMin;
    }

    public void setSwMin(int swMin) {
        this.swMin = swMin;
    }

    public int getSwMax() {
        return swMax;
    }

    public void setSwMax(int swMax) {
        this.swMax = swMax;
    }

    public int getEmilId() {
        return emilId;
    }

    public void setEmilId(int emilId) {
        this.emilId = emilId;
    }

    public String getEmilTitle() {
        return emilTitle;
    }

    public void setEmilTitle(String emilTitle) {
        this.emilTitle = emilTitle;
    }

    public String getEmilRange() {
        return emilRange;
    }

    public void setEmilRange(String emilRange) {
        this.emilRange = emilRange;
    }

    public String getEmilOnRange() {
        return emilOnRange;
    }

    public void setEmilOnRange(String emilOnRange) {
        this.emilOnRange = emilOnRange;
    }

    public int getAdrs() {
        return adrs;
    }

    public void setAdrs(int adrs) {
        this.adrs = adrs;
    }

    public int getBit() {
        return bit;
    }

    public void setBit(int bit) {
        this.bit = bit;
    }

    public int getBlank() {
        return blank;
    }

    public void setBlank(int blank) {
        this.blank = blank;
    }

    public int getOfst() {
        return ofst;
    }

    public void setOfst(int ofst) {
        this.ofst = ofst;
    }

    
    
    
}
