/*
 * This file is created automatically.
 * Also see "prmdb.sh" and "emil.prm" for summary.
 */
#ifndef _em_prmsize_h_
#define _em_prmsize_h_

#define SIZEOF_EMPC			56		/* Patch Common							*/

#define SIZEOF_EMPF			78		/* Patch Common MFX						*/

#define SIZEOF_EMPH			42		/* Patch Common Chorus						*/

#define SIZEOF_EMPV			42		/* Patch Common Reverb						*/

#define SIZEOF_EMPX			32		/* Patch TMT (Tone Mix Table)					*/

#define SIZEOF_EMPT			99		/* Patch Tone							*/
#define NUMOF_EMPT			4		/*  x 4								*/

#define SIZEOF_EMRC			14		/* Rhythm Common						*/

#define SIZEOF_EMRF			78		/* Rhythm Common MFX						*/

#define SIZEOF_EMRH			42		/* Rhythm Common Chorus						*/

#define SIZEOF_EMRV			42		/* Rhythm Common Reverb						*/

#define SIZEOF_EMRT			122		/* Rhythm Tone							*/
#define NUMOF_EMRT			88		/*  x 88							*/

#define NUMOF_EMPAT_PT			4		/* Patch Tone x 4						*/
#define NUMOF_EMUSER_PAT		256		/* Patch							*/
#define NUMOF_EMUSER_BANK		1		/* Bank								*/

#endif /* !_em_prmsize_h_ */
